//
//  Utilities.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 16/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit


class Utilities: NSObject {
    
    
    private static var googlePlace: GooglePlaceFromId?
    
    static func handleRegionLeftEvent(region: CLRegion, viewController: UIViewController){
        GoogleClient.sharedInstance.getPlaceFromId(palceId: region.identifier) { (place) in
            self.googlePlace = place
            //Save to recently visited
            
            let placeName = place.result?.name
            let placeID = place.result?.place_id
            let name = place.result?.name
            
            
            FirebaseDBProvider.saveRecentlyVisited(placeId: placeID!, locationName: name!)
            
  
            let title = "Visit to \(placeName!)"
            let message = "You just visited \(placeName!). We would like to hear what you have to say about the place so we can serve you better in the future. Would you like to leave the feedback? "
            //alertWithAction(title: title, message: message, viewController: viewController)
            alertWithAction(title: title, message: message, viewController: viewController)
        }
    
    }
    

    
    
    
    private static func alertWithAction(title: String, message: String, viewController: UIViewController){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let confirmButton = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) in
            handleYesToFeeedback(vc: viewController)
        }
        let cancelButton = UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(confirmButton)
        alert.addAction(cancelButton)
        viewController.navigationController?.present(alert, animated: true, completion: nil)
        
    }
    
    private static func handleYesToFeeedback(vc: UIViewController){
        let feedbackVC = FeedbackViewController()
        feedbackVC.googlePlace = googlePlace
        vc.present(feedbackVC, animated: true, completion: nil)
    }
}


