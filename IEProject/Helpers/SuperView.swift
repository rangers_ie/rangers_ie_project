//
//  SuperView.swift
//  CompanionApp
//
//  Created by Minon Weerasinghe on 14/8/18.
//  Copyright © 2018 FIT5140. All rights reserved.
//

import UIKit

class SuperView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    func setUpView(){
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    

}
