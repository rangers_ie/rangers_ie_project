//
//  Notifications.swift
//  VOH
//
//  Created by Minon Weerasinghe on 10/12/17.
//  Copyright © 2017 Voice Of Health. All rights reserved.
//

import Foundation
import UIKit


protocol UserAlertActionDelegate: class {
    func userResponded()
}

class NotificationProvider: NSObject {
    
    weak var delegate: UserAlertActionDelegate?
   
    
    static let sharedInstance = NotificationProvider()
    
    
    
    
    
    func alertUser(title: String, message: String, viewController: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        viewController.present(alert, animated: true, completion: nil)
        
    }
    
    func alertWithAction(title: String, message: String, viewController: UIViewController){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let confirmButton = UIAlertAction(title: "Yes I'm sure", style: UIAlertActionStyle.default) { (action) in
            self.delegate?.userResponded()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(confirmButton)
        alert.addAction(cancelButton)
        viewController.present(alert, animated: true, completion: nil)
        
        
    }
    
    
}
