//
//  PTVApi.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 28/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import Foundation
import SwiftPTV

class PTVProvider: NSObject {
    
    private static let ueserID = PTVKEYS.USER_ID
    private static let apiKey = PTVKEYS.API_KEY
    public static let ptvAPI = SwiftPTV(apiKey: apiKey, userID: ueserID)
    
}
