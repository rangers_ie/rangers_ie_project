//
//  Constants.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 26/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import Foundation

class CoordinateConstants: NSObject {
    static let MELBOURNE_CBD_LATITUDE = -37.814
    static let MELBOURNE_CBD_LOGITITUDE = 144.96332
}

class FirebaseConstants: NSObject {
    static let RECENT_PLACES_VISITED = "RecentlyVisted"
    static let LOCATION_FEEDBACK = "LocationFeedback"
    static let ROUTE_REVIEW = "RouteReviews"
    
    static let PLACE_ID = "PlaceId"
    static let PLACE_NAME = "PlaceName"
    static let IMAGE_REF = "ImageURL"
    
    static let SUCESS = "sucess"
    static let FAIL = "fail"
}

class Questions: NSObject {
    static let QUESTION_A = "Did the premises offer ramp \n access?"
    static let QUESTION_B = "Did you have adequate \n space?"
    static let QUESTION_C = "Was the staff \n friendly"
    static let allQuestion = [Questions.QUESTION_A, Questions.QUESTION_B, Questions.QUESTION_C]
}

class Onboarding: NSObject{
    static let INTRO = "Welcome to WheelyGo"
    static let CAFE_TITLE = "Cafes"
    static let REST_TITLE = "Restaurants"
    static let TOILETS_TITLE = "Public Toilets"
    static let TRAM_TITLE = "Tram"
    static let ACCES_TITLE = "Accessibility Info"
    
    
    static let INTRO_DESC = "WheelyGo is aimed at helping the wheelchair users navigate the streets of Melbourne. We have provided a range of functionliaties that are tailor made for wheelchair users."
    
    
    static let CAFE_DESC = "With the cafe function you will be provided with a list of cafes around you and you have the option of choosing which cafe you would like to visit."
    
    static let REST_DESC = "Restaurant function will provide you with a list of resturants around you and you have the option of choosing which resturants you would like to visit."
    
    static let TOILETS_DESC = "WheelyGo lists out all the publicly available, wheelchair accessible toilets around you with the option of getting live directions to the destination."
   
    static let TRAM_DESC = "With the our tram feature all you have to do is select the tram route that you wish to take and we list out wheelchair accessible trams stops on your selected route."
    
    static let ACCES_DESC = "WheelyGo provides accessibility information of various building around the Melburne city. All you have to do is select the feature and enter the address."
}

class Constants: NSObject {
    static let SUCESS = "sucess"
    static let FAILURE = "failure"
    
    static let CAFE = "cafe"
    static let RESTAURANT = "restaurant"
    
    static let ASHLEY = WheelyGoTeam(url: "https://www.linkedin.com/in/ashley-wang-9174a6156/", title: "Data Analyst", name: "Ashley Wang")
    static let MINON = WheelyGoTeam(url: "https://www.linkedin.com/in/minon-weerasinghe-a61379b7/", title: "iOS Developer", name: "Minon Weerasinghe")
    static let AKASH = WheelyGoTeam(url: "https://www.linkedin.com/in/akashsengupta4/", title: "Business Analyst", name: "Akash Sengupta")
    static let LIN = WheelyGoTeam(url: "https://www.linkedin.com/in/chenxin-lin-01a09b99/", title: "Web Developer", name: "Chenxin Lin")
}





