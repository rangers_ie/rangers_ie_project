//
//  SuperCollectionViewCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 27/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class SuperCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    func setUpView(){
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
 
}
