//
//  ZomatoAPI.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 3/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import Foundation
import CoreLocation
class ZomatoAPI: NSObject{
    
    
    static func getTopTrending(location: CLLocation, collectionId: Int?,establishmentType: Int?,completition: @escaping(TrendingWeekly)->()){
        
        let latitude = location.coordinate.latitude
        let longititude = location.coordinate.longitude
        var urlString = ""
        
        if let collectionId = collectionId{
            urlString = "https://developers.zomato.com/api/v2.1/search?lat=\(latitude)&lon=\(longititude)&radius=10000&collection_id=\(collectionId)&sort=real_distance&order=asc&apikey=\(ZOMATO_KEYS.APIKEY)"
        }
        if let establishmentType = establishmentType{
            
            urlString = "https://developers.zomato.com/api/v2.1/search?lat=\(latitude)&lon=\(longititude)&radius=10000&establishment_type=\(establishmentType)&sort=real_distance&order=asc&apikey=\(ZOMATO_KEYS.APIKEY)"
        }
    
        guard let url = URL(string: urlString) else{print("Unable to general URL"); return}
        
        
        let task = URLSession.shared.dataTask(with: url) { (responceData, urlResponce, error) in
            if let error = error {
                print(error)
                print(error.localizedDescription)
                return
            }
            guard let data = responceData else {
                print("No data")
                return
            }
            
            do{
                let jsonDecoder = JSONDecoder()
                let responceModel = try jsonDecoder.decode(TrendingWeekly.self, from: data)
                DispatchQueue.main.async {
                    completition(responceModel)
                }
            }
            catch{
                print("Error loading Zomato Data")
                print(error)
                print(error.localizedDescription)
                
            }
        }
        task.resume()
    }
    
    static func loadRestaurantData(id: Int, completition:@escaping(ZomRestFromId)->()){
        
        let urlString = "https://developers.zomato.com/api/v2.1/restaurant?res_id=\(id)&apikey=\(ZOMATO_KEYS.APIKEY)"
        
        guard let url = URL(string: urlString) else{print("Unable to general URL"); return}
        
        let task = URLSession.shared.dataTask(with: url) { (responceData, urlResponce, error) in
            if let error = error {
                print(error)
                print(error.localizedDescription)
                return
            }
            guard let data = responceData else {
                print("No data")
                return
            }
            
            do{
                let jsonDecoder = JSONDecoder()
                let responceModel = try jsonDecoder.decode(ZomRestFromId.self, from: data)
                DispatchQueue.main.async {
                    completition(responceModel)
                }
            }
            catch{
                print(error)
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
    
    
    static func getZomatoProfile(location: CLLocation, query: String,completition: @escaping(ZomatoTrending)->()){
        
        let latitude = location.coordinate.latitude
        let longititude = location.coordinate.longitude
        var urlString = ""
        

        urlString = "https://developers.zomato.com/api/v2.1/search?lat=\(latitude)&lon=\(longititude)&radius=10000&q=\(query)&sort=real_distance&order=asc&apikey=\(ZOMATO_KEYS.APIKEY)&count=1"
        
        print(urlString)
        
        
        guard let url = URL(string: urlString) else{print("Unable to general URL"); return}
        
        let task = URLSession.shared.dataTask(with: url) { (responceData, urlResponce, error) in
    
            if let error = error {
                print(error)
                print(error.localizedDescription)
                return
            }
            guard let data = responceData else {
                print("No data")
                return
            }
            
            do{
                let jsonDecoder = JSONDecoder()
                let responceModel = try jsonDecoder.decode(ZomatoTrending.self, from: data)
                DispatchQueue.main.async {
                    completition(responceModel)
                }
            }
            catch{
                print("Error loading data getZomatoProfile ")
                print(error)
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
    
    
    
    
}
