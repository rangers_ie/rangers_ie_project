//
//  GoogleClient.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 28/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import Foundation

import Foundation
import CoreLocation
import AFNetworking
import SDWebImage


class GoogleClient : NSObject {
    
    static let sharedInstance = GoogleClient()
    private let session = URLSession(configuration: .default)



    func getPlacesAroundGivenLocation(coordinates: CLLocation,locationType: String, completionHandler: @escaping (GooglePlace) -> ())  {
        
        let latitide = coordinates.coordinate.latitude
        let longititude = coordinates.coordinate.longitude
        
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=\(GOOGLEAPIKEYS.GOOGLE_API_KEY)&location=\(latitide),\(longititude)&radius=10000&type=\(locationType)")
        
        
        let task = session.dataTask(with: url!) { (responseData, responseObject, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            guard let response = try? JSONDecoder().decode(GooglePlace.self, from: responseData!) else {return}
            
            DispatchQueue.main.async {
                completionHandler(response)
            }
            
        }
        task.resume()
    }
    
    
    func getRouteData(sourceCoordinates: CLLocation, destinationCoordinates: CLLocation, completition: @escaping(GoogleDirections)->()){
        
        let sourceLatitude = sourceCoordinates.coordinate.latitude
        let sourceLongititude = sourceCoordinates.coordinate.longitude
        
        let destinationLatitude = destinationCoordinates.coordinate.latitude
        let desintationLongititude = destinationCoordinates.coordinate.longitude
        
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?mode=walking&origin=\(sourceLatitude),\(sourceLongititude)&destination=\(destinationLatitude),\(desintationLongititude)&alternatives=true&avoid=indoor&key=\(GOOGLEAPIKEYS.GOOGLE_API_KEY)")
        
    
        
        let task = session.dataTask(with: url!) { (responseData, responseObject, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let response = try? JSONDecoder().decode(GoogleDirections.self, from: responseData!)else{return}
            DispatchQueue.main.async {
                completition(response)
              
            }
        }
        task.resume()
        
    }
    
    func getImageURL(pictureReference: String)->URL{
        let url = URL(string: "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=\(pictureReference)&key=\(GOOGLEAPIKEYS.GOOGLE_API_KEY)")
        return url!
    }
    
    func getElevationData(startLocaton: CLLocation, endLocation: CLLocation, completition:@escaping(LocationElevation)->()){
        
        let startLats = startLocaton.coordinate.latitude
        let startLongs = startLocaton.coordinate.longitude
        let endLates = endLocation.coordinate.latitude
        let endLongs = endLocation.coordinate.longitude
        
    
        let urlString = "https://maps.googleapis.com/maps/api/elevation/json?path=\(startLats),\(startLongs)%7C\(endLates),\(endLongs)&samples=2&key=AIzaSyCcF3R2ePhvrKNQON2AxQOWg3Fq36VszCQ"
        
        print(urlString)
        
       
        guard let url = URL(string: urlString)else{print("Unable to create URL"); return}

        let task = session.dataTask(with: url) { (responseData, urlResponse, error) in
            if let error = error{
                print(error.localizedDescription)
                return
            }
            guard let response = try? JSONDecoder().decode(LocationElevation.self, from: responseData!)else{return}
            DispatchQueue.main.async {
                completition(response)
            }
        }
        task.resume()
    }
    
    
    func getPlaceFromId(palceId: String, completition:@escaping(GooglePlaceFromId)->()){
        
        let urlString = "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(palceId)&key=\(GOOGLEAPIKEYS.GOOGLE_API_KEY)"
        
        //let urlString = "https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJN1t_tDeuEmsRUsoyG83frY4&key=AIzaSyCcF3R2ePhvrKNQON2AxQOWg3Fq36VszCQ"
        
        guard let url = URL(string: urlString) else {print("Unable to generate URL");return}
        
        let task = session.dataTask(with: url) { (responseData, urlResponse, error) in
            if let error = error{
                print(error.localizedDescription)
                return
            }
            guard let response = try? JSONDecoder().decode(GooglePlaceFromId.self, from: responseData!)else{return}
            
            DispatchQueue.main.async {
                completition(response)
            }
      
        }
        task.resume()
        
    }
    
    func getPlaceInformationUsingAddress(address: String, completition: @escaping(PlaceFromAddress)->()){
        let urlString = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=\(address)&key=\(GOOGLEAPIKEYS.GOOGLE_API_KEY)"
        guard let url = URL(string: urlString) else {print("Unable to generate URL");return}
        
        let task = session.dataTask(with: url) { (responseData, urlResponse, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            guard let response = try? JSONDecoder().decode(PlaceFromAddress.self, from: responseData!)else{return}
            
            DispatchQueue.main.async {
                completition(response)
            }
    
        }
        task.resume()
    }
    
    func getPlaceInfoUsingAddressAndCoordinates(address: String,location:CLLocation, completition: @escaping(PlaceFromAddress)->()){
        
        let latitude = location.coordinate.latitude
        let longititude = location.coordinate.longitude
        
        let urlString = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=\(address)&location=\(latitude),\(longititude)&radius=1000&key=\(GOOGLEAPIKEYS.GOOGLE_API_KEY)"
        
        print(urlString)
        guard let url = URL(string: urlString) else {print("Unable to generate URL");return}
        
        let task = session.dataTask(with: url) { (responseData, urlResponse, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            guard let response = try? JSONDecoder().decode(PlaceFromAddress.self, from: responseData!)else{return}
            
            DispatchQueue.main.async {
                completition(response)
            }
            
        }
        task.resume()
    }
    
    
    func openGoogleMaps(route: CLLocation){
        let destinationLatitude = route.coordinate.latitude
        let destinationLongitude = route.coordinate.longitude
        
        //Automatically picks up the current location - So only needs destination
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps-x-callback://")!) {
            print("Can open google maps will call back")
            
            let url = URL(string: "comgooglemaps-x-callback://?daddr=\(destinationLatitude),\(destinationLongitude)&directionsmode=walking&x-success=sourceapp://?resume=true&x-source=AirApp")
            UIApplication.shared.open(url!, options: [:]) { (response) in
                print(response)
            }
        } else {
            print("Can't use comgooglemaps://");
        }
    }
    
    func reverseGeocode(location: CLLocation, completition: @escaping(ReverseGeoCode)->()){
     
        let latitude = location.coordinate.latitude
        let longititude = location.coordinate.longitude
        
        let urlString = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longititude)&location_type=ROOFTOP&result_type=street_address&key=\(GOOGLEAPIKEYS.GOOGLE_API_KEY)"
        
        guard let url = URL(string: urlString) else {print("Unable to generate URL");return}
        
        let task = session.dataTask(with: url) { (responseData, urlResponse, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            guard let response = try? JSONDecoder().decode(ReverseGeoCode.self, from: responseData!)else{return}
            
            DispatchQueue.main.async {
                completition(response)
            }
            
        }
        task.resume()
        
        
    
        
    }
    
    
    func getPlaceIdFromName(placeName: String,coordinates:CLLocation, completition: @escaping(String)->()){
        
        
        
        let latitude = coordinates.coordinate.latitude
        let longititude = coordinates.coordinate.longitude
        
        let urlString = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=\(placeName)&inputtype=textquery&key=\(GOOGLEAPIKEYS.GOOGLE_API_KEY)&locationbias=point:=\(latitude),\(longititude)"
        
        
        
        guard let url = URL(string: urlString) else {print("Unable to generate URL");return}
        
        let task = session.dataTask(with: url) { (responseData, urlResponse, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            
            guard let response = try? JSONDecoder().decode(PlaceFromName.self, from: responseData!)else{return}
            DispatchQueue.main.async {
                guard let placeId = response.candidates?.first?.place_id else {return}
                completition(placeId)
            }
            
        }
        task.resume()
        
    }
    
    
    
  
    
    

        

}


