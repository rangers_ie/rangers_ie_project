//
//  OtherAPIClient.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 30/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation


import AFNetworking

class APIClient: NSObject {
    static let sharedInstance = APIClient()
    private let session = URLSession(configuration: .default)
    private let afnManager = AFHTTPRequestOperationManager()
    
    
    func getPublicToiletsData(completition: @escaping([PublicToilet])->()){
    
        
        let urll = "https://firebasestorage.googleapis.com/v0/b/ieproject-e04dd.appspot.com/o/acc_toilet_final.json?alt=media&token=f0a75eb1-0da8-4a32-bc8a-3ffabe16e8b4"

        
        let url = URL(string: urll)
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return }
            do{
                //here dataResponse received from a network request
                let jsonResponse = try JSONSerialization.jsonObject(with:
                    dataResponse, options: [])
            
                var allToilets:[PublicToilet] = []
                if let response = jsonResponse as? [[String: Any]]{
                
                    for item in response{
                        let pubToilet = PublicToilet()
                        
                        pubToilet.name = item["name"] as? String
                        pubToilet.wheelchair = item["wheelchair"] as? String
                        pubToilet.male = item["male"] as? String
                        pubToilet.female = item["female"] as? String
                        pubToilet.lat = item["lat"] as? Double
                        pubToilet.lon = item["lon"] as? Double
                        
                        allToilets.append(pubToilet)
                        
                
                    }
                    
                    DispatchQueue.main.async {
                    
                        completition(allToilets)
                    }
                    
                }
                else{
                    print("NOP")
                    print(jsonResponse) //Response result
                }
                
                
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        task.resume()
    }
    
    
    func getBuildingInformation(completion: @escaping([Building])->()){
        let urlString = "https://data.melbourne.vic.gov.au/resource/q8hp-qgps.json"
        
        
        //let urlString = "https://firebasestorage.googleapis.com/v0/b/ieproject-e04dd.appspot.com/o/building_final.json?alt=media&token=eb34699a-cc1b-47e2-b063-6e6eadce6235"
        
        let url = URL(string: urlString)
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            do{
                let jsonDecoder = JSONDecoder()
                let responseModel = try jsonDecoder.decode(Array<Building>.self, from: data!)
                DispatchQueue.main.async {
                    completion(responseModel)
                }
            }
            catch{
                print(error)
                print(error.localizedDescription)
            }
 
            
        }
        task.resume()
        
    }
    
    
    
    
    
    func getBuildingInfoNew(completition: @escaping([BuildingNew])->()){
        let urlString = "https://firebasestorage.googleapis.com/v0/b/ieproject-e04dd.appspot.com/o/building_final.json?alt=media&token=eb34699a-cc1b-47e2-b063-6e6eadce6235"
        
        let url = URL(string: urlString)
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            do{
                let jsonDecoder = JSONDecoder()
                let responseModel = try jsonDecoder.decode(Array<BuildingNew>.self, from: data!)
                DispatchQueue.main.async {
                    //print(responseModel)
                    completition(responseModel)
                }
            }
            catch{
                print(error)
                print(error.localizedDescription)
            }
            
            
        }
        task.resume()
    }
    
    
    
    func getPedestrianInformation(completion: @escaping([Pedestrian])->()){
        let urlstring = "https://data.melbourne.vic.gov.au/resource/mxb8-wn4w.json"
        
        let url = URL(string: urlstring)
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            do{
                let jsonDecoder = JSONDecoder()
                let responseModel = try jsonDecoder.decode(Array<Pedestrian>.self, from: data!)
                //print(responseModel)
                DispatchQueue.main.async {
                    completion(responseModel)
                }
            }
            catch{
                print(error)
                print(error.localizedDescription)
            }
            
            
        }
        task.resume()
 
    }
    
    func getWeatherInformation(location: CLLocation, completion: @escaping(NewWeather)->()){
        let lats = location.coordinate.latitude
        let longs = location.coordinate.longitude
        //let urlString = "https://api.openweathermap.org/data/2.5/weather?lat=\(lats)&lon=\(longs)&APPID=\(WEATHERKEY.API_KEY)"
        
        let urlString = "https://api.darksky.net/forecast/\(DARKSKYWEATHER.APIKEY)/\(lats),\(longs)"
        
        print(urlString)
        
        guard let url = URL(string: urlString) else {print("Unable to generate URL");return}
        
        let task = URLSession.shared.dataTask(with: url) { (responceData, urlResponce, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            
            do{
                let jsonDecoder = JSONDecoder()
                let responceModel = try jsonDecoder.decode(NewWeather.self, from: responceData!)
                DispatchQueue.main.async {
                    completion(responceModel)
                }
            }
            catch{
                print(error)
                print(error.localizedDescription)
            }
            
        }
        task.resume()

    }
    //&radius=4000

    func getFourSquareData(currentLocation: CLLocationCoordinate2D, completition: @escaping(FourSquareSummary)->()){
        let urlString = "https://api.foursquare.com/v2/venues/search?ll=\(currentLocation.latitude),\(currentLocation.longitude)&v=20160607&intent=checkin&limit=1&client_id=\(FOURSQUAREKEYS.CLIENT_ID)&client_secret=\(FOURSQUAREKEYS.CLIENT_SECRET)"
        
        guard let url = URL(string: urlString) else{print("Unable to generate URL"); return}
        
        let task = URLSession.shared.dataTask(with: url) { (responceData, urlResponce, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            do{
                let jsonDecoder = JSONDecoder()
                let responceModel = try jsonDecoder.decode(FourSquareSummary.self, from: responceData!)
                DispatchQueue.main.async {
                    completition(responceModel)
                }
            }
            catch{
                print("Caught Error @ getFourSquareData \(error.localizedDescription)")
                print(error)
            }
        }
        task.resume()
    }
    
    func getMostPopularTime(venueId: String, completition: @escaping(VenueTimesFourSquare)->()){
        let urlString = "https://api.foursquare.com/v2/venues/\(venueId)/hours?client_id=\(FOURSQUAREKEYS.CLIENT_ID)&client_secret=\(FOURSQUAREKEYS.CLIENT_SECRET)&v=20160607"
        
        guard let url = URL(string: urlString) else{print("Unable to generate URL"); return}
        
        let task = URLSession.shared.dataTask(with: url) { (dataResponce, urlResponce, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            do{
                let jsonDecoder = JSONDecoder()
                let responceModel = try jsonDecoder.decode(VenueTimesFourSquare.self, from: dataResponce!)
                DispatchQueue.main.async {
                    completition(responceModel)
                }
            }
            catch{
                print("Caught error @ getMostPopularTime \(error.localizedDescription)")
                print(error)
            }
        }
        task.resume()
    }
   
    
}
