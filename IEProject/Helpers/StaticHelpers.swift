//
//  StaticHelpers.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 6/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import Foundation
import UIKit
class Helpers: NSObject{
    
    static func setCell(cell: PlaceCollectionViewCell){
        
        cell.contentView.layer.cornerRadius = 4.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        cell.layer.shadowRadius = 4.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
    
    }
}
