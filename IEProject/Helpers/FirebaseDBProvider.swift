//
//  FirebaseDBProvider.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 12/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class FirebaseDBProvider: NSObject{
    
    private static let deviceId = UIDevice.current.identifierForVendor?.uuidString
    
    private static var dbRef: DatabaseReference {
        return Database.database().reference()
    }
    
    private static var recentPlacesReference: DatabaseReference {
        return dbRef.child(FirebaseConstants.RECENT_PLACES_VISITED)
    }
    
    private static var locationFeedbackRef: DatabaseReference {
        return dbRef.child(FirebaseConstants.LOCATION_FEEDBACK)
    }
    
    private static var routeReviewRef: DatabaseReference {
        return dbRef.child(FirebaseConstants.ROUTE_REVIEW)
    }
    
    static func saveRecentlyVisited(placeId: String, locationName: String){
        
        let deviceId = UIDevice.current.identifierForVendor?.uuidString
        let placeDetails:[String: Any] = [FirebaseConstants.PLACE_ID:placeId,
                                          FirebaseConstants.PLACE_NAME:locationName]
        recentPlacesReference.child(deviceId!).child(placeId).setValue(placeDetails)
    }
    
    static func getRecentlyVisied(completition: @escaping([RecentPlaceSummary])->()){
        
        var allRecentPlaces:[RecentPlaceSummary] = []
        recentPlacesReference.child(deviceId!).observe(.childAdded) { (dataResponce) in
            
            if let data = dataResponce.value as? [String: AnyObject]{
                let recentPlace = RecentPlaceSummary()
                recentPlace.setValuesForKeys(data)
                allRecentPlaces.append(recentPlace)
                DispatchQueue.main.async {
                    completition(allRecentPlaces)
                }
                
            }
        }
    }
    
    static func saveRating(answers:[String:Any], placeId: String){
        //print(answers)
        locationFeedbackRef.child(placeId).childByAutoId().setValue(answers)
    }
    
    static func getUserRating(placeID: String, completion: @escaping([UserRatings], String)->()){
        var result:[UserRatings] = []

        locationFeedbackRef.child(placeID).observe(.value) { (snapshot) in
            if let dataResponce = snapshot.value as? [String: AnyObject]{
                print("Number of reviews \(dataResponce.count)")
                for rating in dataResponce.values{
                    if rating is [String: AnyObject]{
                        let userRating = UserRatings()
                        userRating.setValuesForKeys(rating as! [String : Any])
                        result.append(userRating)
                    }
                    else{
                        print("BAD")
                    }
                }
                DispatchQueue.main.async {
                    completion(result, FirebaseConstants.SUCESS)
                }

            }
            else{
                let userRating = UserRatings()
                completion([userRating], FirebaseConstants.FAIL)
                print("Unable to convert to dictionary")
            }

        }
        
        
    }
    
    static func saveRouteReview(review: String){
        
        routeReviewRef.childByAutoId().setValue(review)
        
    }
    
   
}
@objcMembers
class RecentPlaceSummary: NSObject{
    var PlaceId: String?
    var PlaceName: String?
}


@objcMembers
class UserRatings: NSObject{
    var Question01: String?
    var Question02: String?
    var Question03: String?
}


