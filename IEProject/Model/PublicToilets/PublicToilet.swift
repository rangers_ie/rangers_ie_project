

import Foundation

@objcMembers
class PublicToilet : NSObject {
    
	var female : String?
	var lat : Double?
	var lon : Double?
	var male : String?
	var name : String?
	var wheelchair : String?
    
}

