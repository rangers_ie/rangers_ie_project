/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Reviews : Codable {
	let author_name : String?
	let author_url : String?
	let language : String?
	let profile_photo_url : String?
	let rating : Int?
	let relative_time_description : String?
	let text : String?
	let time : Int?

	enum CodingKeys: String, CodingKey {

		case author_name = "author_name"
		case author_url = "author_url"
		case language = "language"
		case profile_photo_url = "profile_photo_url"
		case rating = "rating"
		case relative_time_description = "relative_time_description"
		case text = "text"
		case time = "time"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		author_name = try values.decodeIfPresent(String.self, forKey: .author_name)
		author_url = try values.decodeIfPresent(String.self, forKey: .author_url)
		language = try values.decodeIfPresent(String.self, forKey: .language)
		profile_photo_url = try values.decodeIfPresent(String.self, forKey: .profile_photo_url)
		rating = try values.decodeIfPresent(Int.self, forKey: .rating)
		relative_time_description = try values.decodeIfPresent(String.self, forKey: .relative_time_description)
		text = try values.decodeIfPresent(String.self, forKey: .text)
		time = try values.decodeIfPresent(Int.self, forKey: .time)
	}

}