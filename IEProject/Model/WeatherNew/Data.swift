/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct WeatherData : Codable {
	let time : Int?
	let summary : String?
	let icon : String?
	let sunriseTime : Int?
	let sunsetTime : Int?
	let moonPhase : Double?
	let precipIntensity : Double?
	let precipIntensityMax : Double?
	let precipIntensityMaxTime : Int?
	let precipProbability : Double?
	let precipType : String?
	let temperatureHigh : Double?
	let temperatureHighTime : Int?
	let temperatureLow : Double?
	let temperatureLowTime : Int?
	let apparentTemperatureHigh : Double?
	let apparentTemperatureHighTime : Int?
	let apparentTemperatureLow : Double?
	let apparentTemperatureLowTime : Int?
	let dewPoint : Double?
	let humidity : Double?
	let pressure : Double?
	let windSpeed : Double?
	let windGust : Double?
	let windGustTime : Double?
	let windBearing : Double?
	let cloudCover : Double?
	let uvIndex : Double?
	let uvIndexTime : Double?
	let visibility : Double?
	let ozone : Double?
	let temperatureMin : Double?
	let temperatureMinTime : Double?
	let temperatureMax : Double?
	let temperatureMaxTime : Double?
	let apparentTemperatureMin : Double?
	let apparentTemperatureMinTime : Double?
	let apparentTemperatureMax : Double?
	let apparentTemperatureMaxTime : Double?

	enum CodingKeys: String, CodingKey {

		case time = "time"
		case summary = "summary"
		case icon = "icon"
		case sunriseTime = "sunriseTime"
		case sunsetTime = "sunsetTime"
		case moonPhase = "moonPhase"
		case precipIntensity = "precipIntensity"
		case precipIntensityMax = "precipIntensityMax"
		case precipIntensityMaxTime = "precipIntensityMaxTime"
		case precipProbability = "precipProbability"
		case precipType = "precipType"
		case temperatureHigh = "temperatureHigh"
		case temperatureHighTime = "temperatureHighTime"
		case temperatureLow = "temperatureLow"
		case temperatureLowTime = "temperatureLowTime"
		case apparentTemperatureHigh = "apparentTemperatureHigh"
		case apparentTemperatureHighTime = "apparentTemperatureHighTime"
		case apparentTemperatureLow = "apparentTemperatureLow"
		case apparentTemperatureLowTime = "apparentTemperatureLowTime"
		case dewPoint = "dewPoint"
		case humidity = "humidity"
		case pressure = "pressure"
		case windSpeed = "windSpeed"
		case windGust = "windGust"
		case windGustTime = "windGustTime"
		case windBearing = "windBearing"
		case cloudCover = "cloudCover"
		case uvIndex = "uvIndex"
		case uvIndexTime = "uvIndexTime"
		case visibility = "visibility"
		case ozone = "ozone"
		case temperatureMin = "temperatureMin"
		case temperatureMinTime = "temperatureMinTime"
		case temperatureMax = "temperatureMax"
		case temperatureMaxTime = "temperatureMaxTime"
		case apparentTemperatureMin = "apparentTemperatureMin"
		case apparentTemperatureMinTime = "apparentTemperatureMinTime"
		case apparentTemperatureMax = "apparentTemperatureMax"
		case apparentTemperatureMaxTime = "apparentTemperatureMaxTime"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		time = try values.decodeIfPresent(Int.self, forKey: .time)
		summary = try values.decodeIfPresent(String.self, forKey: .summary)
		icon = try values.decodeIfPresent(String.self, forKey: .icon)
		sunriseTime = try values.decodeIfPresent(Int.self, forKey: .sunriseTime)
		sunsetTime = try values.decodeIfPresent(Int.self, forKey: .sunsetTime)
		moonPhase = try values.decodeIfPresent(Double.self, forKey: .moonPhase)
		precipIntensity = try values.decodeIfPresent(Double.self, forKey: .precipIntensity)
		precipIntensityMax = try values.decodeIfPresent(Double.self, forKey: .precipIntensityMax)
		precipIntensityMaxTime = try values.decodeIfPresent(Int.self, forKey: .precipIntensityMaxTime)
		precipProbability = try values.decodeIfPresent(Double.self, forKey: .precipProbability)
		precipType = try values.decodeIfPresent(String.self, forKey: .precipType)
		temperatureHigh = try values.decodeIfPresent(Double.self, forKey: .temperatureHigh)
		temperatureHighTime = try values.decodeIfPresent(Int.self, forKey: .temperatureHighTime)
		temperatureLow = try values.decodeIfPresent(Double.self, forKey: .temperatureLow)
		temperatureLowTime = try values.decodeIfPresent(Int.self, forKey: .temperatureLowTime)
		apparentTemperatureHigh = try values.decodeIfPresent(Double.self, forKey: .apparentTemperatureHigh)
		apparentTemperatureHighTime = try values.decodeIfPresent(Int.self, forKey: .apparentTemperatureHighTime)
		apparentTemperatureLow = try values.decodeIfPresent(Double.self, forKey: .apparentTemperatureLow)
		apparentTemperatureLowTime = try values.decodeIfPresent(Int.self, forKey: .apparentTemperatureLowTime)
		dewPoint = try values.decodeIfPresent(Double.self, forKey: .dewPoint)
		humidity = try values.decodeIfPresent(Double.self, forKey: .humidity)
		pressure = try values.decodeIfPresent(Double.self, forKey: .pressure)
		windSpeed = try values.decodeIfPresent(Double.self, forKey: .windSpeed)
		windGust = try values.decodeIfPresent(Double.self, forKey: .windGust)
		windGustTime = try values.decodeIfPresent(Double.self, forKey: .windGustTime)
		windBearing = try values.decodeIfPresent(Double.self, forKey: .windBearing)
		cloudCover = try values.decodeIfPresent(Double.self, forKey: .cloudCover)
		uvIndex = try values.decodeIfPresent(Double.self, forKey: .uvIndex)
		uvIndexTime = try values.decodeIfPresent(Double.self, forKey: .uvIndexTime)
		visibility = try values.decodeIfPresent(Double.self, forKey: .visibility)
		ozone = try values.decodeIfPresent(Double.self, forKey: .ozone)
		temperatureMin = try values.decodeIfPresent(Double.self, forKey: .temperatureMin)
		temperatureMinTime = try values.decodeIfPresent(Double.self, forKey: .temperatureMinTime)
		temperatureMax = try values.decodeIfPresent(Double.self, forKey: .temperatureMax)
		temperatureMaxTime = try values.decodeIfPresent(Double.self, forKey: .temperatureMaxTime)
		apparentTemperatureMin = try values.decodeIfPresent(Double.self, forKey: .apparentTemperatureMin)
		apparentTemperatureMinTime = try values.decodeIfPresent(Double.self, forKey: .apparentTemperatureMinTime)
		apparentTemperatureMax = try values.decodeIfPresent(Double.self, forKey: .apparentTemperatureMax)
		apparentTemperatureMaxTime = try values.decodeIfPresent(Double.self, forKey: .apparentTemperatureMaxTime)
	}

}
