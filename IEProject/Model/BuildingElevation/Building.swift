/*
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Building : Codable {
    let accessibility_rating : String?
    let accessibility_type : String?
    let accessibility_type_description : String?
    let base_property_id : String?
    let block_id : String?
    let building_height_highest_floor : String?
    let census_year : String?
    let construction_year : String?
    let location : Locationn?
    let predominant_space_use : String?
    let property_id : String?
    let refurbished_year : String?
    let street_address : String?
    let suburb : String?
    let x_coordinate : String?
    let y_coordinate : String?

    enum CodingKeys: String, CodingKey {

        case accessibility_rating = "accessibility_rating"
        case accessibility_type = "accessibility_type"
        case accessibility_type_description = "accessibility_type_description"
        case base_property_id = "base_property_id"
        case block_id = "block_id"
        case building_height_highest_floor = "building_height_highest_floor"
        case census_year = "census_year"
        case construction_year = "construction_year"
        case location = "location"
        case predominant_space_use = "predominant_space_use"
        case property_id = "property_id"
        case refurbished_year = "refurbished_year"
        case street_address = "street_address"
        case suburb = "suburb"
        case x_coordinate = "x_coordinate"
        case y_coordinate = "y_coordinate"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        accessibility_rating = try values.decodeIfPresent(String.self, forKey: .accessibility_rating)
        accessibility_type = try values.decodeIfPresent(String.self, forKey: .accessibility_type)
        accessibility_type_description = try values.decodeIfPresent(String.self, forKey: .accessibility_type_description)
        base_property_id = try values.decodeIfPresent(String.self, forKey: .base_property_id)
        block_id = try values.decodeIfPresent(String.self, forKey: .block_id)
        building_height_highest_floor = try values.decodeIfPresent(String.self, forKey: .building_height_highest_floor)
        census_year = try values.decodeIfPresent(String.self, forKey: .census_year)
        construction_year = try values.decodeIfPresent(String.self, forKey: .construction_year)
        location = try values.decodeIfPresent(Locationn.self, forKey: .location)
        predominant_space_use = try values.decodeIfPresent(String.self, forKey: .predominant_space_use)
        property_id = try values.decodeIfPresent(String.self, forKey: .property_id)
        refurbished_year = try values.decodeIfPresent(String.self, forKey: .refurbished_year)
        street_address = try values.decodeIfPresent(String.self, forKey: .street_address)
        suburb = try values.decodeIfPresent(String.self, forKey: .suburb)
        x_coordinate = try values.decodeIfPresent(String.self, forKey: .x_coordinate)
        y_coordinate = try values.decodeIfPresent(String.self, forKey: .y_coordinate)
    }

}
