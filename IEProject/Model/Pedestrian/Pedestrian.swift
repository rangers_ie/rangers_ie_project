/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Pedestrian : Codable {
	let daet_time : String?
	let day : String?
	let id : String?
	let mdate : String?
	let month : String?
	let qv_market_peel_st : String?
	let sensor_id : String?
	let sensor_name : String?
	let time : String?
	let year : String?

	enum CodingKeys: String, CodingKey {

		case daet_time = "daet_time"
		case day = "day"
		case id = "id"
		case mdate = "mdate"
		case month = "month"
		case qv_market_peel_st = "qv_market_peel_st"
		case sensor_id = "sensor_id"
		case sensor_name = "sensor_name"
		case time = "time"
		case year = "year"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		daet_time = try values.decodeIfPresent(String.self, forKey: .daet_time)
		day = try values.decodeIfPresent(String.self, forKey: .day)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		mdate = try values.decodeIfPresent(String.self, forKey: .mdate)
		month = try values.decodeIfPresent(String.self, forKey: .month)
		qv_market_peel_st = try values.decodeIfPresent(String.self, forKey: .qv_market_peel_st)
		sensor_id = try values.decodeIfPresent(String.self, forKey: .sensor_id)
		sensor_name = try values.decodeIfPresent(String.self, forKey: .sensor_name)
		time = try values.decodeIfPresent(String.self, forKey: .time)
		year = try values.decodeIfPresent(String.self, forKey: .year)
	}

}
