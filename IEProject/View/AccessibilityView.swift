//
//  AccessibilityView.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 13/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class AccessibilityView: SuperView {
    
    
    let accessibility_rating_label:UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.backgroundColor = UIColor.blue
        return label
    }()
    
    let accessibility_type_title: UILabel = {
        let label = UILabel()
        label.text = "Accessibility Type"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    let accessibility_type_label : UILabel = {
        let label = UILabel()
        label.text = "This is only for wheel chair stuff "
        return label
    }()
    
    let accessibility_type_description_title: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.text = "Accessibility Type Description"
        return label
    }()
    
    let accessibility_type_description_label: UITextView = {
        let label = UITextView()
        label.isEditable = false
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textAlignment = .justified
        label.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse"
        return label
    }()
    
    override func setUpView() {
        super.setUpView()
        addSubview(accessibility_rating_label)
        
        addSubview(accessibility_type_title)
        addSubview(accessibility_type_label)
        
        addSubview(accessibility_type_description_title)
        addSubview(accessibility_type_description_label)
        
        accessibility_rating_label.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 40, left: 0, bottom: 0, right: 20), size: .init(width: 50, height: 50))
        
        accessibility_type_title.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 40, left: 20, bottom: 0, right: 0))
        
        accessibility_type_label.anchor(top: accessibility_type_title.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        accessibility_type_description_title.anchor(top: accessibility_type_label.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        accessibility_type_description_label.anchor(top: accessibility_type_description_title.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 10))
        
        
        
        
        
        
    }

}
