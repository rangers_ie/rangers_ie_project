//
//  FeedbackView.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 16/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class FeedbackView: SuperView {
    

    let titleTable: UILabel = {
        let label = UILabel()
        label.text = "Tell us what you think"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.textAlignment = .center
        return label
    }()

    override func setUpView() {
        super.setUpView()
        addSubview(titleTable)
        titleTable.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 20, left: 10, bottom: 0, right: 10))
    }

}
