//
//  HeaderViewTabelView.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 10/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class HeaderViewTabelView: SuperView {
    
    let distanceValue: UILabel = {
        let label = UILabel()
        label.text = "Distance"
        label.textColor = UIColor.gray
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        return label
    }()
    
    let routeSummary: UILabel = {
        let label = UILabel()
        label.text = "DURATION"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    let durationValue: UILabel = {
        let label = UILabel()
        label.text = "10.8"
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        return label
    }()
    
    let cautionImage: UIButton = {
        let btn = UIButton(type: .system)
        let image = UIImage(named: "caution")?.withRenderingMode(.alwaysOriginal)
        btn.setImage(image, for: .normal)
        return btn

    }()
    
    let elevationLabel: UILabel = {
        let label = UILabel()
        label.text = "awdawdawdawdawdadawdawd"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    let seperatorLine: UIView = {
        let lineView = UIView()
        lineView.backgroundColor = UIColor.lightGray
        return lineView
    }()
    
    let timeCaution: UIButton = {
        let btn = UIButton(type: .system)
        let image = UIImage(named: "infoFill")
        btn.setImage(image, for: .normal)
        return btn
        
    }()
    
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        addSubview(distanceValue)
        addSubview(routeSummary)
        addSubview(durationValue)
        addSubview(cautionImage)
        addSubview(elevationLabel)
        addSubview(seperatorLine)
        addSubview(timeCaution)
        
        durationValue.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding:.init(top: 20, left: 20, bottom: 0, right: 0))
        
        distanceValue.anchor(top: topAnchor, leading: durationValue.trailingAnchor, bottom: nil, trailing: nil, padding: .init(top: 20, left: 20, bottom: 0, right: 0))
        
        timeCaution.anchor(top: topAnchor, leading: distanceValue.trailingAnchor, bottom: nil, trailing: nil, padding: .init(top: 30, left: 10, bottom: 0, right: 0))
        
        routeSummary.anchor(top: durationValue.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        elevationLabel.anchor(top: routeSummary.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        cautionImage.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 20))
        
        seperatorLine.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .zero, size: CGSize(width: 0, height: 1))
        
        
    }

}
