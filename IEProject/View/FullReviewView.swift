//
//  FullReviewView.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 18/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class FullReviewView: SuperView {
    
    let reviewText: UILabel = {
        let label = UILabel()
        label.text = "Sample"
        label.numberOfLines = 0
        return label
    }()
    
    let cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Go back", for: .normal)
        return button
    }()

    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        addSubview(reviewText)
        addSubview(cancelButton)
        reviewText.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 0, left: 20, bottom: 0, right: 20))
        cancelButton.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
    }

}
