//
//  AccessibilityInfoTableViewCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 15/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class AccessibilityInfoTableViewCell: UITableViewCell {
    
    let rating: UILabel = {
        let label = UILabel()
        label.text = "3.1"
        label.backgroundColor = UIColor(displayP3Red: 0, green: 122/255, blue: 1, alpha: 1)
        label.textColor = UIColor.white
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textAlignment = .center
        label.layer.cornerRadius = 12.5
        label.layer.masksToBounds = true
        return label
    }()
    
    let ratingLabel:UILabel = {
        let label = UILabel()
        label.text = "Accessibility Rating"
        label.textColor = UIColor.lightGray
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    let buildingName: UILabel = {
        let label = UILabel()
        label.text = "Name Unavailable"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    let type: UILabel = {
        let label = UILabel()
        label.text = ""
        return label
    }()
    
    let typeDesc: UILabel = {
        let label = UILabel()
        label.text = ""
        return label
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(buildingName)
        addSubview(rating)
        addSubview(type)
        //addSubview(typeDesc)
        addSubview(ratingLabel)
        
        ratingLabel.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 20))
        
        rating.anchor(top: ratingLabel.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 50), size: .init(width: 50, height: 25))
        
        
        buildingName.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 30, left: 20, bottom: 0, right: 0 ))
        type.anchor(top: buildingName.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 5, left: 20, bottom: 0, right: 0))
        //typeDesc.anchor(top: type.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 5, left: 20, bottom: 0, right: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
