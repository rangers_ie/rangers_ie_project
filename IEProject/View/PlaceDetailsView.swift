//
//  PlaceDetailsView.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 30/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class PlaceDetailsView: SuperView {
    
    let imageView: UIImageView = {
        let iv = UIImageView(image: UIImage(named: "cafePicTwo"))
        iv.contentMode = .scaleToFill
        iv.backgroundColor = UIColor.green.withAlphaComponent(0.3)
        //iv.layer.shouldRasterize = true
        return iv
    }()
    
    let blackTransparentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        return view
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Name Goes Here"
        label.textColor = UIColor.white
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        return label
    }()
    
    let nameLine: UIView = {
        let line = UIView()
        line.backgroundColor = UIColor.lightGray
        return line
    }()
    
    let address: UILabel = {
        let label = UILabel()
        label.text = "The address goes here"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.numberOfLines = 0
        return label
    }()
    
    
    
    let phoneNumber: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Phone Number", for: .normal)
        return button
    }()
    
    let ratingLabel: UILabel = {
        let ratingLabel = UILabel()
        ratingLabel.text = "3.1"
        ratingLabel.backgroundColor = UIColor.green.withAlphaComponent(0.5)
        ratingLabel.textAlignment = .center
        ratingLabel.textColor = UIColor.gray
        return ratingLabel
    }()
    
    let backButton: UIButton = {
        let backButton = UIButton(type: .system)
        backButton.setImage(UIImage(named: "backButton")?.withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.tintColor = UIColor.white
        return backButton
    }()
    
    let populatTimeAlert: UILabel = {
        let label = UILabel()
        label.text = "This venue is busy at the moment"
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        return label
    }()
    
    let checkBusyTimes: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Check busy hours", for: .normal)
        return button
    }()
    
    //    let accessibilityInfoTitle: UILabel = {
    //        let label = UILabel()
    //        label.text = "Accessibility Information"
    //        label.font = UIFont.preferredFont(forTextStyle: .body)
    //        return label
    //    }()
    //
    //    let sadImage: UIImageView = {
    //        let image = UIImage(named: "sad")
    //        let iv = UIImageView(image: image)
    //        iv.contentMode = .scaleAspectFit
    //        return iv
    //    }()
    //
    //    let accessLabel: UILabel = {
    //        let label = UILabel()
    //        label.text = "Test"
    //        label.textAlignment = .center
    //        label.numberOfLines = 0
    //        label.lineBreakMode = .byWordWrapping
    //        label.font = UIFont.preferredFont(forTextStyle: .caption1)
    //        return label
    //    }()
    
    let reviewButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Google Reviews", for:.normal)
        return button
    }()
    
    let leaveReviewButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Leave your review", for:.normal)
        return button
    }()
    
    let weatherButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "weather"), for: .normal)
        button.setTitle("Weather", for: .normal)
        //button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        //button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 3)
        return button
    }()
    
    let reviewLabel: UILabel = {
        let label = UILabel()
        label.text = "Reviews"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    
    let reviewLine: UIView = {
        let line = UIView()
        line.backgroundColor = UIColor.lightGray
        return line
    }()
    
    let rampAccessLabel: UILabel = {
        let label = UILabel()
        label.text = "50% of guest said this place has ramp access"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    let adequareSpaceLabel: UILabel = {
        let label = UILabel()
        label.text = "50% of guest said this place has enough room"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    let friendlyStaffLabel: UILabel = {
        let label = UILabel()
        label.text = "50% of guest said the staff is friendly"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    let reviewInfoButton: UIButton = {
        let button = UIButton(type: .system)
        let image = UIImage(named: "information")
        button.setImage(image, for: .normal)
        return button
        
    }()
    
    
    
    let noReviewsLabel: UILabel = {
        let label = UILabel()
        label.text = "It seems like we have no reviews for this place. \n Please feel free to leave your review \n if you have visted this venue."
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isHidden = true
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        addSubview(nameLine)
        addSubview(imageView)
        addSubview(blackTransparentView)
        addSubview(nameLabel)
        addSubview(address)
        addSubview(phoneNumber)
        addSubview(backButton)
        //        addSubview(accessibilityInfoTitle)
        //        addSubview(accessLabel)
        addSubview(reviewButton)
        //        addSubview(sadImage)
        addSubview(leaveReviewButton)
        addSubview(weatherButton)
        addSubview(checkBusyTimes)
        addSubview(populatTimeAlert)
        addSubview(reviewLabel)
        addSubview(reviewLine)
        addSubview(rampAccessLabel)
        addSubview(adequareSpaceLabel)
        addSubview(friendlyStaffLabel)
        addSubview(reviewInfoButton)
        addSubview(noReviewsLabel)
        
    
        
        
        backButton.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 20, left: 8, bottom: 0, right: 0))
        
        imageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .zero, size: CGSize(width: 0, height: 250))
        
        blackTransparentView.anchor(top: nil, leading: leadingAnchor, bottom: imageView.bottomAnchor, trailing: trailingAnchor, padding: .zero, size: .init(width: 0, height: 50))
        
        nameLabel.anchor(top: blackTransparentView.topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        address.anchor(top: nameLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 20, left: 20, bottom: 0, right: 0))
        
        weatherButton.anchor(top: address.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 10))
        
        
        phoneNumber.anchor(top: address.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 20, left:20, bottom: 0, right: 0))
        
        
        nameLine.anchor(top: phoneNumber.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 60, left: 20, bottom: 0, right: 20), size: .init(width: 0, height: 1))
        
        populatTimeAlert.anchor(top: nameLine.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 20, left: 20, bottom: 0, right: 0))
        
        checkBusyTimes.anchor(top: populatTimeAlert.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        //        accessibilityInfoTitle.anchor(top: nameLine.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        //
        //        sadImage.anchor(top: accessibilityInfoTitle.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 20, left: frame.width/2, bottom: 0, right: 0))
        //
        //        accessLabel.anchor(top: sadImage.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 10))
        //

    
        reviewLabel.anchor(top: checkBusyTimes.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        
        leaveReviewButton.anchor(top: checkBusyTimes.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 20))
        reviewLine.anchor(top: reviewLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 5, left: 20, bottom: 0, right: 20), size: .init(width: 0, height: 1))
        
        rampAccessLabel.anchor(top: reviewLine.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        reviewInfoButton.anchor(top: reviewLine.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 10))
        
        adequareSpaceLabel.anchor(top: rampAccessLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        friendlyStaffLabel.anchor(top: adequareSpaceLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        reviewButton.anchor(top: friendlyStaffLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        noReviewsLabel.anchor(top: reviewLine.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 20, left: 20, bottom: 0, right: 20))
        
    }
    
}
