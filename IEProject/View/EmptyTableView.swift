//
//  EmptyTableView.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 29/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class EmptyTableView: SuperView {
    
    let label: UILabel = {
        let label = UILabel()
        label.text = "Please select a tram route \n to display the stops available"
        label.numberOfLines = 0
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.textAlignment = .center
        return label
    }()
    
    override func setUpView() {
        super.setUpView()
        addSubview(label)
        label.centerAnchor(centerX: centerXAnchor, centerY: centerYAnchor)
    }

}
