//
//  ButtonPanel.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 6/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import UIButton_MiddleAligning

class ButtonPanel: SuperView {
    
    
    let viewReviewButton: CenteredButton = {
        let button = CenteredButton(type: .system)
        let image = UIImage(named: "allReviews")?.withRenderingMode(.alwaysOriginal)
        button.setTitle("Reviews", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        button.setImage(image, for: .normal)
  
        
        return button
    }()
    
    let leaveReviewButton: CenteredButton = {
        let button = CenteredButton(type: .system)
        let image = UIImage(named: "leaveReview")?.withRenderingMode(.alwaysOriginal)
        button.setImage(image, for: .normal)
        button.setTitle("Feedback", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        return button
    }()
    
    let busyHourButton: CenteredButton = {
        let button = CenteredButton(type: .system)
        let image = UIImage(named: "busyHours")?.withRenderingMode(.alwaysOriginal)
        button.setTitle("Hours", for: .normal)
        button.setImage(image, for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        return button
    }()
    
    
    let callButton: CenteredButton = {
        let button = CenteredButton(type: .system)
        let image = UIImage(named: "phoneIcon")?.withRenderingMode(.alwaysTemplate)
        button.setTitle("Call", for: .normal)
        button.setImage(image, for: .normal)
        let colour = UIColor(hex: "007aff")
        button.tintColor = colour
        button.setTitleColor(colour, for: .normal)
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        return button
    }()
    
    let toiletButton: CenteredButton = {
        let button = CenteredButton(type: .system)
        let image = UIImage(named: "publicToilet")?.withRenderingMode(.alwaysOriginal)
        button.setTitle("Toilets", for: .normal)
        button.setImage(image, for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        return button
    }()
    
    
    let weather: CenteredButton = {
        let button = CenteredButton(type: .system)
        let image = UIImage(named: "weather")?.withRenderingMode(.alwaysOriginal)
        button.setTitle("Weather", for: .normal)
        button.setImage(image, for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        return button
    }()
    
    
    let stackView: UIStackView = {
        let sv = UIStackView(frame: .zero)
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        sv.alignment = .fill
        sv.spacing = 5
        return sv
    }()
    
    
    
    

    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white

        stackView.addArrangedSubview(viewReviewButton)
        stackView.addArrangedSubview(leaveReviewButton)
        stackView.addArrangedSubview(busyHourButton)
        stackView.addArrangedSubview(toiletButton)
        stackView.addArrangedSubview(weather)
        stackView.addArrangedSubview(callButton)
        
        addSubview(stackView)
        stackView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        

        
        
        

 
 
        
        
        
    }

}
