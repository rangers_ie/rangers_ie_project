//
//  DetailView.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 4/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class DetailView: SuperView {
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        let image = UIImage(named: "cafePicTwo")
        imageView.image = image
        return imageView
    }()
    
    let placeName: UILabel = {
        let label = UILabel()
        label.text = "Place Name"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.numberOfLines = 0
        return label
    }()
    
    let accessibleRamp: UILabel = {
        let label = UILabel()
        label.text = "Wheelchair Accessible"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    let address: UILabel = {
        let label = UILabel()
        label.text = "The address goes here"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        label.numberOfLines = 0
        label.textColor = UIColor(red: 142, green: 142, blue: 147)
        return label
    }()
    
    let ratingLabel: UILabel = {
        let label = UILabel()
        label.text = "4.1"
        label.layer.cornerRadius = 5
        label.layer.masksToBounds = true
        label.backgroundColor = UIColor.green
        label.textAlignment = .center
        label.textColor = UIColor.white
        return label
    }()
    
    let seperatorLineOne: UIView = {
        let line = UIView()
        line.backgroundColor = UIColor.lightGray
        return line
    }()
    
    
    let seperatorLineTwo: UIView = {
        let line = UIView()
        line.backgroundColor = UIColor.lightGray
        return line
    }()
    
    let seperatorLineThree: UIView = {
        let line = UIView()
        line.backgroundColor = UIColor.lightGray
        return line
    }()
    
    let transportOptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Transport Options"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    let transportOptionDesc: UILabel = {
        let label = UILabel()
        label.text = "Tram and Metro sations closest to Chinger"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        label.textColor = UIColor(red: 142, green: 142, blue: 147)
        label.numberOfLines = 0
        return label
    }()
    
    let trainsButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Trains", for: .normal)
        button.layer.shadowColor = UIColor.lightGray.cgColor
        button.backgroundColor = UIColor(red: 0, green: 122, blue: 255)
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.shadowOpacity = 0.5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        return button
    }()
    
    let tramButtons: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Trams", for: .normal)
        button.layer.shadowColor = UIColor.lightGray.cgColor
        button.backgroundColor = UIColor(red: 76, green: 217, blue: 100)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.shadowOpacity = 0.5
        return button
    }()
    
    
    let checkBusyTimes: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Check busy hours", for: .normal)
        return button
    }()
    
    let ratingDesc: UILabel = {
        let rating = UILabel()
        rating.text = "Very good"
        rating.font = UIFont.preferredFont(forTextStyle: .caption1)
        rating.textColor = UIColor(red: 142, green: 142, blue: 147)
        return rating
    }()
    
    let accessibilityInfoTitile: UILabel = {
        let label = UILabel()
        label.text = "More information about Nott"
        label.numberOfLines = 0
        //label.textAlignment = .center
        //label.font = UIFont.preferredFont(forTextStyle: .caption2)
        return label
    }()
    
    
    let rampAccessLabel: UILabel = {
        let label = UILabel()
        label.text = "50% of guest said this place has ramp access"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    let adequareSpaceLabel: UILabel = {
        let label = UILabel()
        label.text = "50% of guest said this place has enough room"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    let friendlyStaffLabel: UILabel = {
        let label = UILabel()
        label.text = "50% of guest said the staff is friendly"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    let reviewInfoButton: UIButton = {
        let button = UIButton(type: .system)
        let image = UIImage(named: "information")
        button.setImage(image, for: .normal)
        return button
        
    }()
    
    let noReviewsLabel: UILabel = {
        let label = UILabel()
        label.text = "It seems like we have no reviews for this place. \n Please feel free to leave your review \n if you have visted this venue."
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isHidden = true
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    let buttonPanel: ButtonPanel = {
        let view = ButtonPanel()
        return view
    }()
    
    let getDirectionsButton: CenteredButton = {
        let button = CenteredButton(type: .system)
        let image = UIImage(named: "googleMaps")
        button.setImage(image, for: .normal)
        button.setTitle("Directions", for: .normal)
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        
        return button
    }()
    
    let backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "backButton")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = UIColor.white
        return button
    }()
    
    override func setUpView() {
        super.setUpView()
        addSubview(imageView)
        addSubview(ratingLabel)
        addSubview(placeName)
        addSubview(address)
        addSubview(accessibleRamp)
        addSubview(seperatorLineOne)
        addSubview(accessibilityInfoTitile)
        addSubview(rampAccessLabel)
        addSubview(adequareSpaceLabel)
        addSubview(friendlyStaffLabel)
        addSubview(reviewInfoButton)
        addSubview(transportOptionLabel)
        addSubview(transportOptionDesc)
        addSubview(trainsButton)
        addSubview(tramButtons)
        addSubview(checkBusyTimes)
        addSubview(ratingDesc)
        addSubview(seperatorLineTwo)
        addSubview(noReviewsLabel)
        addSubview(buttonPanel)
        addSubview(seperatorLineThree)
        addSubview(getDirectionsButton)
        addSubview(backButton)
        
        backButton.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 20, left: 10, bottom: 0, right: 0))
    
        imageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .zero, size: .init(width: 0, height: 270))
        
        placeName.anchor(top: imageView.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0), size: .init(width: 300, height: 0))
        
        accessibleRamp.anchor(top: placeName.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        ratingLabel.anchor(top: imageView.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 15), size: .init(width: 35, height: 20))
        
         ratingDesc.anchor(top: ratingLabel.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 15))
        
        address.anchor(top: accessibleRamp.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        seperatorLineOne.anchor(top: address.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 50, left: 10, bottom: 0, right: 10), size: .init(width: 0, height: 1))
        
        accessibilityInfoTitile.anchor(top: seperatorLineOne.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0), size: .init(width: 300, height: 0))
        
        noReviewsLabel.anchor(top: accessibilityInfoTitile.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        rampAccessLabel.anchor(top: accessibilityInfoTitile.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 30, bottom: 0, right: 0))
        
        reviewInfoButton.anchor(top: seperatorLineOne.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 10))
        
        adequareSpaceLabel.anchor(top: rampAccessLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 30, bottom: 0, right: 0))
        
        friendlyStaffLabel.anchor(top: adequareSpaceLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 30, bottom: 0, right: 0))
        
        seperatorLineTwo.anchor(top: friendlyStaffLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 10, bottom: 0, right: 10), size: .init(width: 0, height: 1))
        
        buttonPanel.anchor(top: seperatorLineTwo.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 5, left: 10, bottom: 0, right: 10), size: .init(width: 0, height: 40))
        
        seperatorLineThree.anchor(top: buttonPanel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 10, bottom: 0, right: 10), size: .init(width: 0, height: 1))
        
        getDirectionsButton.anchor(top: seperatorLineThree.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0))
        
        transportOptionLabel.anchor(top: seperatorLineThree.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
    
        transportOptionDesc.anchor(top: transportOptionLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 0, left: 20, bottom: 0, right: 0),size: .init(width: 300, height: 0))
        
        trainsButton.anchor(top: getDirectionsButton.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0), size: .init(width: 160, height: 30))
        
        tramButtons.anchor(top: getDirectionsButton.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 20), size: .init(width: 160, height: 30))
        
        
        
        
        
       
    }

}
