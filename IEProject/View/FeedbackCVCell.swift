//
//  FeedbackCVCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 16/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class FeedbackCVCell: SuperCollectionViewCell {
    
    let question: UILabel = {
        let label = UILabel()
        label.text = "What you think of the place?"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        
        return label
    }()
    
    let thumbsUpButton: UIButton = {
        let button = UIButton(type: .custom)
        let image = UIImage(named: "thumpsUp")?.withRenderingMode(.alwaysTemplate)
        let selectedImage = UIImage(named: "thumbsUpSelected")?.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.setImage(selectedImage, for: .selected)
        return button
    }()
    
    
    let thumbsDownButton: UIButton = {
        let button = UIButton(type: .custom)
        let image = UIImage(named: "thumbsDown")?.withRenderingMode(.alwaysTemplate)
        let selectedImage = UIImage(named: "thumbsDownSlected")?.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.setImage(selectedImage, for: .selected)
        return button
    }()
    
    let takeBackButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Exit", for: .normal)
        let colour = UIColor(red: 255, green: 59, blue: 48)
        button.setTitleColor(UIColor.red, for: .normal)
        //button.isHidden = true
        return button
    }()
    
    let cellCount: UILabel = {
        let label = UILabel()
        label.text = "1/3"
        return label
    }()

    override func setUpView() {
        super.setUpView()
        addSubview(question)
        addSubview(thumbsUpButton)
        addSubview(thumbsDownButton)
        addSubview(takeBackButton)
        addSubview(cellCount)
        
        backgroundColor = .white
        
        question.anchor(top: safeAreaLayoutGuide.topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 75, left: 0, bottom: 0, right: 0))
        thumbsUpButton.anchor(top: question.topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 200, left: 50, bottom: 0, right: 0))
        thumbsDownButton.anchor(top: question.topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 200, left: 0, bottom: 0, right: 50))
        takeBackButton.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 0, left: 0, bottom: 50, right: 0))
        
        cellCount.centerAnchor(centerX: centerXAnchor, centerY: centerYAnchor, size: .init(width: 0, height: 0), padding: .init(top: 20, left: 0, bottom: 0, right: 0))
        
        
        
        
        
    }
}
