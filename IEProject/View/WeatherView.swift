//
//  WeatherView.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 17/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class WeatherView: SuperView {
    
    let temperatuteTabel: UILabel = {
        let label = UILabel()
        label.text = "Temperature"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textAlignment = .center
        return label
    }()
    
    let windSpeed: UILabel = {
        let label = UILabel()
        label.text = "Temperature"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textAlignment = .center
        return label
    }()
    
    let visibility: UILabel = {
        let label = UILabel()
        label.text = "Temperature"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textAlignment = .center
        return label
    }()
    
    let humidity: UILabel = {
        let label = UILabel()
        label.text = "Temperature"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textAlignment = .center
        return label
    }()
    
    let timeTaken: UILabel = {
        let label = UILabel()
        label.text = "Temperature"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        
        addSubview(temperatuteTabel)
        addSubview(windSpeed)
        addSubview(visibility)
        addSubview(humidity)
        addSubview(timeTaken)
        
        temperatuteTabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 20, left: 10, bottom: 0, right: 10))
        windSpeed.anchor(top: temperatuteTabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding:.init(top: 20, left: 10, bottom: 0, right: 10))
        visibility.anchor(top: windSpeed.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding:.init(top: 20, left: 10, bottom: 0, right: 10))
        humidity.anchor(top: visibility.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding:.init(top: 20, left: 10, bottom: 0, right: 10))
        
        timeTaken.anchor(top: humidity.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding:.init(top: 20, left: 10, bottom: 0, right: 10))
    }

}
