//
//  PlaceCollectionViewCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 27/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class PlaceCollectionViewCell: SuperCollectionViewCell {
    
    let placeName: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    let distanceFromYouLabel: UILabel = {
        let label = UILabel()
        label.text = "Distance from you"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = UIColor(hex: "eff0f5")
        label.textAlignment = .center
        return label
    }()
    
    let imageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "cafePicOne"))
        imageView.contentMode = .scaleToFill
        imageView.backgroundColor = UIColor.gray
        return imageView
    }()
    
    let blackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        return view
    }()
    

    
    
    override func setUpView() {
        super.setUpView()
        
        addSubview(imageView)
        addSubview(blackView)
        addSubview(placeName)
        addSubview(distanceFromYouLabel)
        backgroundColor = UIColor.white
        
        
//        imageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 10, bottom: 10, right: 0), size: CGSize(width: 200, height: 120))
//
//        placeName.anchor(top: topAnchor, leading: imageView.trailingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 40, left: 10, bottom: 0, right: 0))
//
//        distanceFromYouLabel.anchor(top: placeName.bottomAnchor, leading: imageView.trailingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 10, bottom: 0, right: 0))
        
        
        imageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 10, left: 10, bottom: 10, right: 10))
        blackView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 10, left: 10, bottom: 10, right: 10))
        
        placeName.centerAnchor(centerX: centerXAnchor, centerY: centerYAnchor, size: .init(width: 300, height: 50), padding: .zero)
        distanceFromYouLabel.anchor(top: placeName.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 0))
        

    
    }
    
   
  
}


