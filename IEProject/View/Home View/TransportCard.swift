//
//  TransportCard.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 4/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class TransportCard: SuperView {
    

    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Trains and Trams around me"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        label.textColor = UIColor.black
        return label
    }()
    
    let trainsButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Trains", for: .normal)
        button.layer.shadowColor = UIColor.lightGray.cgColor
        button.backgroundColor = UIColor(red: 0, green: 122, blue: 255)
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.shadowOpacity = 0.5
        button.layer.cornerRadius = 4
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        return button
    }()
    
    let tramButtons: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Trams", for: .normal)
        button.layer.shadowColor = UIColor.lightGray.cgColor
        button.backgroundColor = UIColor(red: 76, green: 217, blue: 100)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.shadowOpacity = 0.5
        button.layer.cornerRadius = 4
        return button
    }()
    
    let stackView: UIStackView = {
        let sv = UIStackView(frame: .zero)
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        sv.alignment = .fill
        sv.spacing = 5
        return sv
    }()
    
    let infoButton: UIButton = {
        let image = UIImage(named: "infoFill")
        let button = UIButton(type: .custom)
        button.setImage(image, for: UIControlState.normal)
        return button
    }()
    
    
    override func setUpView() {
        super.setUpView()
        addSubview(titleLabel)
        addSubview(trainsButton)
        addSubview(tramButtons)
        addSubview(infoButton)
        addSubview(stackView)
        backgroundColor = UIColor.white
        titleLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        infoButton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 5))
        
        stackView.anchor(top: titleLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 20), size: .init(width: 0, height: 30))
        
        stackView.addArrangedSubview(tramButtons)
        stackView.addArrangedSubview(trainsButton)
        
        //tramButtons.anchor(top: titleLabel.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 20), size: .init(width: 180, height: 30))
        
    }

}
