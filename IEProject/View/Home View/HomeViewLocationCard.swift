//
//  NewHomeView.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 3/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class HomeViewLocationCard: SuperView {
    
    let locationLabel: UILabel = {
        let label = UILabel()
        label.text = "YOUR LOCATION"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        label.textColor = UIColor(red: 142, green: 142, blue: 147)
        return label
    }()
    
    let locationPin: UIImageView = {
        let imagePin = UIImage(named: "locationMarker")
        let iv = UIImageView(image: imagePin)
        return iv
    }()
    
    let locationName: UILabel = {
        let label = UILabel()
        label.text = "Location Here"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    let changeLocationButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("CHANGE", for: .normal)
        let colour = UIColor(red: 0, green: 122, blue: 255)
        button.setTitleColor(colour, for: .normal)
        return button
    }()
    
//    let searchTextField: UITextField = {
//        let tf = UITextField()
//        tf.placeholder = "Search for restaurants, cafes...."
//        tf.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
//        tf.layer.cornerRadius = 5
//        tf.backgroundColor = UIColor(red: 237, green: 240, blue: 245)
//        return tf
//    }()
    
    let searchTextField: UIView = {
        let tf = UIView()
        tf.backgroundColor = UIColor.white
        
        return tf
    }()
    
    let cellId = "trendingCellId"
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        addSubview(locationLabel)
        addSubview(locationPin)
        addSubview(locationName)
        addSubview(changeLocationButton)
        addSubview(searchTextField)
        
        
        locationLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 20, left: 20, bottom: 0, right: 0))
        locationPin.anchor(top: locationLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 5, left: 20, bottom: 0, right: 0))
        locationName.anchor(top: locationLabel.bottomAnchor, leading: locationPin.trailingAnchor, bottom: nil, trailing: nil, padding: .init(top: 5, left: 10, bottom: 0, right: 0))
        changeLocationButton.anchor(top: locationLabel.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 20))
        searchTextField.anchor(top: locationPin.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 45))
        
        
    }

    

}
