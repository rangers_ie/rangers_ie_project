//
//  TrendingCVCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 3/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class TrendingCVCell: SuperCollectionViewCell {
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleToFill
        return iv
    }()
    
    let blackView: UIView = {
        let blackView = UIView()
        blackView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        return blackView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Dooloeys"
        label.textColor = UIColor.white
        //label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    let addressLabel: UILabel = {
        let label = UILabel()
        label.text = "address"
        label.numberOfLines = 0
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        label.textColor = UIColor.white
        return label
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        addSubview(imageView)
        addSubview(blackView)
        addSubview(nameLabel)
        addSubview(addressLabel)
        
        
        imageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        
        blackView.anchor(top: nil, leading: leadingAnchor, bottom: imageView.bottomAnchor, trailing: trailingAnchor, padding: .zero, size: .init(width: 0, height: 60))
        
        nameLabel.anchor(top: blackView.topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 5, left: 10, bottom: 0, right: 20))
        addressLabel.anchor(top: nameLabel.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 0, left: 10, bottom: 0, right: 0))
        
    }

}
