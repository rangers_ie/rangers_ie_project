//
//  HomeViewBrowsePlacesCard.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 3/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class HomeViewBrowsePlacesCard: SuperView {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Browse place around me"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        label.textColor = UIColor(hex: "051e28")
        return label
    }()
    
    let cafeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cafes", for: .normal)
        button.layer.shadowColor = UIColor.lightGray.cgColor
        button.backgroundColor = UIColor(hex: "ff9500")
        
//        button.layer.borderColor = UIColor(red: 88, green: 86, blue: 214).cgColor
//        button.layer.borderWidth = 2
//        button.layer.cornerRadius = 5
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.shadowOpacity = 0.5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.cornerRadius = 5
        return button
    }()
    
    let restaurantButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Restaurants", for: .normal)
        button.layer.shadowColor = UIColor.lightGray.cgColor
        button.backgroundColor = UIColor(hex: "ff9500")
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.shadowOpacity = 0.5
        button.layer.cornerRadius = 5
        return button
    }()
    
    let stackViewForCafe: UIStackView = {
        let sv = UIStackView(frame: .zero)
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        sv.alignment = .fill
        sv.spacing = 5
        return sv
    }()
    
    let stackViewForToilets: UIStackView = {
        let sv = UIStackView(frame: .zero)
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        sv.alignment = .fill
        sv.spacing = 5
        return sv
    }()
    
    let accessibleBuildings: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Accessible Buildings", for: .normal)
        button.layer.shadowColor = UIColor.lightGray.cgColor
        button.backgroundColor = UIColor(hex: "ff9500")
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.shadowOpacity = 0.5
        button.layer.cornerRadius = 5
        return button
    }()
    
    let publicToiletsButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Toilets", for: .normal)
        button.layer.shadowColor = UIColor.lightGray.cgColor
        button.backgroundColor = UIColor(hex: "ff9500")
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.shadowOpacity = 0.5
        button.layer.cornerRadius = 5
        return button
    }()
    
    let viewRecentlyVistedPlacesButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("View recetly visited", for: .normal)
        return button
    }()


    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        addSubview(titleLabel)
        addSubview(cafeButton)
        addSubview(restaurantButton)

        addSubview(accessibleBuildings)
        addSubview(publicToiletsButton)
        addSubview(viewRecentlyVistedPlacesButton)
        
        addSubview(stackViewForCafe)
        addSubview(stackViewForToilets)
        
        
        titleLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        //viewRecentlyVistedPlacesButton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 20))
        
        stackViewForCafe.anchor(top: titleLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 20), size: .init(width: 0, height: 30))
        
        stackViewForCafe.addArrangedSubview(cafeButton)
        stackViewForCafe.addArrangedSubview(restaurantButton)
        
        stackViewForToilets.anchor(top: stackViewForCafe.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 20), size: .init(width: 0, height: 30))
        
        stackViewForToilets.addArrangedSubview(accessibleBuildings)
        stackViewForToilets.addArrangedSubview(publicToiletsButton)
        
        
//        cafeButton.anchor(top: titleLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0), size: .init(width: 180, height: 30))
//        restaurantButton.anchor(top: titleLabel.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 20), size: .init(width: 180, height: 30))

//        accessibleBuildings.anchor(top: cafeButton.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0), size: .init(width: 180, height: 30))
//        publicToiletsButton.anchor(top: cafeButton.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 20), size: .init(width: 180, height: 30))
        
    }

}
