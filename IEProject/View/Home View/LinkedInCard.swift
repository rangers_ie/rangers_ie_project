//
//  LinkedInCard.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 8/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class LinkedInCard: SuperView {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Meet the WheelyGo team"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    let ashleyButton: CenteredButton = {
        let button = CenteredButton(type: .system)
        let image  = UIImage(named: "femaleUser")
        button.setImage(image, for: .normal)
        button.setTitle("Ashley", for: .normal)
        return button
    }()
    let minonButton: CenteredButton = {
        let button = CenteredButton(type: .system)
        
        let image  = UIImage(named: "maleUser")
        button.setImage(image, for: .normal)
        button.setTitle("Minon", for: .normal)
        return button
    }()

    let akashButton: CenteredButton = {
        let button = CenteredButton(type: .system)
        button.setTitle("Akash", for: .normal)
        let image  = UIImage(named: "maleUser")
        button.setImage(image, for: .normal)
        return button
    }()

    let linButton: CenteredButton = {
        let button = CenteredButton(type: .system)
        button.setTitle("Lin", for: .normal)
        let image  = UIImage(named: "maleUser")
        button.setImage(image, for: .normal)
        return button
    }()
    
    let stackView: UIStackView = {
        let sv = UIStackView(frame: .zero)
        sv.axis = .horizontal
        sv.distribution = .fillEqually
        sv.alignment = .fill
        sv.spacing = 5
        return sv
    }()

    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        stackView.addArrangedSubview(ashleyButton)
        stackView.addArrangedSubview(minonButton)
        stackView.addArrangedSubview(akashButton)
        stackView.addArrangedSubview(linButton)
        addSubview(stackView)
        addSubview(titleLabel)
        
        
        titleLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 5, left: 20, bottom: 0, right: 0))
        
        stackView.anchor(top: titleLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 5, left: 0, bottom: 0, right: 0))
    }

}
