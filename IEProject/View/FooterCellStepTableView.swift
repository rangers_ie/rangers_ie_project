//
//  FooterCellStepTableView.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 10/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class FooterCellStepTableView: SuperView {
    
    let startNavButton: UIButton = {
        let startNavButton = UIButton(type: .system)
        startNavButton.setTitle("Take Me", for: .normal)
        startNavButton.setTitleColor(UIColor.white, for: .normal)
        startNavButton.backgroundColor = UIColor.blue
        startNavButton.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        return startNavButton
    }()
    
    let cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cancel", for: .normal)
        return button
    }()
    
    let review: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Review", for: .normal)
        return button
    }()
    

    let seperatorLine: UIView = {
        let lineView = UIView()
        lineView.backgroundColor = UIColor.lightGray
        return lineView
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        addSubview(startNavButton)
        addSubview(seperatorLine)
        addSubview(cancelButton)
        addSubview(review)
        startNavButton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 15, left: 0, bottom: 0, right: 10), size: CGSize(width: 100, height: 40))
        cancelButton.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil, padding: .init(top: 15, left: 10, bottom: 0, right: 0))
        
        //review.anchor(top: topAnchor, leading: cancelButton.trailingAnchor, bottom: bottomAnchor, trailing: nil, padding: .init(top: 15, left: 10, bottom: 0, right: 0))
        
        seperatorLine.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .zero, size: CGSize(width: 0, height: 1))
        
        
        
        
    }

}
