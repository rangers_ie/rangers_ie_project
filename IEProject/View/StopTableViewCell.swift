//
//  StopTableViewCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 30/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class StopTableViewCell: UITableViewCell {
    
    let stopName: UILabel = {
        let label = UILabel()
        label.text = "Melbourne Trams"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    let distanceFromLabel: UILabel = {
        let label = UILabel()
        label.text = "2km from you"
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        return label
    }()
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(stopName)
        contentView.addSubview(distanceFromLabel)
        
        stopName.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 20, left: 20, bottom: 0, right: 0))
        distanceFromLabel.anchor(top: stopName.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    



}
