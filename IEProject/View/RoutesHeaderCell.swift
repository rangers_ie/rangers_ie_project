//
//  RoutesHeaderCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 9/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class RoutesHeaderCell: SuperCollectionViewCell {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Available Pedestrian Paths"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    override func setUpView() {
        super.setUpView()
        addSubview(titleLabel)
        backgroundColor = UIColor.white
        titleLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil, padding: .init(top: 0, left: 10, bottom: 0, right: 0))
    }


}
