//
//  HomeView.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 26/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class HomeView: SuperView {
    
    let titleView: TitleView = {
        let title = TitleView()
        title.layer.cornerRadius = 30
        return title
    }()
    
    let headerImage: UIImageView = {
        let image = UIImage(named: "a2")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    
    let searchTextField: UITextField = {
        let tf = UITextField()
        tf.backgroundColor = UIColor.white
        tf.layer.cornerRadius = 5
        tf.resignFirstResponder()
        tf.placeholder = " What are you looking for?"
        return tf
    }()
    let segemented: UISegmentedControl = {
        let items = ["Around Me","Recently Visited"]
        let sc = UISegmentedControl(items: items)
        sc.tintColor = UIColor(displayP3Red: 0, green: 122/255, blue: 1, alpha: 1)
        sc.backgroundColor = UIColor.white
        sc.selectedSegmentIndex = 0
        return sc
    }()
    
//    lazy var aroundMeCollectionView: UICollectionView = {
//        let col = UICollectionView(frame: frame, collectionViewLayout: UICollectionViewFlowLayout())
//        col.dataSource = self
//        col.delegate = self
//        col.backgroundColor = UIColor.white
//        col.contentInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
//        return col
//    }()
    
    let shadowView: UIView = {
        let sv = UIView()
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        addSubview(headerImage)
        shadowView.addSubview(searchTextField)
        shadowView.addSubview(segemented)
        addSubview(shadowView)
        addSubview(titleView)
        
        //aroundMeCollectionView.register(AroundMeCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        
        titleView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 30, left: 20, bottom: 0, right: 20), size: .init(width: 0, height: 60))
        
        headerImage.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .zero, size: CGSize(width: 0, height: 450))
        
        shadowView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 290, left: 0, bottom: 0, right: 0), size: CGSize(width: 0, height: 110))
        
        
        searchTextField.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 305, left: 5, bottom: 0, right: 5), size: CGSize(width: 0, height: 50))


        segemented.anchor(top: searchTextField.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 15, left: 0, bottom: 0, right: 0), size: CGSize(width: 0, height: frame.height*0.1))

        
    
        
        
       
    }

}

extension  HomeView: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! AroundMeCollectionViewCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = (collectionView.frame.height/2) - 30
        let width = (collectionView.frame.width/3) - 20
        return CGSize(width: width, height: height)
    }
    
    
}

class TitleView: SuperView{
    let title: UILabel = {
        let label = UILabel()
        label.text = "Your journey gets better with WheelyGo"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.textAlignment = .center
        label.textColor = UIColor.blue
        return label
    }()
    
    
    override func setUpView() {
        super.setUpView()
        addSubview(title)
        backgroundColor = UIColor.white.withAlphaComponent(0.4)
        title.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 20, left: 20, bottom: 20, right: 20))
    }
}


