//
//  AroundMeCollectionViewCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 27/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class AroundMeCollectionViewCell: SuperCollectionViewCell {
    
    let icon: UIButton = {
        let icon = UIButton(type: .system)
        let image = UIImage(named: "cafe")
        icon.setImage(image, for: .normal)
        return icon
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.text = "Cafe"
        label.textAlignment = .center
        label.textColor = UIColor(displayP3Red: 0, green: 122/255, blue: 1, alpha: 1)
        return label
    }()

    override func setUpView() {
        super.setUpView()
        addSubview(icon)
        addSubview(label)

        icon.isUserInteractionEnabled = false
        backgroundColor = UIColor.white.withAlphaComponent(0.7)
        icon.centerAnchor(centerX: centerXAnchor, centerY: centerYAnchor, size: CGSize(width: 65, height: 65), padding: .zero)
        label.anchor(top: icon.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor)
        
    }
    
 

    
}
