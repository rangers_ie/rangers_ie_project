//
//  RouteCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 9/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class RouteCell: SuperCollectionViewCell {
    
    let cellId = "stepsCellId"
    

    
    let distanceValue: UILabel = {
        let label = UILabel()
        label.text = "Distance"
        label.textColor = UIColor.gray
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        return label
    }()
    
    let routeSummary: UILabel = {
        let label = UILabel()
        label.text = "DURATION"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    let durationValue: UILabel = {
        let label = UILabel()
        label.text = "10.8"
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        return label
    }()
    
    let stepButton: UIButton = {
        let button = UIButton(type: .system)
        let buttonImage = UIImage(named: "more")
        button.setImage(buttonImage, for: .normal)
        return button
    }()
    
    

    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        addSubview(distanceValue)
        addSubview(routeSummary)
        addSubview(durationValue)
        addSubview(stepButton)


        
        durationValue.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding:.init(top: 20, left: 20, bottom: 0, right: 0))
        
        distanceValue.anchor(top: topAnchor, leading: durationValue.trailingAnchor, bottom: nil, trailing: nil, padding: .init(top: 20, left: 20, bottom: 0, right: 0))
        routeSummary.anchor(top: durationValue.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        stepButton.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 20))
        

     
    }
    
  
}



