//
//  PublicToiletsCollectionViewCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 31/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class PublicToiletsCollectionViewCell: SuperCollectionViewCell {
    
    let namelabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    let distanceLabel: UILabel = {
        let label = UILabel()
        label.text = "Distace From You"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    let maleFemaleImage: UIImageView = {
        let image = UIImage(named: "maleFemale")?.withRenderingMode(.alwaysOriginal)
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let wheelChairImage: UIImageView = {
        let image = UIImage(named: "wheelchair")?.withRenderingMode(.alwaysTemplate)
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    

    
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = .white
        addSubview(namelabel)
        addSubview(distanceLabel)
        addSubview(maleFemaleImage)
        addSubview(wheelChairImage)

        namelabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 20, left: 10, bottom: 0, right: 0))
        
        distanceLabel.anchor(top: namelabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 10, bottom: 0, right: 0))
        
        maleFemaleImage.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 0, left: 0, bottom: 5, right: 10))
        
        wheelChairImage.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: maleFemaleImage.leadingAnchor, padding: .init(top: 0, left: 0, bottom: 5, right: 10))
        
        

    }
    
}
