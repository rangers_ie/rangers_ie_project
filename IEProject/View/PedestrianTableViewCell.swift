//
//  PedestrianTableViewCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 15/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class PedestrianTableViewCell: UITableViewCell {
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Sample Text"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    let numberOfPedestrians: UILabel = {
        let label = UILabel()
        label.text = "200"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    let dateTimeLabel: UILabel = {
        let label = UILabel()
        label.text = "2012-12-12-22"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    let descLabel: UILabel = {
        let label = UILabel()
        label.text = "Pedestrian Count"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(nameLabel)
        contentView.addSubview(numberOfPedestrians)
        contentView.addSubview(dateTimeLabel)
        contentView.addSubview(descLabel)
        
        nameLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 20, left: 20, bottom: 0, right: 0))
        numberOfPedestrians.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 20))
        
        descLabel.anchor(top: nameLabel.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 10))
        
        dateTimeLabel.anchor(top: nameLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
  

}
