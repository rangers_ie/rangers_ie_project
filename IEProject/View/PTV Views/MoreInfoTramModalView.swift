//
//  MoreInfoTramModalView.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 7/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class MoreInfoTramModalView: SuperView {
    
    let messageLabe: UILabel = {
        let label = UILabel()
        label.text = "We are sorry"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.lineBreakMode = .byCharWrapping
        return label
    }()
    
    let accessibleRoutesButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Accessible Trams", for: .normal)
        return button
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        addSubview(messageLabe)
        addSubview(accessibleRoutesButton)
        
        messageLabe.centerAnchor(centerX: centerXAnchor, centerY: centerYAnchor)
        accessibleRoutesButton.anchor(top: messageLabe.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 0))
        
        
        
        
    }

}
