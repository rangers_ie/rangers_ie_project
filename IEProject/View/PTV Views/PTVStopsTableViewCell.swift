//
//  PTVStopsTableViewCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 4/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class PTVStopsTableViewCell: UITableViewCell {
    
    let stopName: UILabel = {
        let label = UILabel()
        label.text = "Stop name goes here"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.textColor = UIColor(red: 0, green: 122, blue: 255)
        return label
    }()
    
    let distanceFromCurrentLoationLabel: UILabel = {
        let label = UILabel()
        label.text = "Stop name goes here"
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(stopName)
        contentView.addSubview(distanceFromCurrentLoationLabel)
        stopName.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 5, left: 20, bottom: 0, right: 0))
        distanceFromCurrentLoationLabel.anchor(top: stopName.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 5, left: 20, bottom: 0, right: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
