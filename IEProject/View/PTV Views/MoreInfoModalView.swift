//
//  MoreInfoModalView.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 4/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit


class MoreInfoModalView: SuperView {
    
    let stationNameLabel: UILabel = {
        let label = UILabel()
        label.text = "Station Name"
        label.textColor = UIColor(red: 0, green: 122, blue: 255)
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        return label
        
    }()
    
    let accessibilityTitle: UILabel = {
        let label = UILabel()
        label.text = "Accessibility Information"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    let accessibleRamp: UILabel = {
        let label = UILabel()
        label.text = "Wheelchair Accessible"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    let accessibleParking: UILabel = {
        let label = UILabel()
        label.text = "Accessible Parking"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    let accessiblePhone: UILabel = {
        let label = UILabel()
        label.text = "Accessible Phone"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    let accessibleToilet: UILabel = {
        let label = UILabel()
        label.text = "Accessible Toilet"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    let takeMeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Take me to .....", for: .normal)
        return button
    }()
    
    
 
    
  

    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        addSubview(stationNameLabel)
        addSubview(accessibilityTitle)
        addSubview(accessibleRamp)
        addSubview(accessibleParking)
        addSubview(accessiblePhone)
        addSubview(accessibleToilet)
        addSubview(takeMeButton)
        
        stationNameLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 20, left: 20, bottom: 0, right: 0))
        accessibilityTitle.anchor(top: stationNameLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 30, left: 20, bottom: 0, right: 0))
        
        accessibleRamp.anchor(top: accessibilityTitle.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 20, left: 50, bottom: 0, right: 0))
        
        
        accessibleParking.anchor(top: accessibleRamp.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 50, bottom: 0, right: 0))
        
        accessiblePhone.anchor(top: accessibleParking.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 50, bottom: 0, right: 0))
        accessibleToilet.anchor(top: accessiblePhone.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 50, bottom: 0, right: 0))
        
        takeMeButton.anchor(top: accessibleToilet.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 0))
    }

}
