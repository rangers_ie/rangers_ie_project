//
//  PTVView.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 27/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class PTVView: SuperView {
    
    let viewHeight = 170
    
    let title: UILabel = {
        let label = UILabel()
        label.text = "Plan your journey"
        label.textColor = UIColor.white
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        return label
    }()

    
    let blackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(displayP3Red: 0, green: 122/255, blue: 1, alpha: 1)
        return view
    }()
    
    let tramRouteTextField: UITextField = {
        let tf = UITextField()
        tf.backgroundColor = UIColor.white
        tf.placeholder = "Tram route"
        tf.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
        return tf
    }()
    

    let showStopsButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.setTitle("Show Tram Stops", for: .normal)
        return btn
    }()
    
    let segementedControl: UISegmentedControl = {
        let items = ["En Route","Near Me"]
        let sc = UISegmentedControl(items: items)
        sc.backgroundColor = UIColor.white
        sc.layer.borderWidth = 2
        sc.layer.borderColor = UIColor.white.cgColor
        sc.isHidden = true
        return sc
    }()
    
    let accessibleNoticeLabel: UILabel = {
        let label = UILabel()
        label.text = "Partly serviced by low floor trams"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.textColor = UIColor.white
        label.isHidden = true
        return label
    }()
    
    let backButton: UIButton = {
        let backButton = UIButton(type: .system)
        backButton.setImage(UIImage(named: "backButton")?.withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.tintColor = UIColor.white
        return backButton
    }()
    
    let sortButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sort", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        return button
    }()
    
    
    
    override func setUpView() {
        super.setUpView()
        addSubview(blackView)
        addSubview(title)
        addSubview(tramRouteTextField)
        addSubview(segementedControl)
        addSubview(accessibleNoticeLabel)
        addSubview(backButton)
        addSubview(sortButton)
    
        
        blackView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .zero, size: .init(width: 0, height: viewHeight))
        
        
        backButton.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 30, left: 10, bottom: 0, right: 0))
        
        title.anchor(top: topAnchor, leading: backButton.trailingAnchor, bottom: nil, trailing: nil, padding: .init(top: 25, left: 20, bottom: 0, right: 0))
        
        
        tramRouteTextField.anchor(top: title.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 30, left: 20, bottom: 0, right: 20), size: .init(width: 0, height: 30))
        segementedControl.anchor(top: tramRouteTextField.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 20, left: 20, bottom: 0, right: 20))
        accessibleNoticeLabel.anchor(top: tramRouteTextField.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 20, left: 20, bottom: 0, right: 20))
        
        sortButton.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 0, left: 0, bottom: 5, right: 20))

    }

}
