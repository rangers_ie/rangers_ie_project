//
//  RecentPlacesCollectionViewCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 30/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class RecentPlacesCollectionViewCell: SuperCollectionViewCell {
    
    let label: UILabel = {
        let label = UILabel()
        label.text = "Your most recent search will go here \n but at the moment its not available. \n Come back in two weeks"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping // or NSLineBreakMode.ByWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    override func setUpView() {
        super.setUpView()
        addSubview(label)
        backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
    }
    
}
