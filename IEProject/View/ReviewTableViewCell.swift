//
//  ReviewTableViewCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 17/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {
    
    let reviewText: UILabel = {
        let label = UILabel()
        label.text = "awadawdw"
        //label.font = UIFont.preferredFont(forTextStyle: .body)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    let rating: UILabel = {
        let label = UILabel()
        label.text = "3.5"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(reviewText)
        addSubview(rating)
        
        reviewText.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil, padding: .init(top: 0, left: 10, bottom: 0, right: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
