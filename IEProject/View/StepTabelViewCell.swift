//
//  StepsTableTableViewCell.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 10/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class StepTabelViewCell: UITableViewCell {
    
    let routeInfoLabel: UILabel = {
        let label = UILabel()
        label.text = "Some text goes here"
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    let distanceValue: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = UIColor.lightGray
        label.text = "10.8km"
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(routeInfoLabel)
        contentView.addSubview(distanceValue)
        
       routeInfoLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        distanceValue.anchor(top: routeInfoLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 50, bottom: 10, right: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
