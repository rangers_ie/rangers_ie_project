//
//  AllPlacesCV.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 5/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import CoreLocation
import NVActivityIndicatorView
import JGProgressHUD

class AllPlacesCV: UIViewController {
    
    var locationType: String!
    var allPlaces: [GoogleResults] = []
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    var activityIndicator: NVActivityIndicatorView?
    
    
    let allPlacesCellId = "cellId"
    var hud: JGProgressHUD?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "All places"
        view.backgroundColor = UIColor.white
        setup()
        hud = JGProgressHUD(style: .extraLight)
        hud?.textLabel.text = "Loading"
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    
    
    private func setup(){
        collectionView.register(PlaceCollectionViewCell.self, forCellWithReuseIdentifier: allPlacesCellId)
        view.addSubview(collectionView)
        collectionView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor)
    }
    

   

}

extension AllPlacesCV: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allPlaces.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: allPlacesCellId, for: indexPath) as! PlaceCollectionViewCell
        let place = allPlaces[indexPath.item]
        cell.placeName.text = place.name!
        
    
        let location = place.geometry?.location
        
        let placeCoordinates = CLLocation(latitude: (location?.lat)!, longitude: (location?.lng)!)
        var distance = AppDelegate.appDelegateShared().currentLocation.distance(from: placeCoordinates)
        distance = distance / 1000
        distance = distance.rounded(toPlaces: 2)
        cell.distanceFromYouLabel.text = "\(distance) km from current location"
        
        if let imageReference = place.photos?.last?.photo_reference {
            let imageURL = GoogleClient.sharedInstance.getImageURL(pictureReference: imageReference)
            cell.imageView.setImageWith(imageURL)
            
        }

        Helpers.setCell(cell: cell)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width-20, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(10, 10, 10, 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let place = allPlaces[indexPath.item]
        let placeId = place.place_id
        hud?.textLabel.text = "Loading \(place.name!)"
        hud?.show(in: self.view)
        GoogleClient.sharedInstance.getPlaceFromId(palceId: placeId!) { (placeResponce) in
            guard let place = placeResponce.result else {return}
             
            DispatchQueue.main.async {
                guard let lats = place.geometry?.location?.lat else {return}
                guard let longs = place.geometry?.location?.lng else {return}
                let location = CLLocation(latitude: lats , longitude: longs)
                let placeDetail = NewPlaceDetailsVC()
                placeDetail.googlePlaceId = placeId
                placeDetail.placeLocation = location
                placeDetail.googlePlaceFromId = place
                placeDetail.googlePlaceForMapVC = placeResponce
                self.navigationController?.pushViewController(placeDetail, animated: true)
                self.hud?.dismiss()
            }
        }
    
    }
    
    
    
    
   
    
    private func loadData(){
        if locationType == Constants.CAFE{
            allPlaces = AppDelegate.appDelegateShared().allCafeForGivenLocation
        }
        else{
            allPlaces = AppDelegate.appDelegateShared().allRestsForGivenLocation
        }
        collectionView.reloadData()
    }
    
}
