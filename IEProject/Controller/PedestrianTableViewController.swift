//
//  PedestrianTableViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 15/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class PedestrianTableViewController: UITableViewController {
    
    let cellId = "cellId"
    var allPedesData: [Pedestrian]=[]
    
    var filteredPedesData:[Pedestrian] = []
    var isSearching: Bool = false
    
    var searchBar: UISearchBar = {
        let width = Int(UIScreen.main.bounds.width)
        let frame = CGRect(x: 0, y: 0, width: width, height: 40)
        let searchBar = UISearchBar(frame: frame)
        searchBar.placeholder = "Search using street name"
        return searchBar
        
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        self.navigationItem.titleView = searchBar
        searchBar.delegate = self
        loadData()
        tableView.register(PedestrianTableViewCell.self, forCellReuseIdentifier: cellId)
        title = "Pedestrian Count"

    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if isSearching{
            return filteredPedesData.count
        }
        return allPedesData.count
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! PedestrianTableViewCell
        
        if indexPath.item != 0 {
            cell.descLabel.isHidden = true
        }
        
        if isSearching{
            let pedestrian = filteredPedesData[indexPath.item]
            cell.nameLabel.text = pedestrian.sensor_name
            cell.numberOfPedestrians.text = pedestrian.qv_market_peel_st
            cell.dateTimeLabel.text = pedestrian.daet_time
            return cell
        }
        let pedestrian = allPedesData[indexPath.item]
        cell.nameLabel.text = pedestrian.sensor_name
        cell.numberOfPedestrians.text = pedestrian.qv_market_peel_st
        cell.dateTimeLabel.text = pedestrian.daet_time
        return cell

    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}

extension PedestrianTableViewController{
    private func loadData(){
        APIClient.sharedInstance.getPedestrianInformation { (responce) in
            self.allPedesData = responce
            self.filteredPedesData = responce
            self.tableView.reloadData()
        }
    }
}
extension PedestrianTableViewController: UISearchBarDelegate{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearching = true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearching = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredPedesData.removeAll(keepingCapacity: false)
        let predicateString = searchBar.text!
        filteredPedesData = allPedesData.filter({$0.sensor_name?.range(of: predicateString) != nil})
        filteredPedesData.sort{$0.qv_market_peel_st! < $1.qv_market_peel_st!}
        isSearching = (filteredPedesData.count == 0) ? false: true
        tableView?.reloadData()
        
    }
}
