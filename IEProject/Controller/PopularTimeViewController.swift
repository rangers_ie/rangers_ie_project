//
//  PopularTimeViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 30/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class PopularTimeViewController: UIViewController {
    
    var popularHoursString: String?
    
    let label: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    let cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Go back", for: .normal)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(label)
        view.addSubview(cancelButton)
        label.text = "Popular Hours \n \(popularHoursString!)"
        
        label.centerAnchor(centerX: view.centerXAnchor, centerY: view.centerYAnchor)
        cancelButton.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 20, right: 0))
        
        cancelButton.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        
        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleOnScreenTap))
        
        view.addGestureRecognizer(tap)
        
    }
    
    @objc
    private func handleOnScreenTap(){
        dismiss(animated: true, completion: nil)
    }
    
    @objc
    private func handleCancel(){
        dismiss(animated: true, completion: nil)
    }
    
    


}
