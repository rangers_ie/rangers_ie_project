//
//  HVExtesionForSearchBar.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 5/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import Foundation
import GooglePlaces

extension LatesHomeViewController{



    func setupSearchController(){
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self

        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController

        //let subView = UIView(frame: CGRect(x: 0, y: 200, width: 350.0, height: 45.0))

        homeViewLocationCard.searchTextField.addSubview((searchController?.searchBar)!)

        //view.addSubview(subView)

        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false

        searchController?.searchBar.searchBarStyle = .minimal
        
        searchController?.searchBar.placeholder = "Search for restaurants, cafes...."
        
        let filter = GMSAutocompleteFilter()
        //auFilter.type = .region
        filter.country = "AU"
        filter.type = .establishment
        
        
//        let regionA = CLLocationCoordinate2DMake(-37.815299, 144.962656)
//        let regionB = locationWithBearing(bearing: 300.0, distanceMeters: 10000, origin: regionA)
//
//        let bounds = GMSCoordinateBounds(coordinate: regionA, coordinate: regionB)
//        resultsViewController?.autocompleteBoundsMode = .restrict
//        resultsViewController?.autocompleteBounds = bounds
        
        resultsViewController?.autocompleteFilter = filter

        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true

    }
    
    func locationWithBearing(bearing:Double, distanceMeters:Double, origin:CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        let distRadians = distanceMeters / (6372797.6) // earth radius in meters
        
        let lat1 = origin.latitude * .pi / 180
        let lon1 = origin.longitude * .pi / 180
        
        let lat2 = asin(sin(lat1) * cos(distRadians) + cos(lat1) * sin(distRadians) * cos(bearing))
        let lon2 = lon1 + atan2(sin(bearing) * sin(distRadians) * cos(lat1), cos(distRadians) - sin(lat1) * sin(lat2))
        
        return CLLocationCoordinate2D(latitude: lat2 * 180 / .pi, longitude: lon2 * 180 / .pi)
    }
    
}

extension LatesHomeViewController: GMSAutocompleteResultsViewControllerDelegate{
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        
        if resultsController == resultsViewController {
            let placeId = place.placeID
            GoogleClient.sharedInstance.getPlaceFromId(palceId: placeId) { (googPlace) in
                DispatchQueue.main.async {
                    let detailView = NewPlaceDetailsVC()
                    detailView.googlePlaceForMapVC = googPlace
                    detailView.googlePlaceFromId = googPlace.result
                    guard let lats = googPlace.result?.geometry?.location?.lat else {return}
                    guard let longs = googPlace.result?.geometry?.location?.lng else {return}
                    detailView.placeLocation = CLLocation(latitude: lats, longitude: longs)
                    self.navigationController?.pushViewController(detailView, animated: true)
                }
            }
        }

    }

    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
        print("didFailAutocompleteWithError")
        print(error.localizedDescription)
    }

    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }

    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }


}

