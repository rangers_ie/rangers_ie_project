//
//  HomeViewExtensions.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 3/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import JGProgressHUD

extension LatesHomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return trendingRestaurants.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId , for: indexPath) as! TrendingCVCell
        setupCellCardLayout(cell: cell)
        if let restaurant = trendingRestaurants[indexPath.item].restaurant{
            setupCell(cell: cell, restaurant: restaurant)
        }
    
        return cell
    }
    private func setupCell(cell: TrendingCVCell, restaurant: TrendingRestaurant ){
        if let imageRef = restaurant.thumb{
            guard let url = URL(string: imageRef) else{
                print("Problem when creating URL for image ")
                return
            }
            cell.imageView.setImageWith(url)
            cell.nameLabel.text = restaurant.name
            cell.addressLabel.text = restaurant.location?.address
        }
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200, height: 200)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let place = trendingRestaurants[indexPath.item]
        guard let location = place.restaurant?.location else {return}
        guard let lats = location.latitude?.toDouble() else {return}
        guard let longs = location.longitude?.toDouble() else {return}
        guard var name = place.restaurant?.name else {return}
        hud?.textLabel.text = "Loading \(name)"
        hud?.show(in: self.view)
        name = name.replacingOccurrences(of: " ", with: "%20")
        let coordinates = CLLocation(latitude: lats, longitude: longs)
        GoogleClient.sharedInstance.getPlaceIdFromName(placeName: name, coordinates: coordinates) { (responce) in
            let placeId = responce
            GoogleClient.sharedInstance.getPlaceFromId(palceId: placeId, completition: { (googPlace) in
                DispatchQueue.main.async {
                    let placeDetailVC = NewPlaceDetailsVC()
                    placeDetailVC.googlePlaceId = placeId
                    placeDetailVC.googlePlaceFromId = googPlace.result
                    placeDetailVC.googlePlaceForMapVC = googPlace
                    placeDetailVC.placeLocation = coordinates
                    self.navigationController?.pushViewController(placeDetailVC, animated: true)
                    self.hud?.dismiss()
                }
            })
        }
       
        
        
    }
    
    private func setupCellCardLayout(cell: TrendingCVCell){
        
        cell.contentView.layer.cornerRadius = 4.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        cell.layer.shadowRadius = 4.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
    
    }
}

extension LatesHomeViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
}


