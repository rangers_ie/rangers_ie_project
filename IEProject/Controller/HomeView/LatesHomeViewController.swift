//
//  LatesHomeViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 3/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import Foundation
import GooglePlaces
import JGProgressHUD

class LatesHomeViewController: UIViewController, AddressComponentsDelegate {
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    
    
    let homeViewLocationCard: HomeViewLocationCard = {
        let view = HomeViewLocationCard()
        return view
    }()
    
    let homeViewBrowseLocationCard: HomeViewBrowsePlacesCard = {
        let view = HomeViewBrowsePlacesCard()
        return view
    }()
    
    let transportOptionsCard: TransportCard = {
        let view = TransportCard()
        return view
    }()
    
    
    
    lazy var trendingCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        //layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        cv.backgroundColor = UIColor.white
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let linkedInCardView: LinkedInCard = {
        let view = LinkedInCard()
        return view
    }()
    
    
    let trendingCVTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.text = "Trending this week"
        //label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        return label
    }()
    var zomatoTrendingData: TrendingWeekly?
    var trendingRestaurants: [TrendingRestaurants] = []
    var allPublicToiletsAroundCurrentLocation: [Toilets] = []
    
    
    let cellId = "trendingCellId"
    
    var hud: JGProgressHUD?

    override func viewDidLoad() {
        super.viewDidLoad()
        currentLocationNotification()
        setupViews()
        view.backgroundColor = UIColor(red: 237, green: 240, blue: 245)
        trendingCollectionView.register(TrendingCVCell.self, forCellWithReuseIdentifier: cellId)
        loadTrendingData()
        setupCurrentLocation(addressComponents: AppDelegate.appDelegateShared().currentAddressComponents)
        
        hud = JGProgressHUD(style: .extraLight)
        
        
    }
    
    @objc
    private func handleOpenSearchController(){
        let gooleSearchController = GoogleAutoCompleteViewController()
        gooleSearchController.addressDelegate = self
        navigationController?.pushViewController(gooleSearchController, animated: true)
    }
    
    
    
    
    
    private func setupViews(){
        
        view.addSubview(homeViewLocationCard)
        homeViewLocationCard.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .zero, size: .init(width: 0, height: 125))
        homeViewLocationCard.changeLocationButton.addTarget(self, action: #selector(handleOpenSearchController), for: .touchUpInside)
        homeViewLocationCard.changeLocationButton.isHidden = true
        
        view.addSubview(homeViewBrowseLocationCard)
        homeViewBrowseLocationCard.anchor(top: homeViewLocationCard.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 120))
        
        homeViewBrowseLocationCard.cafeButton.addTarget(self, action: #selector(handleCafeButton), for: .touchUpInside)
        homeViewBrowseLocationCard.restaurantButton.addTarget(self, action: #selector(handleRestaurantButton), for: .touchUpInside)
        homeViewBrowseLocationCard.publicToiletsButton.addTarget(self, action: #selector(handlePublicToilets), for: .touchUpInside)
        homeViewBrowseLocationCard.accessibleBuildings.addTarget(self, action: #selector(handleAccessibleBuildings), for: .touchUpInside)
        
        view.addSubview(transportOptionsCard)
        transportOptionsCard.anchor(top: homeViewBrowseLocationCard.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 70))
        transportOptionsCard.trainsButton.addTarget(self, action: #selector(handleTrainsButton), for: .touchUpInside)
        transportOptionsCard.tramButtons.addTarget(self, action: #selector(handleTramButton), for: .touchUpInside)
        transportOptionsCard.infoButton.addTarget(self, action: #selector(handleTrainsAndTramsInfoButton), for: .touchUpInside)
        
        view.addSubview(trendingCVTitle)
        trendingCVTitle.anchor(top: transportOptionsCard.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        

        view.addSubview(trendingCollectionView)
        trendingCollectionView.anchor(top: trendingCVTitle.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 10, left: 5, bottom: 0, right: 5), size: .init(width: 0, height: 200))
        
        view.addSubview(linkedInCardView)
        linkedInCardView.anchor(top: trendingCollectionView.bottomAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor, padding: UIEdgeInsetsMake(10, 0, 0, 0))
        
        linkedInCardView.ashleyButton.addTarget(self, action: #selector(handleOpenLinkedIn(_:)), for: .touchUpInside)
        linkedInCardView.ashleyButton.tag = 0
        linkedInCardView.minonButton.addTarget(self, action: #selector(handleOpenLinkedIn(_:)), for: .touchUpInside)
        linkedInCardView.minonButton.tag = 1
        linkedInCardView.akashButton.addTarget(self, action: #selector(handleOpenLinkedIn(_:)), for: .touchUpInside)
        linkedInCardView.akashButton.tag = 2
        linkedInCardView.linButton.addTarget(self, action: #selector(handleOpenLinkedIn(_:)), for: .touchUpInside)
        linkedInCardView.linButton.tag = 3
        
        setupSearchController()
    }
    
    @objc
    private func handleOpenLinkedIn(_ sender: UIButton){
        var wheelGoMember: WheelyGoTeam?
        switch sender.tag {
        case 0:
            wheelGoMember = Constants.ASHLEY
        case 1:
            wheelGoMember = Constants.MINON
        case 2:
            wheelGoMember = Constants.AKASH
        case 3:
            wheelGoMember = Constants.LIN
        default:
            print("No user")
        }
        let webView = WebViewController()
        webView.memberData = wheelGoMember
        navigationController?.pushViewController(webView, animated: true)
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        loadToiletDataWithDistance()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @objc
    private func handleTrainsAndTramsInfoButton(){
        let title = "Accessibility Information"
        let message = "Currently WheelyGo does not have access to specfic accessibility information on Tram stops. We are working very hard to getting this information for you. Thank You For Your Understanding"
        NotificationProvider.sharedInstance.alertUser(title: title, message: message, viewController: self)
    }
    
    
    @objc
    private func handleAccessibleBuildings(){
        let acces = NewBuildingAccessibiltyTableViewController()
        navigationController?.pushViewController(acces, animated: true)
    }
    
    @objc
    private func handlePublicToilets(){

        let toilets = NewPublicToiletsViewController()
        toilets.allToiletsData = allPublicToiletsAroundCurrentLocation
        navigationController?.pushViewController(toilets, animated: true)
    }
    
    @objc
    private func handleCafeButton(){

        let allPlaces = AllPlacesCV()
        allPlaces.locationType = Constants.CAFE
        navigationController?.pushViewController(allPlaces, animated: true)
    }
    
    @objc
    private func handleRestaurantButton(){
        
        let allPlaces = AllPlacesCV()
        allPlaces.locationType = Constants.RESTAURANT
        navigationController?.pushViewController(allPlaces, animated: true)
    }
    
    @objc
    private func handleTrainsButton(){
        let trainVC = TrainsTableViewController()
        trainVC.allTrainStops = AppDelegate.appDelegateShared().allTrainStops
        navigationController?.pushViewController(trainVC, animated: true)
    }
    
    @objc
    private func handleTramButton(){
        let tramVC = TramTableViewController()
        tramVC.allTrainStops = AppDelegate.appDelegateShared().allTramStops
        navigationController?.pushViewController(tramVC, animated: true)
    }
    
    
    
    private func setupCurrentLocation(addressComponents: [Address_components]){
        
        if addressComponents.count > 0 {
            guard let streetNumber = addressComponents[0].long_name else {return}
            guard let route = addressComponents[1].long_name else {return}
            let addressString = "\(streetNumber) \(route)"
            homeViewLocationCard.locationName.text = addressString
        }
        else{
            let title = "Error in connection"
            let message = "An error occurred while starting the app. The app may offer limited functionality. Please restart."
            NotificationProvider.sharedInstance.alertUser(title: title, message: message, viewController: self)
            homeViewLocationCard.locationName.text = "An error occurred"
        }
        
        

    }
    
    func getAddressComponents(addressComps: [Address_components]) {
        setupCurrentLocation(addressComponents: addressComps)
    }
    
   
    
    
    private func loadTrendingData(){
        let trendingDataFromAppDelegate = AppDelegate.appDelegateShared().allTrendingLocations
        if let data = trendingDataFromAppDelegate {
            zomatoTrendingData = data
            guard let restaurants = zomatoTrendingData?.restaurants else{
                print("Was unable to load resturants")
                return
            }
            trendingRestaurants = restaurants
        }
        else{
            //This is only a fail safe in case the data does not gets loaded up in the app delegate
            let currentLocation = AppDelegate.appDelegateShared().currentLocation
            ZomatoAPI.getTopTrending(location: currentLocation, collectionId: 1, establishmentType: nil) { (responce) in
                //self.zomatoTrendingData = responce
                guard let restaurants = self.zomatoTrendingData?.restaurants else{
                    print("Was unable to load resturants even at failsafe")
                    return
                }
                self.trendingRestaurants = restaurants
                
            }
        }
        trendingCollectionView.reloadData()
        
    }
    
    
    private func loadToiletDataWithDistance(){
        
        let allToilets = AppDelegate.appDelegateShared().allPublicToilets
        let currentLocation = AppDelegate.appDelegateShared().currentLocation
        
        var allPublicToiletsWithDistance:[Toilets] = []
        for toilet in allToilets{
            guard let lats = toilet.lat else {print("No Lats");return}
            guard let longs = toilet.lon else {print("No Lons");return}
            let toiletLocation = CLLocation(latitude: lats, longitude: longs)
            let distance = toiletLocation.distance(from: currentLocation)
            let data = Toilets(toilet: toilet, distance: distance)
            allPublicToiletsWithDistance.append(data)
        }
        allPublicToiletsWithDistance.sort{$0.distance < $1.distance}
        allPublicToiletsAroundCurrentLocation = allPublicToiletsWithDistance
        
    }
    

    
    
    private func currentLocationNotification(){
        let currentLocation = AppDelegate.appDelegateShared().currentLocation
        let centerCBD = CLLocation(latitude: -37.815299, longitude: 144.962656)
        let distance  = centerCBD.distance(from: currentLocation)
        if distance > 10000{
            let title = "Away from Melbourne CBD"
            let message = "Your current location puts you away from Melbourne CBD. This app is customized to work around Melbourne CBD. Due to the unavailability of data around the current location some features (Toilets) may present unrealistic information"
            NotificationProvider.sharedInstance.alertUser(title: title, message: message, viewController: self)
            
        }
        
    }
    
   
    
    
    


}
