//
//  WebViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 8/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import JGProgressHUD
class WebViewController: UIViewController {
    
    var memberData: WheelyGoTeam!

    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Minon - iOS Developer"
        label.textColor = UIColor(hex: "ff9500")
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    lazy var webView: UIWebView = {
        let wb = UIWebView(frame: .zero)
        wb.delegate = self
        return wb
    }()
    
    var hud: JGProgressHUD?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(titleLabel)
        view.addSubview(webView)
        view.backgroundColor = UIColor.white
        
        titleLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, trailing: view.safeAreaLayoutGuide.trailingAnchor, padding: .init(top: 20, left: 10, bottom: 0, right: 0))
        webView.anchor(top: titleLabel.bottomAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0))
        hud = JGProgressHUD(style: .extraLight)
        setup()
    }
    
    private func setup(){
        hud?.show(in: self.view)
        let name = memberData.fullName
        let jobTitle = memberData.designation
        let urlString = memberData.linkedInURL
        
        if let url = URL(string: urlString){
            let header = "Meet \(name) our \(jobTitle)"
            titleLabel.text = header
            let request = URLRequest(url: url)
            webView.loadRequest(request)
            hud?.dismiss()
        }

    }
 

}


extension WebViewController: UIWebViewDelegate{
    
}


struct WheelyGoTeam{
    var linkedInURL: String
    var designation: String
    var fullName: String
    
    init(url: String, title: String, name: String) {
        self.linkedInURL = url
        self.designation = title
        self.fullName = name
    }
}
