//
//  WalkingPathViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 4/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import GoogleMaps

class MapViewController: UIViewController {
    
    private let cellId = "cellId"
    private let headerCellId = "headerCellId"
    
    var place: GooglePlaceFromId?
    
    var mapView: GMSMapView!
    
    private let locationManager = CLLocationManager()
    private var destination: CLLocation!
    
    private var routes:[Routes] = []
    
    let mapsViewHeight:CGFloat = 450.0
    
    //This time is created in the cell for item at method
    var modifiedTime: String?
    
    lazy var routesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionHeadersPinToVisibleBounds = true
        //layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .white
        //cv.isPagingEnabled = true
        return cv
    }()
    
    lazy var mapKitView: MKMapView = {
        let mapView = MKMapView()
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.mapType = MKMapType.standard
        mapView.delegate = self
        return mapView
    }()


    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        title = "Pedestrian Paths"
        view.addSubview(routesCollectionView) //Anchor is coded in setupGoogleMaps for convinience. CHANGE LATER
        routesCollectionView.register(RouteCell.self, forCellWithReuseIdentifier: cellId)
        routesCollectionView.register(RoutesHeaderCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerCellId)
        getCurrentLocation()
    
    }//end of view did load
    
    @objc
    private func handleTakeMeButton(){
        
    }
    
    
    private func getCurrentLocation(){
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //navigationController?.setNavigationBarHidden(true, animated: false)
        let monitoredRegions = locationManager.monitoredRegions
        print("Currently Monitoring \(monitoredRegions.count)  regions")
        setupGeoFence()
    }
    
    
    

    
}

//MARK: Location related methods
extension MapViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locationValue = manager.location?.coordinate else{return}
        let currentLocation = CLLocation(latitude: locationValue.latitude, longitude: locationValue.longitude)
        let destinationCoordinates = getDestinationCoordinates()
        getRouteInformation(sourceLocation: currentLocation, destinationLocation: destinationCoordinates)
        //MARK: setupGoogleMaps method
        setupGoogleMaps(currentPosition: currentLocation)
        locationManager.stopUpdatingLocation()
        

    }
    
    private func getRouteInformation(sourceLocation: CLLocation, destinationLocation: CLLocation){
        GoogleClient.sharedInstance.getRouteData(sourceCoordinates: sourceLocation, destinationCoordinates: destinationLocation) { (response) in
            guard let allRoutes = response.routes else {print("Unable to obtain routes");return}
            self.routes = allRoutes
            //Sorting depending on the distance. closest on top
            self.routes.sort{($0.legs?.last?.distance?.value)! < ($1.legs?.last?.distance?.value)!}
            self.routesCollectionView.reloadData()
            
        }
    }
    
    private func getDestinationCoordinates()->CLLocation {
        
        if let googlePlace = place {
            let latitude = googlePlace.result?.geometry?.location?.lat
            let longititude = googlePlace.result?.geometry?.location?.lng
            return CLLocation(latitude: latitude!, longitude: longititude!)
        }
        else{
            print("Place information is currently unavailable")
            return CLLocation(latitude: 0.0, longitude: 0.0)
        }
    }
}
//MARK: Google Maps Methods
extension MapViewController{
    private func setupGoogleMaps(currentPosition: CLLocation){
        let latitude = currentPosition.coordinate.latitude
        let longititude = currentPosition.coordinate.longitude
        
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longititude, zoom: 15)
        
        let height = view.frame.height - 250
        let mapFram = CGRect(x: 0, y: 0, width: view.frame.width, height: height)
        mapView = GMSMapView.map(withFrame: mapFram, camera: camera)
        view.addSubview(mapView)
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longititude)
//        marker.title = "Sydney"
//        marker.snippet = "Australia"
        
        marker.map = mapView
        
        //MARK: routesCollectionView anchor constraint
    
        
        routesCollectionView.anchor(top: mapView.bottomAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor)
        
    }
    

}

extension MapViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return routes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! RouteCell
        
        let route = routes[indexPath.item]
        cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.4)
        let distance = route.legs?.last?.distance?.value!
        let timeTakenForWC = timeForWheelChair(distance: distance!, sum: route.summary!)
        cell.distanceValue.text = route.legs?.last?.distance?.text
        cell.durationValue.text = timeTakenForWC
        cell.routeSummary.text = route.summary
        cell.stepButton.tag = indexPath.item
        cell.stepButton.addTarget(self, action: #selector(handleCellButton(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc
    private func handleCellButton(_ sender: UIButton){
        let index = sender.tag
        let route = routes[index]
        let polyLine = route.overview_polyline?.points
        showPath(polyString: polyLine!, mapView: mapView)
        
    }
    
    private func showPath(polyString: String, mapView: GMSMapView){
        let path = GMSPath(fromEncodedPath: polyString)
        let polyLine = GMSPolyline(path: path)
        polyLine.strokeWidth = 3.0
        polyLine.strokeColor = .blue
        polyLine.map = mapView
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerCellId, for: indexPath) as! RoutesHeaderCell
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let stepVC = StepsViewController()
        let route = routes[indexPath.item]
        let warnings = route.warnings
        stepVC.warningsPresent = includeWarning(warnings: warnings!)
        stepVC.warning = warnings
        stepVC.steps = route.legs?.last?.steps
        stepVC.route = route.legs?.last
        stepVC.routeSummary = route.summary
        stepVC.modifiedDuration = modifiedTime
        stepVC.polyLine = route.overview_polyline?.points
        let distance = route.legs?.last?.distance?.value!
        let timeTakenForWC = timeForWheelChair(distance: distance!, sum: route.summary!)
        stepVC.modifiedDuration = timeTakenForWC
        
        present(stepVC, animated: true, completion: nil)
        //navigationController?.pushViewController(stepVC, animated: true)
    }
    
    private func includeWarning(warnings: [String])->Bool{
        if warnings.count > 0{return true}
        else{return false}
    }
    
    private func timeForWheelChair(distance: Int, sum: String)->String{

        let dist = Double(distance)
        let speed = Double(3900) // speed in m/h
        let timeInSeconds = (dist/speed)*3600
        let timeAsInteger = Int(timeInSeconds)
        print("Time in seconds for \(sum) \(time) Travelling \(dist) Time as INT \(timeAsInteger)")
        return secondsToHoursMinutesSeconds(seconds: timeAsInteger)
        
    }
    
    private func secondsToHoursMinutesSeconds (seconds : Int) -> String {
        let hours = seconds/3600
        let mins = (seconds % 3600) / 60
        return "\(hours) hour \(mins) mins"
    }
}

extension MapViewController: MKMapViewDelegate{
    
    private func setupGeoFence(){
        let destination = getDestinationCoordinates()
        print("Adding \((place?.result?.name)!) for region monitoring")
        let coordinates = CLLocationCoordinate2DMake(destination.coordinate.latitude, destination.coordinate.longitude)
    
        let identifier = place?.result?.place_id
        
        if CLLocationManager.authorizationStatus() == .authorizedAlways{
            //Make sure region mnonitoring is supported
            if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self){
                let maxDistance = 200.0

                let region = CLCircularRegion(center: coordinates, radius: maxDistance, identifier:identifier!)
                region.notifyOnExit = true
                region.notifyOnEntry = true
                resetRegions()
                locationManager.startMonitoring(for: region)
                
            }
            else{
                print("Region monitoring is not available")
                NotificationProvider.sharedInstance.alertUser(title: "Error", message: "Geofencing is not supported on this device to ", viewController: self)
            }
            
            
        }
        else{
            print("Location Monitoring not authroized")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        GoogleClient.sharedInstance.getPlaceFromId(palceId: region.identifier) { (place) in
            print("Monitoring Region \((place.result?.name)!)")
            
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager failed with the following error: \(error)")
    }
    
    private func resetRegions(){
        for region in locationManager.monitoredRegions{
            locationManager.stopMonitoring(for: region)
        }
    }
    
}



    
    





