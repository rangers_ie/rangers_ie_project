//
//  MapViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 20/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import CoreLocation
import UberCore
import UberRides
import GoogleMaps
import GooglePlaces


class HomeViewController: UIViewController{
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    
    let reuseIdRecentSearch = "recenSearchCellId"
    let resuseIdAroundMe = "arundMeCellId"
    
    var allRecentPlaces:[RecentPlaceSummary] = []
    
    

    let homeView: HomeView = {
        let hv = HomeView()
        return hv
    }()
    
    let lineOne: UIView = {
        let line = UIView()
        line.backgroundColor = UIColor.lightGray
        return line
    }()
    
    let lineTwo: UIView = {
        let line = UIView()
        line.backgroundColor = UIColor.lightGray
        return line
    }()
    
    let lineThree: UIView = {
        let line = UIView()
        line.backgroundColor = UIColor.lightGray
        return line
    }()
    
    let yComPonenet:CGFloat = 400
    
    
    lazy var recentPlaces: UICollectionView = {
        let frame = CGRect(x: 0, y: view.frame.maxY , width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height-yComPonenet)
        let col = UICollectionView(frame: frame, collectionViewLayout: UICollectionViewFlowLayout())
        col.dataSource = self
        col.delegate = self
        col.isUserInteractionEnabled = true
        col.backgroundColor = UIColor.white
        col.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        return col
    }()
    
    let aroundMeItems = ["Cafe","Restaurant","Parks","Museums","Toilets","Events"]
    let iconNames = ["cafe","restaurant","toilets","tram","accessibility","under"]
    
    
    
    lazy var aroundMeCollectionView: UICollectionView = {
        let col = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        col.dataSource = self
        col.delegate = self
        col.backgroundColor = UIColor.white
        col.contentInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        col.isUserInteractionEnabled = true
        
        return col
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocationManager()
        view.backgroundColor = UIColor.white
        view.addSubview(homeView)
        view.addSubview(recentPlaces)
        view.addSubview(aroundMeCollectionView)
        
        
        view.addSubview(lineOne)
        view.addSubview(lineTwo)
        view.addSubview(lineThree)
        
        lineOne.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil, padding: .init(top: 30, left: 130, bottom: 30, right: 0), size: .init(width: 1, height: view.frame.height - 460) )
        
        lineTwo.anchor(top: nil, leading: nil, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 30, left: 0, bottom: 30, right: 130), size: .init(width: 1, height: view.frame.height - 460) )
        
        lineThree.anchor(top: homeView.segemented.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 155, left: 30, bottom: 0, right: 30), size: .init(width: 0, height: 1))
        
        
        homeView.anchor(top: view.topAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor)
        //aroundMeCollectionView.anchor(top: homeView.segemented.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 0))
    
        aroundMeCollectionView.frame = CGRect(x: 0, y: 400, width: view.frame.width, height: view.frame.height-400)
        
        
        //Registering Collection View Cells
        recentPlaces.register(RecentPlacesCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdRecentSearch)
        aroundMeCollectionView.register(AroundMeCollectionViewCell.self, forCellWithReuseIdentifier: resuseIdAroundMe)
    
    
        
        homeView.segemented.addTarget(self, action: #selector(handleSegmentedControl(sender:)), for: .valueChanged)
        homeView.segemented.selectedSegmentIndex = 0
        
        homeView.searchTextField.delegate = self
        
    

    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadData()
    }
    
    
    @objc
    private func handleSegmentedControl(sender: UISegmentedControl){
        
        let index = sender.selectedSegmentIndex

        if index == 1 {
            lineOne.isHidden = true
            lineTwo.isHidden = true
            lineThree.isHidden = true
            //Show the View
            
            if allRecentPlaces.count == 0 {
                let title = "You need to get moving"
                let message = "It looks like you haven't been anywhere with us. Come back here after you've used us to go places."
                NotificationProvider.sharedInstance.alertUser(title: title, message: message, viewController: self)
            }
            UIView.animate(withDuration: 0.35) {
                
                self.aroundMeCollectionView.frame.origin.y = self.view.frame.maxY
                self.recentPlaces.frame.origin.y = 400
            }
        }
        else{
            lineOne.isHidden = false
            lineTwo.isHidden = false
            lineThree.isHidden = false
            //Hide the view
            UIView.animate(withDuration: 0.35) {
                self.recentPlaces.frame.origin.y = self.view.frame.maxY
                self.aroundMeCollectionView.frame.origin.y = 400
            }
        }
    }
    
    private func setupLocationManager(){
       //Request Autoriziation from user to use the current location
        self.locationManager.requestAlwaysAuthorization()
        
        //Request Authorization from user to use location in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    

    private func setUpUber(){
        //Setting up single sign on : https://developer.uber.com/docs/riders/ride-requests/tutorials/api/ios#cocoapods
        let loginManager = LoginManager()
        loginManager.login(requestedScopes: [.request], presentingViewController: self) { (token, error) in
            if let error = error {
                print("Error occurred when trying to log in to uber AUTH")
                print(error.localizedDescription)
            }
            else{
                print("Succesfully logged in to uber \((token?.expirationDate)!)")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension HomeViewController: CLLocationManagerDelegate{
    //MARK: Delegate Methods from the CLLocationManagerDelegate
    //This method will be executed every time the users location gets updated
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //Getting the current cooridnates of the users current location
        guard let locationValue: CLLocationCoordinate2D = manager.location?.coordinate else{return}
        currentLocation = CLLocation(latitude: locationValue.latitude , longitude: locationValue.longitude)
        manager.stopUpdatingLocation()

        
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == aroundMeCollectionView{
            return aroundMeItems.count
        }
        else{
            return allRecentPlaces.count
        }
    
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == aroundMeCollectionView{

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: resuseIdAroundMe, for: indexPath) as! AroundMeCollectionViewCell
            cell.icon.setImage(UIImage(named: iconNames[indexPath.item]), for: .normal)
            cell.label.text = iconNames[indexPath.item].capitalizingFirstLetter()
            return cell
            
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdRecentSearch , for: indexPath) as? RecentPlacesCollectionViewCell
            let recentPlace = allRecentPlaces[indexPath.item]
            cell?.label.text = recentPlace.PlaceName
            
            
            
            return cell!
        }
        

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == aroundMeCollectionView {
            let height = (collectionView.frame.height/2) - 60
            let width = (collectionView.frame.width/3) - 20
            return CGSize(width: width, height: height)
        }
        else{
            let width = (view.frame.width) - 10
            let size = CGSize(width: width, height: 50)
            return size
        }

    }
    
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == aroundMeCollectionView {
        
            if indexPath.item == 5{
                underConsAlert()
            }
                
            else if(indexPath.item == 2){
                let pubToils = PublicToiletsCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
                navigationController?.pushViewController(pubToils, animated: true)
            }
                
            else if(indexPath.item == 3){
                
                let ptvController = PTVViewController()
                navigationController?.pushViewController(ptvController, animated: true)
    
            }
            
            else if(indexPath.item == 4){
                //let accessibilityVC = AccessibilityInformationTableViewController()
                
                let acces = NewBuildingAccessibiltyTableViewController()
                navigationController?.pushViewController(acces, animated: true)
            }
                
            else
            {
                //Starting Location Manager to get the latest location
                //This triggers the did update method for location delegate
                locationManager.startUpdatingLocation()
                
                let allPlacesController = AllPlacesCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
                let locationType = aroundMeItems[indexPath.item].lowercased()
                
                //Check if current location exist
                guard let currentLoc = currentLocation else {print("Current Location Not Updated"); return}
                
                let locInfo = LocationInfo(coord: currentLoc, type: locationType)
                allPlacesController.locationInformation = locInfo
                
                locationManager.stopUpdatingLocation()
                navigationController?.pushViewController(allPlacesController, animated: true)
            }
        }
        else{
            print("Recent Search Cell")
            underConsAlert()
        }
    }
    
    private func underConsAlert(){
        let title = "Ooops.. Curious One aren't you"
        let message = "Unfortunately these facilities have not been implemented yet. Please check us out in two weeks"
        NotificationProvider.sharedInstance.alertUser(title: title, message: message, viewController: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == aroundMeCollectionView {
            return 0
        }
        return 1
    }
    
  
}

extension HomeViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        //Taking foucs away from the textfield
        homeView.searchTextField.resignFirstResponder()
        let gooleSearchController = GoogleAutoCompleteViewController()
        navigationController?.pushViewController(gooleSearchController, animated: true)
    }
    
    
}

extension HomeViewController{
    private func loadData(){
        FirebaseDBProvider.getRecentlyVisied { (responce) in
            self.allRecentPlaces = responce
            self.recentPlaces.reloadData()

        }
    }

    private func populatedCell(placeId: String, cell: RecentPlacesCollectionViewCell){

    }
}









