//
//  OnboardingViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 1/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import SwiftyOnboard

class OnboardingViewController: UIViewController {
    
    let titles = [Onboarding.INTRO, Onboarding.CAFE_TITLE, Onboarding.REST_TITLE, Onboarding.TOILETS_TITLE, Onboarding.TRAM_TITLE, Onboarding.ACCES_TITLE]
    
    let descriptions = [Onboarding.INTRO_DESC, Onboarding.CAFE_DESC, Onboarding.REST_DESC, Onboarding.TOILETS_DESC, Onboarding.TRAM_DESC, Onboarding.ACCES_DESC]
    
    let imageNames = ["intro","cafe","restaurant","toilets","tram","accessibility"]

    let swiftyOnboard = SwiftyOnboard(frame: .zero)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        swiftyOnboard.frame = view.frame
        
        
        
        view.addSubview(swiftyOnboard)
        swiftyOnboard.dataSource = self
        swiftyOnboard.delegate = self
        // Do any additional setup after loading the view.
    }

}

extension OnboardingViewController: SwiftyOnboardDataSource{
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: SwiftyOnboard) -> Int {
        return titles.count
    }
    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: SwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        
        let page = SwiftyOnboardPage()
        page.backgroundColor = UIColor.white
        
        let imageName = imageNames[index]
        let title = titles[index]
        let desc = descriptions[index]
        
        let image = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
        page.imageView.image = image
        
        page.title.text = title
        page.subTitle.text = desc
        return page
    }
    func swiftyOnboardOverlayForPosition(_ swiftyOnboard: SwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
        
        overlay.skipButton.tag = Int(position)
    }
   
}

extension OnboardingViewController: SwiftyOnboardDelegate{
    func swiftyOnboard(_ swiftyOnboard: SwiftyOnboard, currentPage index: Int) {
        
    }
    
    func swiftyOnboardViewForOverlay(_ swiftyOnboard: SwiftyOnboard) -> SwiftyOnboardOverlay? {
        let overLay = SwiftyOnboardOverlay()
        overLay.continueButton.addTarget(self, action: #selector(handleContinue(_:)), for: .touchUpInside)
        overLay.skipButton.addTarget(self, action: #selector(handleSkip(_:)), for: .touchUpInside)
        overLay.continueButton.setTitle("Go to app", for: .normal)
        return overLay
    }
    
    @objc
    private func handleSkip(_ sender: UIButton){
        let index = sender.tag
        if index + 1 < titles.count {
            swiftyOnboard.goToPage(index: index + 1, animated: true)
        }
        

    }
    @objc
    private func handleContinue(_ sender: UIButton){
        let homeView = HomeViewController()
        navigationController?.pushViewController(homeView, animated: true)
    }
}
