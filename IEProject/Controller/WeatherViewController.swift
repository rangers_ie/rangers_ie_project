//
//  WeatherViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 17/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import CoreLocation
import JGProgressHUD
class WeatherViewController: UIViewController {
    
    var googlePlace: GooglePlaceFromId?
    
    let weatherView: WeatherView = {
        let weatherView = WeatherView()
        weatherView.layer.cornerRadius = 10
        return weatherView
    }()

    var hud: JGProgressHUD?
    override func viewDidLoad() {
        super.viewDidLoad()
        hud = JGProgressHUD(style: .extraLight)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        view.addSubview(weatherView)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleOnScreenTap))
        
        view.addGestureRecognizer(tap)
        
        weatherView.centerAnchor(centerX: view.centerXAnchor, centerY: view.centerYAnchor, size: .init(width: 250, height: 250), padding: .zero)
        hud?.show(in: self.view)
        setupWeatherInfo()
        
        // Do any additional setup after loading the view.
    }
    
    private func setupWeatherInfo(){
        let lats = googlePlace?.result?.geometry?.location?.lat
        let longs = googlePlace?.result?.geometry?.location?.lng
        let location = CLLocation(latitude: lats!, longitude: longs!)
        APIClient.sharedInstance.getWeatherInformation(location: location) { (weatherResult) in
            guard let weatherData = weatherResult.currently else {return}
            
            let temerature:Double = self.convertToCelcius(temp: weatherData.apparentTemperature!)
            let humidity = weatherData.humidity!*100
            let wind = weatherData.windSpeed!
            let visibility = weatherData.visibility!
            let probability = weatherData.precipProbability!*100
        
            self.weatherView.temperatuteTabel.text = "Temperature \((temerature.description)) C"
            self.weatherView.windSpeed.text = "Wind speed \((wind.description)) km/h"
            self.weatherView.humidity.text = "Humidity \((humidity.description)) %"
            self.weatherView.visibility.text = "Visibility \((visibility.description)) km"
            self.weatherView.timeTaken.text = "Chance of rain \(probability) %"
            self.hud?.dismiss()
            
            
        }
        
    }
    
    private func setupTime(timeStamp: Double)->String{
        let date = Date(timeIntervalSince1970: timeStamp)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    private func convertToCelcius(temp: Double)->Double{
        let a = temp - 32
        let b = a * 5/9
        return b.rounded(toPlaces: 2)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc
    private func handleOnScreenTap(){
        dismiss(animated: true, completion: nil)
    }
    

}
