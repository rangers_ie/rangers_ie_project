//
//  PlaceDetailsViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 30/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import UberRides
import CoreLocation
import SDWebImage

class PlaceDetailsViewController: UIViewController {
    
    var place: GooglePlaceFromId?
    var fullTextForPopularHours = ""
    
    
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.backgroundColor = UIColor.red
        sv.contentSize.height = 2000
        sv.isUserInteractionEnabled = false
        sv.isExclusiveTouch = false
        sv.canCancelContentTouches = false
        sv.delaysContentTouches = false
        return sv
    }()

    let placeDetailView: PlaceDetailsView = {
        let detailView = PlaceDetailsView()
        return detailView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPopularHoursTitle()
        getUserRatingFromFirebase()
        view.addSubview(placeDetailView)
        placeDetailView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor)

        placeDetailView.backButton.addTarget(self, action: #selector(handleBackButton), for: .touchUpInside)
        placeDetailView.phoneNumber.addTarget(self, action: #selector(handlePhoneNumberButton), for: .touchUpInside)
        placeDetailView.reviewButton.addTarget(self, action: #selector(handleViewReview), for: .touchUpInside)
        placeDetailView.leaveReviewButton.addTarget(self, action: #selector(handleLeaveReview), for: .touchUpInside)
        placeDetailView.weatherButton.addTarget(self, action: #selector(handleWeatherVC), for: .touchUpInside)
        placeDetailView.checkBusyTimes.addTarget(self, action: #selector(handleCheckBusyTime), for: .touchUpInside)
        placeDetailView.reviewInfoButton.addTarget(self, action: #selector(handleReviewInfoButton), for: .touchUpInside)
        
        
    }
    
    @objc
    private func handleReviewInfoButton(){
        let title = "User Feedback"
        let message = "We compute these scores based on the feedback provided by other WheelyGo users"
        NotificationProvider.sharedInstance.alertUser(title: title, message: message, viewController: self)
    }
    
    @objc
    private func handleCheckBusyTime(){
        
        let popTimeVC = PopularTimeViewController()
        popTimeVC.popularHoursString = fullTextForPopularHours
        popTimeVC.modalTransitionStyle = .coverVertical
        popTimeVC.modalPresentationStyle = .overCurrentContext
        present(popTimeVC, animated: true, completion: nil)
    }
    
    @objc
    private func handleWeatherVC(){
        let weatherVC = WeatherViewController()
        weatherVC.googlePlace = place
        weatherVC.modalPresentationStyle = .overCurrentContext
        weatherVC.modalTransitionStyle = .crossDissolve
        present(weatherVC, animated: true, completion: nil)
    }
    
    @objc
    private func handleBackButton(){
        //navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    
    @objc
    private func handlePhoneNumberButton(){
        let phoneNumber = (place?.result?.international_phone_number)!.removingWhitespaces()
        guard let number = URL(string: "tel://\(phoneNumber)") else {print("Invalid Number"); return }
        UIApplication.shared.open(number)
        
    }
    
    @objc
    private func handleViewReview(){
        let reviewVC = ReviewsTableViewController()
        reviewVC.googlePlace = place
        let navEditorViewController: UINavigationController = UINavigationController(rootViewController: reviewVC)
        present(navEditorViewController, animated: true, completion: nil)
    }
    
    
    @objc
    private func handleLeaveReview(){
        let feedbackVC = FeedbackViewController()
        feedbackVC.googlePlace = place
        present(feedbackVC, animated: true, completion: nil)
    }
    
    
    
    
    func setUpUberButton(location: CLLocation, locationName: String, address: String){
        let uberButton = RideRequestButton()
        let builder = RideParametersBuilder()
        builder.dropoffLocation = location
        builder.dropoffNickname = locationName
        builder.dropoffAddress = address
        uberButton.rideParameters = builder.build()
        view.addSubview(uberButton)
        uberButton.anchor(top: placeDetailView.phoneNumber.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 10, left: 20, bottom: 0, right: 20))
    }
    
    override func viewWillAppear(_ animated: Bool){
        navigationController?.setNavigationBarHidden(true, animated: false)
        setup()
    }
    
    private func setup(){
        let latitude = place?.result?.geometry?.location?.lat
        let longititude = place?.result?.geometry?.location?.lng
        let coordinates = CLLocation(latitude: latitude!, longitude: longititude!)
        let locationName = place?.result?.name
        let phoneNumber = place?.result?.formatted_phone_number
        let address = place?.result?.formatted_address
        
        
        placeDetailView.nameLabel.text = locationName
        placeDetailView.address.text = address
        placeDetailView.phoneNumber.setTitle(phoneNumber, for: .normal)
        
        
        

    
        let imageReference = place?.result?.photos?.first?.photo_reference
        if imageReference != nil{
            let imageURL = GoogleClient.sharedInstance.getImageURL(pictureReference: imageReference!)
            placeDetailView.imageView.setImageWith(imageURL)
        }
        
        
//        let message = "Unfortunately we do not have accessibility information for \(locationName!). Please click links below to view other reviews or leave your feedback about \(locationName!)"
//        placeDetailView.accessLabel.text = message
        
        //Setup Uber Button
        setUpUberButton(location: coordinates,locationName: locationName!, address: address!)
        setBusyAlert()
    }
    

    
 
}

extension PlaceDetailsViewController: RideRequestViewDelegate{
    func rideRequestView(_ rideRequestView: RideRequestView, didReceiveError error: NSError) {
        
    }
}

extension PlaceDetailsViewController{
    
    
    private func getPopularHoursTitle(){
        
        let lats = (place?.result?.geometry?.location?.lat)!
        let longs = (place?.result?.geometry?.location?.lng)!
        let location = CLLocationCoordinate2DMake(lats, longs)
        
        APIClient.sharedInstance.getFourSquareData(currentLocation: location) { (fourSquare) in
            guard let responce = fourSquare.response?.venues?.last else {return}
            
            let venueId = responce.id!
            APIClient.sharedInstance.getMostPopularTime(venueId: venueId, completition: { (venueResponce) in
                guard let responce = venueResponce.response else {return}
                if let timeFrames = responce.popular?.timeframes{
                    
                    if timeFrames.count > 0 {
                        for time in timeFrames {
                            
                            guard let day = time.days else {return}
                            guard let popTime = time.open else {return}
                            
                            switch day {
                            case[7]:
                                let day = "Sunday"
                                self.generatePopularTimeString(popularTime: popTime, day: day)
                            case[1]:
                                let day = "Monday"
                                self.generatePopularTimeString(popularTime: popTime, day: day)
                            case[2]:
                                let day = "Tuesday"
                                self.generatePopularTimeString(popularTime: popTime, day: day)
                            case[3]:
                                let day = "Wednesday"
                                self.generatePopularTimeString(popularTime: popTime, day: day)
                            case[4]:
                                let day = "Thursday"
                                self.generatePopularTimeString(popularTime: popTime, day: day)
                            case[5]:
                                let day = "Friday"
                                self.generatePopularTimeString(popularTime: popTime, day: day)
                            case[6]:
                                let day = "Saturday"
                                self.generatePopularTimeString(popularTime: popTime, day: day)
                            default:
                                print("default")
                            }
                            
                        }//End of For statment
                    }//Send of if timeFrames.count > 0
                    
                    else{
                        self.fullTextForPopularHours = "Looks like we are still figuring \n this place out!"
                    }
                    //print(self.fullTextForPopularHours)
                }
                else{
                    self.fullTextForPopularHours = "Looks like we are still figuring \n this place out!"
                }
                
            })
        }
    }
    
    private func generatePopularTimeString(popularTime: [OpenForPopular], day: String){
    
        fullTextForPopularHours += "\(day) between"
        if popularTime.count > 1{
            
            for time in popularTime{
                
                let startTime = convertFromMilitaryTime(timeString: time.start!)
                let endTime = convertFromMilitaryTime(timeString: time.end!)
                fullTextForPopularHours += " \(startTime) to \(endTime) \n\n"
            }
        }
        else{
            
            
            if let time = popularTime.last {
                let startTime = convertFromMilitaryTime(timeString: time.start!)
                let endTime = convertFromMilitaryTime(timeString: time.end!)
                fullTextForPopularHours +=  " \(startTime) to \(endTime) \n\n"
            }
            //fullTextForPopularHours += "Time Unavailable"
    
   
            
        }
    }
    
    private func setBusyAlert(){
        
        let currentDateTime = Date()
        print(currentDateTime)

    }
    
    private func convertFromMilitaryTime(timeString: String)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HHmm"
        
        if let date = dateFormatter.date(from: timeString){
            dateFormatter.dateFormat = "h:mm a"
            let Date12 = dateFormatter.string(from: date)
            return Date12
        }
       
        return "ND"
    }
    
    
    
}

extension PlaceDetailsViewController{
    private func getUserRatingFromFirebase(){
        var questionOneYesCount = 0
        var questionTwoYesCount = 0
        var questionThreeYesCount = 0

        var questionOneNoCount = 0
        var questionTwoNoCount = 0
        var questionThreeNoCount = 0
        
       
        let userId = place?.result?.place_id
        FirebaseDBProvider.getUserRating(placeID: userId!) { (results, message) in
            if message == FirebaseConstants.FAIL{
                self.hideLabelsForReviews()
            }
            else{
                self.showLabelsForReviews()
                for result in results {
                    switch result.Question01{
                    case "Yes":
                        questionOneYesCount += 1
                    case "No":
                        questionOneNoCount += 1
                    default:
                        print("default")
                    }
                    switch result.Question02{
                    case "Yes":
                        questionTwoYesCount += 1
                    case "No":
                        questionTwoNoCount += 1
                    default:
                        print("default")
                    }
                    switch result.Question03{
                    case "Yes":
                        questionThreeYesCount += 1
                    case "No":
                        questionThreeNoCount += 1
                    default:
                        print("default")
                        
                    }
                }
                let totalForQuestionOne = Double(questionOneYesCount) + Double(questionOneNoCount)
                let totalForQuestionTwo = Double(questionTwoYesCount) + Double(questionTwoNoCount)
                let totalForQuestionThree = Double(questionThreeYesCount) + Double(questionThreeNoCount)
                
                
                let yesPercentageForQuestionOne = (Double(questionOneYesCount)/totalForQuestionOne).rounded(toPlaces: 2)*100
                
                let yesPercentageForQuestionTwo = (Double(questionTwoYesCount)/totalForQuestionTwo).rounded(toPlaces: 2)*100
                
                let yesPercentageForQuestionThree = (Double(questionThreeYesCount)/totalForQuestionThree).rounded(toPlaces: 2)*100
                
                
                self.placeDetailView.rampAccessLabel.text = "\(yesPercentageForQuestionOne)% of guest said this place has ramp access"
                self.placeDetailView.adequareSpaceLabel.text = "\(yesPercentageForQuestionTwo)% of guest said this place has enough room"
                self.placeDetailView.friendlyStaffLabel.text = "\(yesPercentageForQuestionThree)% of guest said the staff is friendly"
                
            }
 
  
          
        }
    }
    
    
    
    private func hideLabelsForReviews(){
        placeDetailView.rampAccessLabel.isHidden = true
        placeDetailView.adequareSpaceLabel.isHidden = true
        placeDetailView.friendlyStaffLabel.isHidden = true
        placeDetailView.noReviewsLabel.isHidden = false
    }
    
    private func showLabelsForReviews(){
        placeDetailView.noReviewsLabel.isHidden = true
        placeDetailView.rampAccessLabel.isHidden = false
        placeDetailView.adequareSpaceLabel.isHidden = false
        placeDetailView.friendlyStaffLabel.isHidden = false
    }
}



