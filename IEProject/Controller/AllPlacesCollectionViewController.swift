//
//  AllPlacesCollectionViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 27/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import AFNetworking
import CoreLocation


class LocationInfo: NSObject {
    var coordinates: CLLocation?
    var locationType: String?
    
    
    init(coord: CLLocation, type: String) {
        self.coordinates = coord
        self.locationType = type
    }
    
}

class AllPlacesCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var locationInformation: LocationInfo?
    var allPlacess: [GoogleResults] = []
    
    let imageNames = ["cafePicOne","cafePicTWo","restPicOne","restPicTwo"]
    
    
 
    
    private let reuseIdentifier = "Cell"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = locationInformation?.locationType?.capitalizingFirstLetter()
        collectionView?.backgroundColor = UIColor.white
        self.collectionView!.register(PlaceCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
    }
    
    
    
  
    
    
    override func viewWillAppear(_ animated: Bool) {
        loadGooglePlacesData()
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    


    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return allPlacess.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PlaceCollectionViewCell
        let place = allPlacess[indexPath.item]
        let imageReference = place.photos?.first?.photo_reference
        
        
        if imageReference != nil {
            let imageURL = GoogleClient.sharedInstance.getImageURL(pictureReference: imageReference!)
            cell.imageView.setImageWith(imageURL)}
        else{
            //Assign Static Image when image is not present
            cell.imageView.image = UIImage(named: "cafePicOne")}
        cell.placeName.text = place.name
        setDistanceFromCurrentLocation(googlePlace: place, cellLabel: cell)
        
    
        cell.contentView.layer.cornerRadius = 4.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        cell.layer.shadowRadius = 4.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        

        
        
        
        
        
        
        
    
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 150)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        
        let tabBarController = TabBarViewController()
        tabBarController.locationType = locationInformation?.locationType //Passed to say if its Cafe or Restaurant
        tabBarController.placeID = allPlacess[indexPath.item].place_id
        
        tabBarController.modalTransitionStyle = .flipHorizontal
        present(tabBarController, animated: true, completion: nil)
        //navigationController?.pushViewController(tabBarController, animated: true)
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension AllPlacesCollectionViewController{
    private func loadGooglePlacesData(){
        if let locationData = locationInformation {
            GoogleClient.sharedInstance.getPlacesAroundGivenLocation(coordinates: locationData.coordinates!, locationType: locationData.locationType!) { (response) in
                self.allPlacess = response.results!
                self.collectionView?.reloadData()
            }

        }
    }
    
    
    private func setDistanceFromCurrentLocation(googlePlace: GoogleResults, cellLabel: PlaceCollectionViewCell){
        
        let placeLats = googlePlace.geometry?.location?.lat
        let placeLongs = googlePlace.geometry?.location?.lng
        let placeCoordinates = CLLocation(latitude: placeLats!, longitude: placeLongs!)
        
        
        GoogleClient.sharedInstance.getRouteData(sourceCoordinates: (locationInformation?.coordinates)!, destinationCoordinates: placeCoordinates) { (responce) in
            let distanceFromSource = (responce.routes?.last?.legs?.last?.distance?.text)!
            let displayString = "\(distanceFromSource) from you"
            cellLabel.distanceFromYouLabel.text = displayString
        }
    }
}


