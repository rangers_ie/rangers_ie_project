//
//  TramMoreInfoModal.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 7/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class TramMoreInfoModal: UIViewController {
    
    
    let tramModal: MoreInfoTramModalView = {
        let view = MoreInfoTramModalView()
        view.layer.cornerRadius = 10
        return view
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        view.addSubview(tramModal)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleOnScreenTap))
        view.addGestureRecognizer(tap)
        tramModal.centerAnchor(centerX: view.centerXAnchor, centerY: view.centerYAnchor, size: .init(width: view.frame.width-40, height: 200), padding: .zero)
        
        let message = "Unfortunately we do not have accessibility\n information for tram stops.\n However, if you know your tram route we can \n let you know if the tram stops on that route are accessible.\n Thank You for Your Understanding."
        
        tramModal.messageLabe.text = message
        tramModal.accessibleRoutesButton.setTitle("Show me tram routes", for: .normal)
        
        tramModal.accessibleRoutesButton.addTarget(self, action: #selector(handleShowAccessibleTramRoutes), for: .touchUpInside)
    }
    
    
    @objc
    private func handleOnScreenTap(){
        dismiss(animated: true, completion: nil)
    }
    
    @objc
    private func handleShowAccessibleTramRoutes(){
   
        let ptvControntroller = PTVViewController()
        
        present(ptvControntroller, animated: true, completion: nil)
        //navigationController?.pushViewController(ptvControntroller, animated: true)
    }

  

}
