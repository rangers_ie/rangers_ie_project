//
//  PTVViewController.swift
//  IEProject
//
//  Created by Rhea Tuteja on 22/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import SwiftPTV
import CoreLocation
class PTVViewController: UIViewController {
    
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    
    private let FullyAccessibleTramRoutes = [19, 96, 109]
    private let PartiallyAccessibleTramRoute = [5, 6, 11, 16, 48, 58, 72, 86]
    
    let ptvView: PTVView = {
        let view = PTVView()
        return view
    }()
    
    
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.delegate = self
        tv.dataSource = self
        tv.backgroundColor = UIColor.white
        
        return tv
    }()

    let cellId = "cellId"
    var allStopsOnRoute: [StopOnRoute] = []
    var allStopsWithDistances: [TramGoogleDirection] = []
    
    //When the screen loads up initial display would be to show trams enroute
    var lookingNearByTrams: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        title = "PTV"
        
        view.addSubview(ptvView)
        view.addSubview(tableView)
       
        
        ptvView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .zero, size: .init(width: view.frame.width, height: 170))
    
        tableView.anchor(top: ptvView.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        tableView.register(StopTableViewCell.self, forCellReuseIdentifier: cellId)

        ptvView.tramRouteTextField.delegate = self
        
        
        ptvView.backButton.addTarget(self, action: #selector(handleBackButton), for: .touchUpInside)
        
        ptvView.sortButton.isHidden = true
        ptvView.sortButton.addTarget(self, action: #selector(handleSort), for: .touchUpInside)
        
        handleEmptyTableView()
        
        setupLocationManager()

    }
    
    @objc
    private func handleBackButton(){
        //navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    private func handleEmptyTableView(){

        if allStopsOnRoute.count == 0 {
            let view = EmptyTableView()
            tableView.backgroundView = view
            
        }
    }
}

extension PTVViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == ptvView.tramRouteTextField{
            textField.resignFirstResponder()
            let ptvTV = PTVTableViewViewController()
            ptvTV.delegate = self
            let navControlle = UINavigationController(rootViewController: ptvTV)
            present(navControlle, animated: true, completion: nil)
            
        }
     
    }
}

//MARK: Delegate Method for passing the Route
extension PTVViewController: PassDataToPTVControllerDelegate {
    func passRoute(route: Route) {

        ptvView.tramRouteTextField.text = route.name
        let routeId = route.ID!
        guard let routeNumber = route.number else {print("Cannot convert ROUTE TO  INT"); return}
        setAccessibilityNotice(routeNumber: routeNumber)
        guard let routeTypeId = route.type else {print("ROUTE TYPE ID NOT AVAILABLE");return}
        getRouteType(routeTypeID: routeTypeId) { (routeType) in
            PTVProvider.ptvAPI.retrieveStopsOnRoute(routeID: routeId, routeType: routeType, parameters: nil, { (responce) in
                guard let allStopsOnRouteRes = responce?.stops else {print("NO STOPS TO DISPLAY");return}
                
                DispatchQueue.main.async {
                    self.allStopsOnRoute = allStopsOnRouteRes
                    //This will trigger delegate method for CLLocation
                    //Deleate method will create the new list and reload data to table view
                    self.locationManager.startUpdatingLocation()
                }
            })
            
        }
    }
    
    private func setAccessibilityNotice(routeNumber: String){
        ptvView.accessibleNoticeLabel.isHidden = false
        guard let routeId = Int(routeNumber) else {print("Unable to convert to Int"); return}
        if FullyAccessibleTramRoutes.contains(routeId){
            ptvView.accessibleNoticeLabel.text = "Fully Accessible Tram Route"
            ptvView.blackView.backgroundColor = UIColor(displayP3Red: 0, green: 122/255, blue: 1, alpha: 1)
        }
        else if PartiallyAccessibleTramRoute.contains(routeId){
            ptvView.accessibleNoticeLabel.text = "Partially Accessible Tram Route"
            ptvView.blackView.backgroundColor = UIColor(displayP3Red: 1, green: 149/255, blue: 0, alpha: 1)
        }
        else{
            ptvView.accessibleNoticeLabel.text = "Accessibility Not Determined"
            ptvView.blackView.backgroundColor = UIColor(displayP3Red: 1, green: 59/255, blue: 48/255, alpha: 1)
        }
    }
    
    private func getRouteType(routeTypeID: Int, completition: @escaping(RouteType)->()){
 
        PTVProvider.ptvAPI.retrieveRouteTypes(parameters: nil) { (responce) in
            guard let allRouteTypes = responce?.routeTypes else {print("NO ROUTE TYPES"); return}
            for routeType in allRouteTypes{
                let routeTypeIDFromAPI = routeType.type
                if routeTypeID == routeTypeIDFromAPI{
                    DispatchQueue.main.async {completition(routeType)}
                }
            }
        }
    }
}

extension PTVViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return allStopsWithDistances.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! StopTableViewCell
        let stop = allStopsWithDistances[indexPath.item]
        let distanceFromCurrentLocation = stop.googleDir.routes?.last?.legs?.last?.distance?.text
        
        cell.stopName.text = stop.tramStop.name
        
        if let distance = distanceFromCurrentLocation {
            cell.distanceFromLabel.text = "\(distance) from current location"
        }
        else
        {
            cell.distanceFromLabel.text = "Distance currently unavailable"
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stop = allStopsWithDistances[indexPath.item]
        let latitude = stop.tramStop.latitude!
        let longitude = stop.tramStop.longitude!
        let location = CLLocation(latitude: latitude, longitude: longitude)
        GoogleClient.sharedInstance.openGoogleMaps(route: location)
        
    }
    
    
 

   
}

//MARK: Location Manager Delegages
extension PTVViewController: CLLocationManagerDelegate{
    
    private func setupLocationManager(){
        //Request Autoriziation from user to use the current location
        self.locationManager.requestAlwaysAuthorization()
        
        //Request Authorization from user to use location in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = manager.location?.coordinate else {print("Unable to get current Loc"); return}
        let clLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
        
        for stop in allStopsOnRoute{
            let latitude = stop.latitude!
            let longitude = stop.longitude!
            
            let stopLocation = CLLocation(latitude: latitude, longitude: longitude)
            GoogleClient.sharedInstance.getRouteData(sourceCoordinates: clLocation, destinationCoordinates: stopLocation) { (googleResponce) in
                let tramWithDirection = TramGoogleDirection(stop: stop, dir: googleResponce)
                self.allStopsWithDistances.append(tramWithDirection)
                self.locationManager.stopUpdatingLocation()
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    @objc
    private func handleSort(){
        //Sorting the results based on the distance
        self.allStopsWithDistances.sort{($0.googleDir.routes?.last?.legs?.last?.distance?.value)! < ($1.googleDir.routes?.last?.legs?.last?.distance?.value)!}
        tableView.reloadData()
    }
    
}


struct TramGoogleDirection {
    var tramStop: StopOnRoute
    var googleDir: GoogleDirections
    
    init(stop: StopOnRoute, dir: GoogleDirections) {
        self.tramStop = stop
        self.googleDir = dir
    }
}



