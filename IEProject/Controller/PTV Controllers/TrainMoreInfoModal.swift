//
//  PTVMoreInfoModalController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 4/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import SwiftPTV
import CoreLocation
class TrainMoreInfoModal: UIViewController {
    
    
    let modalView: MoreInfoModalView = {
        let view = MoreInfoModalView()
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        return view
    }()
    

    var stop: StopGeosearch?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        setup()
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleOnScreenTap))
        view.addGestureRecognizer(tap)
        modalView.takeMeButton.addTarget(self, action: #selector(handleTakeMeButton), for: .touchUpInside)
    

        // Do any additional setup after loading the view.
    }
    
    @objc
    private func handleTakeMeButton(){
        guard let latitude = stop?.latitude else {return}
        guard let longitude = stop?.longitude else {return}
        let location = CLLocation(latitude: latitude, longitude: longitude)
        GoogleClient.sharedInstance.openGoogleMaps(route: location)
        
    }
    
    private func setup(){
        view.addSubview(modalView)
        modalView.centerAnchor(centerX: view.safeAreaLayoutGuide.centerXAnchor, centerY: view.safeAreaLayoutGuide.centerYAnchor, size: CGSize(width: view.frame.width-40, height: 300), padding: .zero)
       
    }
    
    private func populateAccessibilityInfo(stopInfo: StopResponse){
        guard let name = stopInfo.stop?.name else {return}
        let stationName = "\(name) Station"
        modalView.stationNameLabel.text = stationName
        let buttonTitle = "Take me to \(stationName)"
        modalView.takeMeButton.setTitle(buttonTitle, for: .normal)
        
        guard let wheelchairSuppoer = stopInfo.stop?.accessibility?.wheelchair else {return}
        guard let accessible_ramp = wheelchairSuppoer.accessibleRamp else {return}
        guard let accessible_parking = wheelchairSuppoer.accessibleParking else {return}
        guard let accessible_phone = wheelchairSuppoer.accessiblePhone else {return}
        guard let accessible_toilet = wheelchairSuppoer.accessibleToilet else {return}
        
        let imageGreenTick = "greenTick"
        let imageRedCross = "redCross"
        
        let rampStatusLabel = modalView.accessibleRamp
        let parkingStatusLabel = modalView.accessibleParking
        let accessiblePhoneLabel = modalView.accessiblePhone
        let accessibleToilet = modalView.accessibleToilet
        if accessible_ramp{
            
            setAccessibility(label: rampStatusLabel, labelText: "Wheelchair Accessible", imageName: imageGreenTick)}
        else{setAccessibility(label: rampStatusLabel, labelText: "Wheelchair Accessible", imageName: imageRedCross)}
        
        if accessible_parking{
            setAccessibility(label: parkingStatusLabel, labelText: "Accessible Parking", imageName: imageGreenTick)}
        else{setAccessibility(label: parkingStatusLabel, labelText: "Accessible Parking", imageName: imageRedCross)}
        
        if accessible_phone{
            setAccessibility(label: accessiblePhoneLabel, labelText: "Accessible Phone", imageName: imageGreenTick)}
        else{setAccessibility(label: accessiblePhoneLabel, labelText: "Accessible Phone", imageName: imageRedCross)}
        
        if accessible_toilet{
            setAccessibility(label: accessibleToilet, labelText: "Accessible Toilet", imageName: imageGreenTick)}
        else{setAccessibility(label: accessibleToilet, labelText: "Accessible Toiler", imageName: imageRedCross)}
        
    
    }
    
    private func setAccessibility(label: UILabel, labelText: String, imageName: String){
        let image = UIImage(named: imageName)
        
        //Create Attachment
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = image
        //Set bound to reposition
        let imageOffsetY:CGFloat = -5.0;
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        //Add image to mutable string
        completeText.append(attachmentString)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: " \(labelText)")
        completeText.append(textAfterIcon)
        label.textAlignment = .center;
        label.attributedText = completeText;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let stopId = stop?.ID {
            loadStopInformation(stopID: stopId)
        }
    }
    
    
    @objc
    private func handleOnScreenTap(){
        dismiss(animated: true, completion: nil)
    }
    
  
    

    private func loadStopInformation(stopID: Int){
        let routeType = AppDelegate.appDelegateShared().allRouteTypes[0]
        let prams = ["stop_amenities":"true","stop_accessibility":"true"]
    
        PTVProvider.ptvAPI.retrieveStopDetails(stopID: stopID, routeType: routeType, parameters: prams) { (stopResponce) in
            guard let stopInfo = stopResponce else {print("Unable to get STOP INFORMATION");return}
            
            DispatchQueue.main.async {
                self.populateAccessibilityInfo(stopInfo: stopInfo)
            }
        }
    }
   

}
