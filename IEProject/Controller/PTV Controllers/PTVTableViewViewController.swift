//
//  PTVTableViewViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 28/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import SwiftPTV
import JGProgressHUD
class PTVTableViewViewController: UIViewController {
    
    let cellid = "cellId"
    var searchTerm: String?
    
    var filteredRoutes: [Route] = []
    var tramRoutes: [Route] = []
    var isSearching: Bool = false
    
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.delegate = self
        tv.dataSource = self
        tv.backgroundColor = UIColor.white
        return tv
    }()
    
    var searchBar: UISearchBar = {
        let width = Int(UIScreen.main.bounds.width)
        let frame = CGRect(x: 0, y: 0, width: 200, height: 20)
        let searchBar = UISearchBar(frame: frame)
        searchBar.placeholder = "Search using route"
        return searchBar
        
    }()
    
    var hud: JGProgressHUD?
    var delegate: PassDataToPTVControllerDelegate?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        hud = JGProgressHUD(style: .extraLight)
        view.backgroundColor = UIColor.white
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellid)
        
        view.addSubview(tableView)
        tableView.centerAnchor(centerX: view.centerXAnchor, centerY: view.centerYAnchor, size: .init(width: view.frame.width, height: view.frame.height), padding: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        loadData()
        setupSearchBar()
        
        let backButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleBackButton))
        navigationItem.leftBarButtonItem = backButton

    }
    
    @objc
    private func handleBackButton(){
        dismiss(animated: true, completion: nil)
    }
    
    private func setupSearchBar(){
        searchBar.delegate = self
        self.navigationItem.titleView = searchBar
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @objc
    private func handleOnScreenTap(){
        dismiss(animated: true, completion: nil)
    }


}

extension PTVTableViewViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return filteredRoutes.count
        }
        return tramRoutes.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath)
        if isSearching && searchTerm != nil{
            let route = filteredRoutes[indexPath.item]
            cell.textLabel?.text = route.name
            return cell
        }
        let route = tramRoutes[indexPath.item]
        cell.textLabel?.text = route.name
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        hud?.show(in: self.view)
        if isSearching && searchTerm != nil{
            let route = filteredRoutes[indexPath.item]
        
            DispatchQueue.main.async {
                self.delegate?.passRoute(route: route)
                self.dismiss(animated: true, completion: nil)
                self.hud?.dismiss()
            }
       
        }
        else{
            let route = tramRoutes[indexPath.item]
            PTVProvider.ptvAPI.retrieveRouteDetails(routeID: route.ID!, parameters: nil) { (routeDetail) in
                guard let route = routeDetail?.route else{print("ROUTE DETAIL UNAVLB"); return}
                
                DispatchQueue.main.async {
                    self.delegate?.passRoute(route: route)
                    self.dismiss(animated: true, completion: nil)
                    self.hud?.dismiss()
                    
                }
                
            }

        }
    }
    
    private func loadData(){
        
        
        PTVProvider.ptvAPI.retrieveRoutes(parameters: nil) { (response) in
            guard let allRoutes = response?.routes else{print("NO PTV ROUTES");return}
            DispatchQueue.main.async {
                for route in allRoutes {
                    let routeType = route.type
                    //0: Train
                    //1: Tram
                    //2:
                    //3:
                    //4: Night Bus
                    if routeType == 1 {
                        self.tramRoutes.append(route)
                    }
                }
                self.tableView.reloadData()
            }
        }
    }
    
    
    
}


extension PTVTableViewViewController: UISearchBarDelegate{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearching = true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearching = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchTerm = searchText
        filteredRoutes.removeAll(keepingCapacity: false)
        let predicateString = searchBar.text!
        filteredRoutes = tramRoutes.filter({$0.name?.range(of: predicateString) != nil})
        //filteredRoutes.sort{$0.qv_market_peel_st! < $1.qv_market_peel_st!}
        isSearching = (filteredRoutes.count == 0) ? false: true
        tableView.reloadData()
    }
}
//MARK: Protocol for data transfer between
protocol PassDataToPTVControllerDelegate {
    func passRoute(route: Route)
}








