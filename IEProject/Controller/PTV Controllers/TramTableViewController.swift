//
//  TramTableViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 4/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import SwiftPTV
class TramTableViewController: UIViewController {

    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "All tram stations that are closest to you"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Click on the preferred station to view more information"
        label.numberOfLines = 0
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = UIColor(red: 142, green: 142, blue: 147)
        return label
    }()
    
    
    
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.delegate = self
        tv.dataSource = self
        return tv
    }()
    let cellId = "trainsCellId"
    
    var allTrainStops:[StopGeosearch] = []
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Train stops"
        view.backgroundColor = UIColor.white
        tableView.register(PTVStopsTableViewCell.self, forCellReuseIdentifier: cellId)
        setup()
        tableView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    private func setup(){
        
        view.addSubview(titleLabel)
        titleLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, trailing: view.safeAreaLayoutGuide.trailingAnchor, padding: .init(top: 20, left: 20, bottom: 0, right: 10))
        view.addSubview(descriptionLabel)
        descriptionLabel.anchor(top: titleLabel.bottomAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, trailing: view.safeAreaLayoutGuide.trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 0))
        
        view.addSubview(tableView)
        tableView.anchor(top: descriptionLabel.bottomAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 10, right: 10))
    }
    


}

extension TramTableViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allTrainStops.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! PTVStopsTableViewCell
        let stop = allTrainStops[indexPath.item]
        let stopName = stop.name
        
        let distance = stop.distance
        cell.stopName.text = stopName
        cell.stopName.textColor = UIColor(hex: "02b21f")
        cell.distanceFromCurrentLoationLabel.text = "\((distance!/1000).rounded(toPlaces: 2)) km from current location"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
 
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let modalVC = TramMoreInfoModal()
        modalVC.modalTransitionStyle = .crossDissolve
        modalVC.modalPresentationStyle = .overCurrentContext
        present(modalVC, animated: true, completion: nil)

    }
    
    
}











