//
//  NewHomeViewController.swift
//  IEProject
//
//  Created by Rhea Tuteja on 26/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import TRMosaicLayout

class NewHomeViewController: UIViewController {
    
    let cellId = "cellId"
    
    
    lazy var collectionView: UICollectionView = {
        let mosaicLayout = TRMosaicLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: mosaicLayout)
        mosaicLayout.delegate = self
        cv.dataSource = self
        cv.delegate = self
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(collectionView)
        collectionView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
 
        // Do any additional setup after loading the view.
    }
    
}

extension NewHomeViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        cell.backgroundColor = UIColor.brown
        return cell
    }
    
    
}

extension NewHomeViewController: TRMosaicLayoutDelegate{
    func collectionView(_ collectionView: UICollectionView, mosaicCellSizeTypeAtIndexPath indexPath: IndexPath) -> TRMosaicCellType {
        return indexPath.item % 3 == 0 ? TRMosaicCellType.big : TRMosaicCellType.small
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: TRMosaicLayout, insetAtSection: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3)
    }
    
    func heightForSmallMosaicCell() -> CGFloat {
        return 150
    }
    
    
}
