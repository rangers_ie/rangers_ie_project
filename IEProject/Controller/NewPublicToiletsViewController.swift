//
//  NewPublicToiletsViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 8/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import CoreLocation
class NewPublicToiletsViewController: UIViewController {
    
    var allToiletsData:[Toilets] = []
    
    let descLabel: UILabel = {
        let label = UILabel()
        label.text = "All public toilets around current location "
        label.numberOfLines = 0
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        return label
    }()
    
    let seperatorLineTwo: UIView = {
        let line = UIView()
        line.backgroundColor = UIColor(hex: "dedede")
        return line
    }()
    

    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .white
        return cv
    }()

    let cellId = "toiletCellId"
    override func viewDidLoad() {
        super.viewDidLoad()
        currentLocationNotification()
        view.backgroundColor = .white
        view.addSubview(seperatorLineTwo)
        view.addSubview(descLabel)
        view.addSubview(collectionView)
        title = "Public Toilets"
        collectionView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
  
        
        seperatorLineTwo.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, trailing: view.safeAreaLayoutGuide.trailingAnchor,padding: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), size: CGSize(width: 0, height: 100))
        
        descLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, trailing: view.safeAreaLayoutGuide.trailingAnchor, padding: UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 20))
        
        collectionView.anchor(top: seperatorLineTwo.bottomAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor, padding: UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0))
        collectionView.register(PublicToiletsCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.reloadData()
        

    }
    


}

extension NewPublicToiletsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allToiletsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PublicToiletsCollectionViewCell
        let toilet = allToiletsData[indexPath.item].toilet
        var distance = allToiletsData[indexPath.item].distance
        distance = distance/1000
        distance = distance.rounded(toPlaces: 2)
        
        
        var name = toilet.name
        name = name?.replacingOccurrences(of: "Public Toilet - ", with: "")
        name = name?.replacingOccurrences(of: "Toilet ", with: "")
        cell.namelabel.text = name
        cell.distanceLabel.text = "\(distance) km from you"
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let toilet = allToiletsData[indexPath.item].toilet
        
        let coordinates = CLLocation(latitude: toilet.lat!, longitude: toilet.lon!)
        
        GoogleClient.sharedInstance.openGoogleMaps(route: coordinates)
    }
    
    private func currentLocationNotification(){
        let currentLocation = AppDelegate.appDelegateShared().currentLocation
        let centerCBD = CLLocation(latitude: -37.815299, longitude: 144.962656)
        let distance  = centerCBD.distance(from: currentLocation)
        if distance > 10000{
            let title = "Public Toilets Data"
            let message = "Your current location puts you away from Melbourne CBD. This app is customized to work around Melbourne CBD. Public Toilets data presented on WheelyGo may present unrealistic distances to public toilets around you. Feel free to explore other options presented. Enjoy."
            NotificationProvider.sharedInstance.alertUser(title: title, message: message, viewController: self)
            
        }
        
    }
    
    
}
