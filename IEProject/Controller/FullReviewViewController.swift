//
//  FullReviewViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 18/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class FullReviewViewController: UIViewController {
    
    var fullReview: Reviews?
    
    let fullReviewVIew: FullReviewView = {
        let rv = FullReviewView()
        rv.layer.cornerRadius = 10
        return rv
    }()
   
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        view.addSubview(fullReviewVIew)
        fullReviewVIew.centerAnchor(centerX: view.centerXAnchor, centerY: view.centerYAnchor, size: .init(width: view.frame.width - 20, height: 500), padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        
        fullReviewVIew.reviewText.text = fullReview?.text
        fullReviewVIew.cancelButton.addTarget(self, action: #selector(handleOnScreenTap), for: .touchUpInside)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleOnScreenTap))
        
        view.addGestureRecognizer(tap)

        // Do any additional setup after loading the view.
    }
    
    @objc
    private func handleOnScreenTap(){
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}
