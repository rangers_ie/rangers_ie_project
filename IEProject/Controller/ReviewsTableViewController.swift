//
//  ReviewsTableViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 17/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class ReviewsTableViewController: UITableViewController {
    
    let cellId = "cellId"
    var googlePlace: GooglePlaceFromId?

    override func viewDidLoad() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        title = "Google Reviews"
        
        let backButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleBack))
        
        self.navigationItem.setLeftBarButton(backButton, animated: true)
        
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ReviewTableViewCell.self, forCellReuseIdentifier: cellId)
//        tableView.estimatedRowHeight = 44.0
//        tableView.rowHeight = UITableViewAutomaticDimension
        

    }
    
    @objc
    private func handleBack(){
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let reviewCount = googlePlace?.result?.reviews?.count
        return reviewCount!
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ReviewTableViewCell
        let review = googlePlace?.result?.reviews![indexPath.item]
        cell.reviewText.text = review?.text
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let review = googlePlace?.result?.reviews![indexPath.item]
        let fullReviewVC = FullReviewViewController()
        fullReviewVC.modalPresentationStyle = .overCurrentContext
        fullReviewVC.modalTransitionStyle = .crossDissolve
        fullReviewVC.fullReview = review
        present(fullReviewVC, animated: true, completion: nil)
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }



}


