//
//  TabBarViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 7/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import SwipeableTabBarController
import CoreLocation

class TabBarViewController: SwipeableTabBarController {
    
    
    var placeID: String?
    
    var locationType: String?
    var allBuildings: [Building]!
    
    
    
    
    var mapViewContrller: MapViewController!
    var detailViewController: PlaceDetailsViewController!
    var accessibilityVC: AccssibilityInfoViewController!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getPlaceDetails()
    }
    
 
    
    private func setUpTabBar(googlePlace: GooglePlaceFromId){
        //Pre selected Tab
        selectedIndex = 0
        //@ Index Zero
        setUpDetailViewController(googlePlace: googlePlace)
        //@ Index One
        setUpMapViewController(googlePlace: googlePlace)
        
        //@Index Two
        //setupAccessibilityVC(googlePlace: googlePlace)
        //Adding VCs to the tab bar
        viewControllers = [detailViewController, mapViewContrller]
        //Set swiper actions
        setSwipeAnimation(type: SwipeAnimationType.sideBySide)
        isSwipeEnabled = false
    }
    
    private func setUpDetailViewController(googlePlace: GooglePlaceFromId){
        detailViewController = PlaceDetailsViewController()
        detailViewController.place = googlePlace
        detailViewController.title = "Information"
        let detailViewIcon = UIImage(named: "information")?.withRenderingMode(.alwaysOriginal)
        let detailViewSelectedIcon = UIImage(named: "information")
        let detailViewIconItem = UITabBarItem(title: "About", image: detailViewIcon, selectedImage: detailViewSelectedIcon)
        detailViewController.tabBarItem = detailViewIconItem
    }
    
    private func setUpMapViewController(googlePlace: GooglePlaceFromId){
        
        mapViewContrller = MapViewController()
        mapViewContrller.place = googlePlace
        mapViewContrller.title = "Walking Path"
        let mapViewIcon = UIImage(named: "map")?.withRenderingMode(.alwaysOriginal)
        let mapViewIconSelected = UIImage(named: "map")
        let mapViewIconItem = UITabBarItem(title: "Map", image: mapViewIcon, selectedImage: mapViewIconSelected)
        mapViewContrller.tabBarItem = mapViewIconItem
    }
    
    private func setupAccessibilityVC(googlePlace: GooglePlaceFromId){
        accessibilityVC = AccssibilityInfoViewController()
        accessibilityVC.place = googlePlace
        accessibilityVC.title = "Accessibility"
        let wheelChairIcon = UIImage(named: "wheelchair")?.withRenderingMode(.alwaysOriginal)
        let wheelChiarIconSelected = UIImage(named: "wheelchair")
        let accebViewIconItem = UITabBarItem(title: "Accessibility", image: wheelChairIcon, selectedImage: wheelChiarIconSelected)
        accessibilityVC.tabBarItem = accebViewIconItem
        
    }
    
    private func getPlaceDetails(){
        GoogleClient.sharedInstance.getPlaceFromId(palceId: placeID!) { (place) in
            self.setUpTabBar(googlePlace: place)
            
        }
    }
    
  
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {

    }
    
   

}






