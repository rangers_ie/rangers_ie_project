//
//  StepsReviewViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 18/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class StepsReviewViewController: UIViewController {
    
    var polyLine: String?
    
    let reviewTextView: UITextView = {
        let tv = UITextView()
        tv.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
        tv.layer.cornerRadius = 10
        return tv
    }()
    
    let submitButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Submit Review", for: .normal)
        return button
    }()
    
    let cancelButton: UIButton = {
        let cancelButton = UIButton(type: .system)
        cancelButton.setTitle("Cancel", for: .normal)
        return cancelButton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        view.addSubview(reviewTextView)
        view.addSubview(submitButton)
        view.addSubview(cancelButton)
        
        reviewTextView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 20, left: 10, bottom: 0, right: 10), size: .init(width: view.frame.width, height: 300))
        submitButton.anchor(top: reviewTextView.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 0))
        cancelButton.anchor(top: submitButton.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0))
        
        submitButton.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    @objc
    private func handleCancel(){
        dismiss(animated: true, completion: nil)
    }
    
    @objc
    private func handleSubmit(){
        let review = reviewTextView.text
        FirebaseDBProvider.saveRouteReview(review: review!)
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
