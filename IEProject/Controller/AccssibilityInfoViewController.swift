//
//  AccssibilityInfoViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 13/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import CoreLocation

class AccssibilityInfoViewController: UIViewController {

    var allBuildings:[Building]!
    var buidlingInfo: AccessibilityInformation!

    private let cellId = "commentTableId"
    var place: GooglePlaceFromId?
    private let initialViewHeight: CGFloat = 400

    let accessbilityView: AccessibilityView = {
        let accessbilityView = AccessibilityView()
        accessbilityView.backgroundColor = UIColor.white
        return accessbilityView
    }()

    lazy var commentsTableView: UITableView = {
        let tv = UITableView()
        tv.dataSource = self
        tv.delegate = self
        return tv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        print("Buildings")
        view.addSubview(accessbilityView)
        view.addSubview(commentsTableView)
        accessbilityView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 400))

        commentsTableView.anchor(top: accessbilityView.bottomAnchor, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 0))
        commentsTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)

        view.backgroundColor = UIColor.lightGray


    }

    override func viewWillAppear(_ animated: Bool) {
        buidlingInfo = loadBuildingInformation(place: place!)
        setup(info: buidlingInfo)
    }

    private func setup(info: AccessibilityInformation){


        accessbilityView.accessibility_type_description_label.text = info.typeDescription
        accessbilityView.accessibility_rating_label.text = info.rating
        accessbilityView.accessibility_type_label.text = info.type
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension AccssibilityInfoViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.textLabel?.text = "Text "
        return cell
    }


}

extension AccssibilityInfoViewController{
    private func isSameLocation(locationA: CLLocation, locationB: CLLocation)->Bool{

        let numDP = 3
//                print("Location A - Latitude \(locationA.coordinate.latitude.rounded(toPlaces:numDP )) Longitude \(locationA.coordinate.longitude.rounded(toPlaces: numDP))")
//                print("Location B - Latitude \(locationB.coordinate.latitude.rounded(toPlaces: numDP)) Longitude \(locationB.coordinate.longitude.rounded(toPlaces: numDP))")

        if locationA.coordinate.latitude.rounded(toPlaces: numDP).isEqual(to: locationB.coordinate.latitude.rounded(toPlaces: numDP))  &&
            locationA.coordinate.longitude.rounded(toPlaces: numDP).isEqual(to: locationB.coordinate.longitude.rounded(toPlaces: numDP)) {
            return true
        }
        return false
    }


    private func loadBuildingInformation(place: GooglePlaceFromId)->AccessibilityInformation{
        var accessibilityInformation = AccessibilityInformation(desc: "Non", rating: "Non", typ: "Non")

        APIClient.sharedInstance.getBuildingInformation { (response) in
            self.allBuildings = response
            let currentPlace = CLLocation(latitude: (place.result?.geometry?.location?.lat)!, longitude: (place.result?.geometry?.location?.lng)!)

            print("Current Place Lats: \(currentPlace.coordinate.latitude) Longs: \(currentPlace.coordinate.longitude)")

            var buildingLats: Double!
            var buidlingLong: Double!

            for i in self.allBuildings{
                if let latitude = i.y_coordinate, let doubleLat = Double(latitude){
                    buildingLats = doubleLat}
                if let longititude = i.x_coordinate, let doubleLngs = Double(longititude){
                    buidlingLong = doubleLngs
                }
                if buildingLats != nil && buidlingLong != nil{
                    let thisPlace = CLLocation(latitude: buildingLats, longitude: buidlingLong)
                    print("Place from DS: lats \(thisPlace.coordinate.latitude) longs\(thisPlace.coordinate.longitude)")
                    if self.isSameLocation(locationA: thisPlace, locationB: currentPlace){

                        let description = i.accessibility_type_description!
                        let rating = i.accessibility_rating!
                        let type = i.accessibility_type!
                        accessibilityInformation = AccessibilityInformation(desc: description, rating: rating, typ: type)


                    }
                }
            }//End of For Loop
        }//End of API Client
        return accessibilityInformation

    }//End of loadBuildingInfo Method
}

struct AccessibilityInformation {
    var typeDescription: String
    var rating: String
    var type: String

    init(desc: String, rating: String, typ: String) {
        self.typeDescription = desc
        self.rating = rating
        self.type = typ
    }
}
