//
//  StepsViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 9/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import CoreLocation

class StepsViewController: UIViewController {
    
    let cellId = "stepsCellId"
    
    var steps: [Steps]!
    var route: Legs!
    var warningsPresent: Bool!
    var warning: [String]!
    var routeSummary: String!
    var polyLine: String!
    var modifiedDuration: String?
    
    lazy var stepTableViewController: UITableView = {
        let tv = UITableView()
        tv.dataSource = self
        tv.delegate = self
        return tv
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Route Information"
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        view.addSubview(stepTableViewController)

        stepTableViewController.anchor(top: view.topAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor, padding: .zero)

        stepTableViewController.register(StepTabelViewCell.self, forCellReuseIdentifier: cellId)

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

extension StepsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return steps.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! StepTabelViewCell
        
        let pathInfo = steps[indexPath.item].html_instructions?.html2String
        cell.routeInfoLabel.text = pathInfo

        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = HeaderViewTabelView()
        getElevationData(headerView: view)
        view.distanceValue.text = route.distance?.text
        //view.durationValue.text = route.duration?.text
        view.durationValue.text = modifiedDuration
        view.routeSummary.text = routeSummary
        view.timeCaution.addTarget(self, action: #selector(handleTimeCaution), for: .touchUpInside)
        view.cautionImage.addTarget(self, action: #selector(handleRouteCaution), for: .touchUpInside)

        
        if warningsPresent{
            view.cautionImage.isHidden = false
        }
        else{
            view.cautionImage.isHidden = true
        }
        print(warning)
        return view
    }
    
    @objc
    private func handleRouteCaution(){
        let title = "Route Caution"
        if let message = warning.first {
            NotificationProvider.sharedInstance.alertUser(title: title, message: message, viewController: self)
            
        }
        
    }
    
    @objc
    private func handleTimeCaution(){
        let notificationTitle = "Time for travel"
        let message = "The time displayed on WheelyGo for travel may not directly reflect the time displayed on Google Maps. WheelyGo presents the time by carefully considering the speed and performance of a manual wheel chair to serve you better. Enjoy your journey"
        NotificationProvider.sharedInstance.alertUser(title: notificationTitle, message: message, viewController: self)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = FooterCellStepTableView()
        view.startNavButton.addTarget(self, action: #selector(handleTakeMeButton), for: .touchUpInside)
        view.cancelButton.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        view.review.addTarget(self, action: #selector(handleReview), for: .touchUpInside)
        return view
    }
    
    @objc
    private func handleReview(){
        let stepReviewVC = StepsReviewViewController()
        stepReviewVC.polyLine = "polyLn"
        present(stepReviewVC, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }
    
    @objc
    private func handleCancel(){
        dismiss(animated: true, completion: nil)
    }
    
    func getElevationData(headerView: HeaderViewTabelView){
        
        let startLocationLats = (route.start_location?.lat)!
        let startLovationLongs = (route.start_location?.lng)!
        let startLocationCoordinates = CLLocation(latitude: startLocationLats, longitude: startLovationLongs)
        let endLocationLates = (route.end_location?.lat)!
        let endLocationsLongs = (route.end_location?.lng)!
        let endLocaionCoordinates = CLLocation(latitude: endLocationLates, longitude: endLocationsLongs)
        
        GoogleClient.sharedInstance.getElevationData(startLocaton: startLocationCoordinates, endLocation: endLocaionCoordinates) { (response) in
            var elevatonResults:[Double]=[]
            for result in response.results!{
                elevatonResults.append((result.elevation?.rounded(toPlaces: 2))!)
            }
            let changeInElevation = elevatonResults.first! - elevatonResults.last!
            let elevString = "Change in elevation \(changeInElevation.rounded(toPlaces: 2))"
            if changeInElevation > 0 {
                headerView.elevationLabel.textColor = UIColor.green
            }
            else{
                headerView.elevationLabel.textColor = UIColor.red
            }
            headerView.elevationLabel.text = elevString
            
        }
        
    }
    


    @objc
    private func handleTakeMeButton(){
        let destinationLatitude = (route.end_location?.lat)!
        let destinationLongitude = (route.end_location?.lng)!
        let location = CLLocation(latitude: destinationLatitude, longitude: destinationLongitude)
        GoogleClient.sharedInstance.openGoogleMaps(route: location)
 
    }
}


