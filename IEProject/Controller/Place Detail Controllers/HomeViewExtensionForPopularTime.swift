//
//  HomeViewExtensionForPopularTime.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 5/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import Foundation
import CoreLocation

extension NewPlaceDetailsVC {
    
    
     func getPopularHoursTitle(location: CLLocation){
        
     
        let location = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        
        APIClient.sharedInstance.getFourSquareData(currentLocation: location) { (fourSquare) in
            guard let responce = fourSquare.response?.venues?.last else {return}
            
            let venueId = responce.id!
            APIClient.sharedInstance.getMostPopularTime(venueId: venueId, completition: { (venueResponce) in
                guard let responce = venueResponce.response else {return}
                if let timeFrames = responce.popular?.timeframes{
                    
                    if timeFrames.count > 0 {
                        for time in timeFrames {
                            
                            guard let day = time.days else {return}
                            guard let popTime = time.open else {return}
                            
                            switch day {
                            case[7]:
                                let day = "Sunday"
                                self.generatePopularTimeString(popularTime: popTime, day: day)
                            case[1]:
                                let day = "Monday"
                                self.generatePopularTimeString(popularTime: popTime, day: day)
                            case[2]:
                                let day = "Tuesday"
                                self.generatePopularTimeString(popularTime: popTime, day: day)
                            case[3]:
                                let day = "Wednesday"
                                self.generatePopularTimeString(popularTime: popTime, day: day)
                            case[4]:
                                let day = "Thursday"
                                self.generatePopularTimeString(popularTime: popTime, day: day)
                            case[5]:
                                let day = "Friday"
                                self.generatePopularTimeString(popularTime: popTime, day: day)
                            case[6]:
                                let day = "Saturday"
                                self.generatePopularTimeString(popularTime: popTime, day: day)
                            default:
                                print("default")
                            }
                            
                        }//End of For statment
                    }//Send of if timeFrames.count > 0
                        
                    else{
                        self.fullTextForPopularHours = "Looks like we are still figuring \n this place out!"
                    }
                    //print(self.fullTextForPopularHours)
                }
                else{
                    self.fullTextForPopularHours = "Looks like we are still figuring \n this place out!"
                }
                
            })
        }
    }
    
    private func generatePopularTimeString(popularTime: [OpenForPopular], day: String){
        
        fullTextForPopularHours += "\(day) between"
        if popularTime.count > 1{
            
            for time in popularTime{
                
                let startTime = convertFromMilitaryTime(timeString: time.start!)
                let endTime = convertFromMilitaryTime(timeString: time.end!)
                fullTextForPopularHours += " \(startTime) to \(endTime) \n\n"
            }
        }
        else{
            
            
            if let time = popularTime.last {
                let startTime = convertFromMilitaryTime(timeString: time.start!)
                let endTime = convertFromMilitaryTime(timeString: time.end!)
                fullTextForPopularHours +=  " \(startTime) to \(endTime) \n\n"
            }
            //fullTextForPopularHours += "Time Unavailable"
            
            
            
        }
    }
    
    private func setBusyAlert(){
        
        let currentDateTime = Date()
        print(currentDateTime)
        
    }
    
    private func convertFromMilitaryTime(timeString: String)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HHmm"
        
        if let date = dateFormatter.date(from: timeString){
            dateFormatter.dateFormat = "h:mm a"
            let Date12 = dateFormatter.string(from: date)
            return Date12
        }
        
        return "ND"
    }
    
    
    
}


extension NewPlaceDetailsVC{
     func getUserRatingFromFirebase(){
        var questionOneYesCount = 0
        var questionTwoYesCount = 0
        var questionThreeYesCount = 0
        
        var questionOneNoCount = 0
        var questionTwoNoCount = 0
        var questionThreeNoCount = 0
        
        var nuberOfReviews: Int = 0
        let userId = googlePlaceFromId?.place_id
        FirebaseDBProvider.getUserRating(placeID: userId!) { (results, message) in
            
            if message == FirebaseConstants.FAIL{
                self.hideLabelsForReviews()
            }
            else{
                self.showLabelsForReviews()
                
                nuberOfReviews = results.count
                for result in results {
                    switch result.Question01{
                    case "Yes":
                        questionOneYesCount += 1
                    case "No":
                        questionOneNoCount += 1
                    default:
                        print("default")
                    }
                    switch result.Question02{
                    case "Yes":
                        questionTwoYesCount += 1
                    case "No":
                        questionTwoNoCount += 1
                    default:
                        print("default")
                    }
                    switch result.Question03{
                    case "Yes":
                        questionThreeYesCount += 1
                    case "No":
                        questionThreeNoCount += 1
                    default:
                        print("default")
                        
                    }
                }
                let totalForQuestionOne = Double(questionOneYesCount) + Double(questionOneNoCount)
                let totalForQuestionTwo = Double(questionTwoYesCount) + Double(questionTwoNoCount)
                let totalForQuestionThree = Double(questionThreeYesCount) + Double(questionThreeNoCount)
                
                
                let yesPercentageForQuestionOne = (Double(questionOneYesCount)/totalForQuestionOne).rounded(toPlaces: 2)*100
                
                let yesPercentageForQuestionTwo = (Double(questionTwoYesCount)/totalForQuestionTwo).rounded(toPlaces: 2)*100
                
                let yesPercentageForQuestionThree = (Double(questionThreeYesCount)/totalForQuestionThree).rounded(toPlaces: 2)*100
                
//
                self.detailView.rampAccessLabel.text = "\(yesPercentageForQuestionOne)% of guests said this place has ramp access"
                self.detailView.adequareSpaceLabel.text = "\(yesPercentageForQuestionTwo)% of guests said this place has enough room"
                self.detailView.friendlyStaffLabel.text = "\(yesPercentageForQuestionThree)% of guests said the staff is friendly"
                
                self.detailView.accessibilityInfoTitile.text = "\(nuberOfReviews) people reviewed this place"
//
            }
            
            
            
        }
    }
    
    
    
    private func hideLabelsForReviews(){
        detailView.rampAccessLabel.isHidden = true
        detailView.adequareSpaceLabel.isHidden = true
        detailView.friendlyStaffLabel.isHidden = true
        detailView.noReviewsLabel.isHidden = false
    }

    private func showLabelsForReviews(){
        detailView.noReviewsLabel.isHidden = true
        detailView.rampAccessLabel.isHidden = false
        detailView.adequareSpaceLabel.isHidden = false
        detailView.friendlyStaffLabel.isHidden = false
    }
}
