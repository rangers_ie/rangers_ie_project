//
//  NewPlaceDetailsVC.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 4/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import CoreLocation
import UberRides
import SwiftPTV

class NewPlaceDetailsVC: UIViewController {
    
    var placeId: Int!
    var googlePlaceId: String?
    var placeLocation: CLLocation?
    var googlePlaceFromId: Result?
    var googlePlaceForMapVC: GooglePlaceFromId?
    
    private var allPublicToiletsAroundLocation:[Toilets]=[]
    
    
    
    var fullTextForPopularHours = ""
    var selectedRestaurant: ZomRestFromId?
    
    let detailView: DetailView = {
        let view = DetailView()
        return view
    }()
    
    
    //Struct toilets
    var allPublicToilets:[Toilets] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        setup()
        populateView()
        setupUber()
        loadToiletsNearLocation()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
        getPopularHoursTitle(location: placeLocation!)
        loadZomatoProfile()
        getUserRatingFromFirebase()
        
    
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    

    private func setup(){
        view.addSubview(detailView)
        detailView.anchor(top: view.topAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor)
        
        detailView.tramButtons.addTarget(self, action: #selector(handleTramButton), for: .touchUpInside)
        detailView.trainsButton.addTarget(self, action: #selector(handleTrainButton), for: .touchUpInside)
        detailView.getDirectionsButton.addTarget(self, action: #selector(handleGetDirectionsButton), for: .touchUpInside)
        detailView.backButton.addTarget(self, action: #selector(handleBackButton), for: .touchUpInside)
        detailView.reviewInfoButton.addTarget(self, action: #selector(handleReviewInfoButton), for: .touchUpInside)
        
        guard let name = googlePlaceFromId?.name else {return}
        let shortName = getShortName(name: name )
        detailView.accessibilityInfoTitile.text = "More information about \(shortName)"
        
        let buttonPanel = detailView.buttonPanel
        buttonPanel.viewReviewButton.addTarget(self, action: #selector(handleViewReview), for: .touchUpInside)
        buttonPanel.leaveReviewButton.addTarget(self, action: #selector(handleLeaveReview), for: .touchUpInside)
        buttonPanel.busyHourButton.addTarget(self, action: #selector(handleCheckBusyTime), for: .touchUpInside)
        buttonPanel.callButton.addTarget(self, action: #selector(handlePhoneNumberButton), for: .touchUpInside)
        buttonPanel.toiletButton.addTarget(self, action: #selector(handlePublicToiletsButton), for: .touchUpInside)
        buttonPanel.weather.addTarget(self, action: #selector(handleWeatherVC), for: .touchUpInside)
    }
    
    

    
    private func setupUber(){
        guard let placeCoordinates = placeLocation else {return}
        guard let placeName = googlePlaceFromId?.name else {return}
        guard let address = googlePlaceFromId?.adr_address else {return}
        self.setUpUberButton(location: placeCoordinates, locationName: placeName, address: address)
    }
    
    @objc
    private func handleWeatherVC(){
        let weatherVC = WeatherViewController()
        weatherVC.googlePlace = googlePlaceForMapVC
        weatherVC.modalPresentationStyle = .overCurrentContext
        weatherVC.modalTransitionStyle = .crossDissolve
        present(weatherVC, animated: true, completion: nil)
    }
    
    @objc
    private func handleReviewInfoButton(){
        let title = "User Feedback"
        let message = "We compute these scores based on the feedback provided by other WheelyGo users"
        NotificationProvider.sharedInstance.alertUser(title: title, message: message, viewController: self)
    }
    
    @objc
    private func handlePublicToiletsButton(){
        let name = googlePlaceFromId?.name
        let toilets = NewPublicToiletsViewController()
        toilets.allToiletsData = allPublicToiletsAroundLocation
        toilets.descLabel.text = "Public toilets around \(name!)"
        navigationController?.pushViewController(toilets, animated: true)
    }
    
    
    private func loadToiletsNearLocation(){
        let allPublicToilets = AppDelegate.appDelegateShared().allPublicToilets
        for toilet in allPublicToilets{
            let toiletLocation = CLLocation(latitude: toilet.lat!, longitude: toilet.lon!)
            let distance  = toiletLocation.distance(from: placeLocation!)
            let toiletWithDistance = Toilets(toilet: toilet, distance: distance)
            allPublicToiletsAroundLocation.append(toiletWithDistance)
        }
        allPublicToiletsAroundLocation.sort{$0.distance < $1.distance}
        
    }
    
    @objc
    private func handleBackButton(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc
    private func handlePhoneNumberButton(){
        let phoneNumber = (googlePlaceForMapVC?.result?.international_phone_number)!.removingWhitespaces()
        guard let number = URL(string: "tel://\(phoneNumber)") else {print("Invalid Number"); return }
        UIApplication.shared.open(number)
        
    }
    
    @objc
    private func handleGetDirectionsButton(){
        let mapVC = MapViewController()
        mapVC.place = googlePlaceForMapVC
        navigationController?.pushViewController(mapVC, animated: true)
    }
    
    
    @objc
    private func handleViewReview(){
        let reviewVC = ReviewsTableViewController()
        reviewVC.googlePlace = googlePlaceForMapVC
        let navEditorViewController: UINavigationController = UINavigationController(rootViewController: reviewVC)
        present(navEditorViewController, animated: true, completion: nil)
    }
    
    @objc
    private func handleLeaveReview(){
        let feedbackVC = FeedbackViewController()
        feedbackVC.googlePlace = googlePlaceForMapVC
        present(feedbackVC, animated: true, completion: nil)
    }
    
    @objc
    private func handleTramButton(){
        
        guard let placeLocation = placeLocation else {return}
        guard let placeName = googlePlaceFromId?.name else {return}
        let ptvLocation = PTVLocation(lats: placeLocation.coordinate.latitude, longs: placeLocation.coordinate.longitude)
        let params = ["route_types":"1","max_distance":"10000"]
        PTVProvider.ptvAPI.retrieveStopsNearLocation(location: ptvLocation, parameters: params) { (stopsNearLocation) in
            
            DispatchQueue.main.async {
                guard let allStops = stopsNearLocation?.stops else {print("Stops Not Returned");return}
                let tramTV = TramTableViewController()
                tramTV.titleLabel.text = "Tram stops near \(placeName)"
                tramTV.allTrainStops = allStops
                self.navigationController?.pushViewController(tramTV, animated: true)
                
            }
  
        }
    }
    
    @objc
    private func handleTrainButton(){
 
        guard let placeLocation = placeLocation else {return}
        guard let placeName = googlePlaceFromId?.name else {return}
        let ptvLocation = PTVLocation(lats: placeLocation.coordinate.latitude, longs: placeLocation.coordinate.longitude)
        let params = ["route_types":"0","max_distance":"10000"]
        PTVProvider.ptvAPI.retrieveStopsNearLocation(location: ptvLocation, parameters: params) { (stopsNearLocation) in
            
            DispatchQueue.main.async {
                guard let allStops = stopsNearLocation?.stops else {return}
                let metroTV = TrainsTableViewController()
                metroTV.allTrainStops = allStops
                metroTV.titleLabel.text = "Train Stops near \(placeName)"
                self.navigationController?.pushViewController(metroTV, animated: true)
            }

            
        }
    }
    
  
    
    
    
    private func populateView(){

        guard let placeName = googlePlaceFromId?.name else {return}
        guard let address = googlePlaceFromId?.formatted_address else {return}
        guard let imageReference = googlePlaceFromId?.photos?.first?.photo_reference else {return}
        
        detailView.placeName.text = placeName
        detailView.address.text = address

        
        
        let imageURL = GoogleClient.sharedInstance.getImageURL(pictureReference: imageReference)
        detailView.imageView.setImageWith(imageURL)
        
  
        
        let shortName = getShortName(name: placeName)
        
        
        detailView.transportOptionDesc.text = "Tram and Metro stations closest to \(shortName)"
        
        
//        detailView.ratingLabel.backgroundColor = UIColor(hex: ratingColour)
    
//        let accessibilityLabel = detailView.accessibleRamp
//

    }
    
    private func getShortName(name: String)->String{
        var shortName = ""
        let placeNameArray = name.components(separatedBy: " ")
        if placeNameArray.count > 4{
            shortName = "\(placeNameArray[0]) \(placeNameArray[1]) \(placeNameArray[2]) \(placeNameArray[3])"
            return shortName
        }
        return name
    }
    
    private func isAccessible(startUrl: URL)->Bool{
        let crawler = Crawler()
        let pagesToVisit: Set<URL> = [startUrl]
        crawler.pagesToVisit = pagesToVisit
        crawler.crawl()
        crawler.semaphore.wait()
        return crawler.isAccessible
    }
    
    
    
    
    
    
    private func setAccessibility(label: UILabel, labelText: String, imageName: String){
        let image = UIImage(named: imageName)
        
        //Create Attachment
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = image
        //Set bound to reposition
        let imageOffsetY:CGFloat = -3.0;
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        //Add image to mutable string
        completeText.append(attachmentString)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: " \(labelText)")
        completeText.append(textAfterIcon)
        label.textAlignment = .center;
        label.attributedText = completeText;
    }
    
    func setUpUberButton(location: CLLocation, locationName: String, address: String){
        let uberButton = RideRequestButton()
        let builder = RideParametersBuilder()
        builder.dropoffLocation = location
        builder.dropoffNickname = locationName
        builder.dropoffAddress = address
        uberButton.rideParameters = builder.build()
        uberButton.colorStyle = .white
        view.addSubview(uberButton)
        uberButton.anchor(top: detailView.address.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing:view.trailingAnchor, padding: .init(top: 0, left: 5, bottom: 0, right: 20))
    }
    
    @objc
    private func handleCheckBusyTime(){
        
        let popTimeVC = PopularTimeViewController()
        popTimeVC.popularHoursString = fullTextForPopularHours
        popTimeVC.modalTransitionStyle = .coverVertical
        popTimeVC.modalPresentationStyle = .overCurrentContext
        present(popTimeVC, animated: true, completion: nil)
    }
    
    
    
    private func loadZomatoProfile(){
        guard let location = placeLocation else {return}
        guard let placeName = googlePlaceFromId?.name!.replacingOccurrences(of: " ", with: "%20") else {return}
        ZomatoAPI.getZomatoProfile(location: location, query: placeName) { (zomatoProfile) in
            //Take URL
            //Set Accessibility
            //Set Rating colour
            guard let restaurant = zomatoProfile.restaurants?.first?.restaurant else {return}
            guard let urlString = restaurant.url else {return}
            guard let startUrl = URL(string: urlString) else {return}
            guard let ratingHexCode = restaurant.user_rating?.rating_color else {return}
            guard let rating = restaurant.user_rating?.aggregate_rating else {return}
            guard let ratingDesc = restaurant.user_rating?.rating_text else {return}
            self.detailView.ratingLabel.backgroundColor = UIColor(hex: ratingHexCode)
            self.detailView.ratingLabel.text = rating
            self.detailView.ratingDesc.text = ratingDesc
            let accessibilityLabel = self.detailView.accessibleRamp
            
            if self.isAccessible(startUrl: startUrl){
                self.setAccessibility(label: accessibilityLabel, labelText: "Wheelchair Accessible", imageName: "greenTick")
            }
            else{
                self.setAccessibility(label: accessibilityLabel, labelText: "Wheelchair Accessible", imageName: "redCross")
            }
           
        }
    }
    

    

}


struct Toilets{
    var toilet: PublicToilet
    var distance: Double
    
    init(toilet: PublicToilet, distance: Double) {
        self.toilet = toilet
        self.distance = distance
    }
}
