//
//  PublicToiletsCollectionViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 30/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import CoreLocation
private let reuseIdentifier = "Cell"

class PublicToiletsCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var allToiletsData:[Toilets] = []
    
    var locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Public Toilets"
        
        collectionView?.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        view.backgroundColor = .white
        self.collectionView!.register(PublicToiletsCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView?.reloadData()
     
       
    }
    override func viewWillAppear(_ animated: Bool) {
        //allToiletsData = AppDelegate.appDelegateShared().allPublicToiletsWithDistance
        print("Pring toilets count from CV \(allToiletsData.count)")
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }





    override func numberOfSections(in collectionView: UICollectionView) -> Int {

        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return allToiletsData.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PublicToiletsCollectionViewCell
        
        let toilet = allToiletsData[indexPath.item].toilet
        var distance = allToiletsData[indexPath.item].distance
        distance = distance/1000
        distance = distance.rounded(toPlaces: 2)
        
    
        var name = toilet.name
        name = name?.replacingOccurrences(of: "Public Toilet - ", with: "")
        name = name?.replacingOccurrences(of: "Toilet ", with: "")
        cell.namelabel.text = name
        cell.distanceLabel.text = "\(distance) km from you"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 100)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let toilet = allToiletsData[indexPath.item].toilet
        
        let coordinates = CLLocation(latitude: toilet.lat!, longitude: toilet.lon!)
        
        GoogleClient.sharedInstance.openGoogleMaps(route: coordinates)

        
    }
    
 

}


