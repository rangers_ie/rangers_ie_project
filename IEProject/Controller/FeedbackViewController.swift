//
//  FeedbackViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 16/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController {
    private let cellId = "cellId"
    var googlePlace: GooglePlaceFromId?
    
    
    let feedbackView: FeedbackView = {
        let feedBackView = FeedbackView()
        return feedBackView
    }()
    
    private var answers = [String: Any]()
    
    
    
    
    
    lazy var feedbackCV: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let col = UICollectionView(frame: .zero, collectionViewLayout: layout )
        
        col.dataSource = self
        col.delegate = self
        col.backgroundColor = UIColor.white
        //col.contentInset = UIEdgeInsets(top: 60, left: 0, bottom: 0, right: 0)
        col.isUserInteractionEnabled = true

        col.isPagingEnabled = true
        
        return col
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(feedbackCV)
        feedbackCV.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        feedbackCV.register(FeedbackCVCell.self, forCellWithReuseIdentifier: cellId)
        
    }
    
    var thumbsUPButton = UIButton ()
    var thumbsDownButton = UIButton ()
    @objc
    private func handleThumbsUp(_ sender: UIButton){
        
        thumbsUPButton = sender
        thumbsDownButton.isSelected = false
        let index = String(sender.tag)
        let indexKey = setQuestionToIndex(index: sender.tag)
        print(indexKey)
        let answer = "Yes"
        answers.updateValue(answer, forKey: indexKey)
        if sender.isSelected == true{
            sender.isSelected = false}
        else{
            sender.isSelected = true}
        
        if index.description == (Questions.allQuestion.count - 1).description {
            showCompletitionAlert()
        }
        handleScrollToNextIndex()
        
    }
    
    @objc
    private func handleThumbsDown(_ sender: UIButton){
        thumbsDownButton = sender
        thumbsUPButton.isSelected = false
        let index = String(sender.tag)
        let indexKey = setQuestionToIndex(index: sender.tag)
        print(indexKey)
        let answer = "No"
        answers.updateValue(answer, forKey: indexKey)
        if sender.isSelected == true{
            sender.isSelected = false}
        else{
            sender.isSelected = true}
 
        if index.description == (Questions.allQuestion.count - 1).description {
            showCompletitionAlert()
        }
        handleScrollToNextIndex()
    }
    
    private func showCompletitionAlert(){
        
        let completetionAlert = UIAlertController(title: "Thank You", message: "We appreciate your feedback. Please press Ok to continue", preferredStyle: UIAlertControllerStyle.alert)
        
        completetionAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            let placeId = (self.googlePlace?.result?.place_id)!
            if self.answers.count == 3{
                //answers.updateValue(placeId, forKey: "id")
                FirebaseDBProvider.saveRating(answers: self.answers, placeId: placeId)
            }
            self.dismiss(animated: true, completion: nil)
        }))
        
        
        
        present(completetionAlert, animated: true, completion: nil)
        
    }
    
    private func setQuestionToIndex(index: Int)->String{
        switch index{
        case 0:
            let question = "Question01"
            return question
        case 1:
            let question = "Question02"
            return question
        case 2:
            let question = "Question03"
            return question
        default:
            print("default")
            return "default"
        }
    }
    
    @objc
    private func handleExit(_ sender: UIButton){
        let placeId = (googlePlace?.result?.place_id)!
        if answers.count == 3{
            //answers.updateValue(placeId, forKey: "id")
            FirebaseDBProvider.saveRating(answers: answers, placeId: placeId)
        }
        dismiss(animated: true, completion: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  
}

extension FeedbackViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Questions.allQuestion.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! FeedbackCVCell
//        if indexPath.item == 2 {
//            cell.takeBackButton.isHidden = false
//        }
        cell.thumbsUpButton.addTarget(self, action: #selector(handleThumbsUp(_:)), for: .touchUpInside)
        cell.thumbsDownButton.addTarget(self, action: #selector(handleThumbsDown(_:)), for: .touchUpInside)
        cell.takeBackButton.addTarget(self, action: #selector(handleExit(_:)), for: .touchUpInside)
        
        cell.thumbsUpButton.tag = indexPath.item
        cell.thumbsDownButton.tag = indexPath.item
        
        let question = Questions.allQuestion[indexPath.item]
        cell.question.text = question
        cell.cellCount.text = "\(indexPath.item+1)/3"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    @objc
    private func handleScrollToNextIndex(){
        
        let visibleItems: NSArray = self.feedbackCV.indexPathsForVisibleItems as NSArray
        
        var minItem: NSIndexPath = visibleItems.object(at: 0) as! NSIndexPath
        for itr in visibleItems {
            
            if minItem.row > (itr as AnyObject).row {
                minItem = itr as! NSIndexPath
            }
        }
        
        let nextItem = NSIndexPath(row: minItem.row + 1, section: 0)
        
        if nextItem.item < Questions.allQuestion.count {
            self.feedbackCV.scrollToItem(at: nextItem as IndexPath, at: .left, animated: true)
        }
        
        
        

    }
    
    
}
