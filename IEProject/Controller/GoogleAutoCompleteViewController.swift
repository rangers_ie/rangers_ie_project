//
//  GoogleAutoCompleteViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 27/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation
class GoogleAutoCompleteViewController: UIViewController {
    
    var GresultsViewController: GMSAutocompleteResultsViewController?
    var GsearchController: UISearchController?
    var GresultView: UITextView?
    var addressDelegate: AddressComponentsDelegate?
    //var isSearching: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        setUpGoogleSearchBar()


    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    private func setUpGoogleSearchBar(){
        GresultsViewController = GMSAutocompleteResultsViewController()
        GresultsViewController?.delegate = self
        
        GsearchController = UISearchController(searchResultsController: GresultsViewController)
        GsearchController?.searchResultsUpdater = GresultsViewController
        
        // Put the search bar in the navigation bar.
        GsearchController?.searchBar.sizeToFit()
        navigationItem.titleView = GsearchController?.searchBar
        
        GsearchController?.searchBar.becomeFirstResponder()
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // Prevent the navigation bar from being hidden when searching.
        GsearchController?.hidesNavigationBarDuringPresentation = false
        
        //Restrict to australia
        let auFilter = GMSAutocompleteFilter()
        //auFilter.type = .region
        auFilter.country = "AU"
        
        let regionA = CLLocationCoordinate2DMake(-37.815299, 144.962656)
        let regionB = locationWithBearing(bearing: 300.0, distanceMeters: 10000, origin: regionA)
        
        let bounds = GMSCoordinateBounds(coordinate: regionA, coordinate: regionB)
        GresultsViewController?.autocompleteBoundsMode = .restrict
        GresultsViewController?.autocompleteBounds = bounds
        
        GresultsViewController?.autocompleteFilter = auFilter
        
        
    }
    
    func locationWithBearing(bearing:Double, distanceMeters:Double, origin:CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        let distRadians = distanceMeters / (6372797.6) // earth radius in meters
        
        let lat1 = origin.latitude * .pi / 180
        let lon1 = origin.longitude * .pi / 180
        
        let lat2 = asin(sin(lat1) * cos(distRadians) + cos(lat1) * sin(distRadians) * cos(bearing))
        let lon2 = lon1 + atan2(sin(bearing) * sin(distRadians) * cos(lat1), cos(distRadians) - sin(lat1) * sin(lat2))
        
        return CLLocationCoordinate2D(latitude: lat2 * 180 / .pi, longitude: lon2 * 180 / .pi)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
 
}

//GMSAutocompleteResultsViewControllerDelegate

extension GoogleAutoCompleteViewController: GMSAutocompleteResultsViewControllerDelegate{

    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        
    
        GsearchController?.isActive = false

//        let locationId = place.placeID
//        let placeVC = TabBarViewController()
//        placeVC.placeID = locationId

        let latitude = place.coordinate.latitude
        let longitude = place.coordinate.longitude
        let location = CLLocation(latitude: latitude, longitude: longitude)
        AppDelegate.appDelegateShared().currentLocation = location

        GoogleClient.sharedInstance.reverseGeocode(location: location) { (responce) in
            guard let addressComponents = responce.results?.first?.address_components else{return}

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {

                self.addressDelegate?.getAddressComponents(addressComps: addressComponents)
                self.navigationController?.popViewController(animated: true)

            }


        }

    }


    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }

    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }

    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }


}


protocol AddressComponentsDelegate {
    func getAddressComponents(addressComps: [Address_components])
}

class GoogleSearchLocation: NSObject {
    var name: String?
    var address: String?
    var coordinates: CLLocation?
    
    init(nm: String, ads: String,cord: CLLocation) {
        self.name = nm
        self.address = ads
        self.coordinates = cord
    }
}


