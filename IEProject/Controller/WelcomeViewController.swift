//
//  WelcomeViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 20/8/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class WelcomeViewController: UIViewController {
    
    let backgroundImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "firstPage"))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
//    let passwordTextField: UITextField = {
//        let textField = UITextField()
//        textField.placeholder = "Enter password here"
//        textField.backgroundColor = UIColor.white
//        textField.layer.cornerRadius = 10
//        textField.isSecureTextEntry = true
//        textField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
//        return textField
//    }()
//
//    let takeMeHomeButton: UIButton = {
//        let button = UIButton(type: .system)
//        button.setTitle("Click here to login", for: .normal)
//        button.setTitleColor(UIColor.blue.withAlphaComponent(0.5), for: .normal)
//        return button
//    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(backgroundImageView)
//        view.addSubview(takeMeHomeButton)
//        view.addSubview(passwordTextField)
        
        //passwordTextField.delegate = self
        
        backgroundImageView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            let latestHomeViewController = LatesHomeViewController()
            self.navigationController?.pushViewController(latestHomeViewController, animated: true)
                    }

        //passwordTextField.centerAnchor(centerX: view.centerXAnchor, centerY: view.centerYAnchor, size: CGSize(width: view.frame.width-50, height: 50), padding: .init(top: 80, left: 0, bottom: 0, right: 0))
      
        //takeMeHomeButton.centerAnchor(centerX: view.centerXAnchor, centerY: passwordTextField.bottomAnchor, size: CGSize(width: view.frame.width, height: 0), padding: .init(top: 20, left: 0, bottom: 0, right: 0))
        
        //takeMeHomeButton.addTarget(self, action: #selector(handleTakeMeHome), for: .touchUpInside)
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(handleOnScreenTap))
//        view.addGestureRecognizer(tap)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
//    @objc
//    private func handleTakeMeHome(){
//        let secretKey = "71"
//        let passwordFieldText = passwordTextField.text
//        if passwordFieldText == "" {
//            NotificationProvider.sharedInstance.alertUser(title: "Invalid login data", message: "The password field must be filled to login", viewController: self)
//            return
//        }
//        else if(passwordFieldText?.elementsEqual(secretKey))!{
//            let latestHomeViewController = LatesHomeViewController()
//
//
//            //let obvc = OnboardingViewController()
//            navigationController?.pushViewController(latestHomeViewController, animated: true)
//        }
//        else{
//            NotificationProvider.sharedInstance.alertUser(title: "Incorrect Password", message: "The password entered in invalid", viewController: self)
//            return
//        }
//
//    }
//
//    @objc
//    private func handleOnScreenTap(){
//        view.endEditing(true)
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}


//extension WelcomeViewController: UITextFieldDelegate {
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        handleTakeMeHome()
//        return true
//    }
//}
