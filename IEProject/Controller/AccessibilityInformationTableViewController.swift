//
//  AccessibilityInformationTableViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 15/9/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import CoreLocation

class AccessibilityInformationTableViewController: UITableViewController {
    
    let cellId = "cellId"
    var allBuildings: [Building] = []
    var allPlacesFromAddress: [PlaceFromAddress] = []
    var filteredBuildings:[Building] = []
    var isSearching: Bool = false
    
   
    
    var searchBar: UISearchBar = {
        let width = Int(UIScreen.main.bounds.width)
        let frame = CGRect(x: 0, y: 0, width: width, height: 40)
        let searchBar = UISearchBar(frame: frame)
        searchBar.placeholder = "Enter address here"
        return searchBar
        
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Accessibility Information"
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        self.navigationItem.titleView = searchBar
        tableView.register(AccessibilityInfoTableViewCell.self, forCellReuseIdentifier: cellId)
        loadData()

    }
    
    override func viewWillAppear(_ animated: Bool) {

        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if isSearching{
            return filteredBuildings.count
        }
        return allBuildings.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! AccessibilityInfoTableViewCell
        
        if isSearching{
            let building = filteredBuildings[indexPath.item]
            setupCell(building: building, cell: cell)
            return cell
        }
        let building = allBuildings[indexPath.item]
        setupCell(building: building, cell: cell)
        return cell
    }
    
    private func setupCell(building: Building, cell: AccessibilityInfoTableViewCell){
        if building.accessibility_rating != nil{
            let rating = Int(building.accessibility_rating!)
            if rating! > 1{
                cell.rating.backgroundColor = UIColor.green
                cell.rating.textColor = UIColor.black
            }
            else{
                cell.rating.backgroundColor = UIColor.orange
            }
        }
        cell.buildingName.text = building.street_address
        cell.rating.text = building.accessibility_rating
        cell.type.text = building.accessibility_type
        
    
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSearching{
            let building = filteredBuildings[indexPath.item]
            setupCellDidSelect(building: building)
        }
        else{
            let building = allBuildings[indexPath.item]
            setupCellDidSelect(building: building)
        }
    }
    
    private func setupCellDidSelect(building: Building){
        let address = "\(building.street_address!) Melbourne".replacingOccurrences(of: " ", with: "+")
        getGooglePlaceForBuilding(address: address)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }

}

extension AccessibilityInformationTableViewController{
    
    private func loadData(){
        APIClient.sharedInstance.getBuildingInformation { (buildings) in
            self.allBuildings = buildings
            self.filteredBuildings = buildings
            //self.allBuildings.sorted{Int($0.accessibility_rating!)(Int($1!)) == .orderedAscending}
            self.tableView.reloadData()
            
            
            
            //  self.allStopsWithDistances.sort{($0.googleDir.routes?.last?.legs?.last?.distance?.value)! < ($1.googleDir.routes?.last?.legs?.last?.distance?.value)!}
        }
    }
    
    private func getGooglePlaceForBuilding(address: String){
        GoogleClient.sharedInstance.getPlaceInformationUsingAddress(address:address) { (response) in
            let tabBarVC = TabBarViewController()
            guard let placeId = response.results?.first?.place_id else{print("Invalid Location"); return}
            tabBarVC.placeID = placeId
            self.present(tabBarVC, animated: true, completion: nil)

        }
    }
}



extension AccessibilityInformationTableViewController: UISearchBarDelegate{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearching = true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearching = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredBuildings.removeAll(keepingCapacity: false)
        let predicateString = searchBar.text!
        filteredBuildings = allBuildings.filter({$0.street_address?.range(of: predicateString) != nil})
        isSearching = (filteredBuildings.count == 0) ? false: true
        tableView?.reloadData()
    }
}


