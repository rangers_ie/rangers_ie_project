//
//  NewBuildingAccessibiltyTableViewController.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 2/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import UIKit
import JGProgressHUD
class NewBuildingAccessibiltyTableViewController: UITableViewController {
    
    var allBuildingData:[BuildingNew] = []
    var allFilteredBuidings:[BuildingNew] = []
    var isSearching: Bool = false
    
    
    var searchBar: UISearchBar = {
        let width = Int(UIScreen.main.bounds.width)
        let frame = CGRect(x: 0, y: 0, width: 200, height: 20)
        let searchBar = UISearchBar(frame: frame)
        searchBar.placeholder = "Enter address here"
        return searchBar
        
    }()
    var hud: JGProgressHUD?
    let cellID = "cellId"
    override func viewDidLoad() {
        super.viewDidLoad()
        hud = JGProgressHUD(style: .extraLight)
        hud?.show(in: self.view)
        searchBar.delegate = self
        self.navigationItem.titleView = searchBar
        loadData()
        
        tableView.register(AccessibilityInfoTableViewCell.self, forCellReuseIdentifier: cellID)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if isSearching{
            return allFilteredBuidings.count
            
        }
        return allBuildingData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! AccessibilityInfoTableViewCell
        
        if isSearching{
            let building = allFilteredBuidings[indexPath.item]
            cell.buildingName.text = building.street_address
            cell.type.text = building.accessibility_type
            cell.typeDesc.text = building.accessibility_type_description
            cell.rating.text = building.accessibility_rating?.description
           
            return cell
        }
        let building = allBuildingData[indexPath.item]
        cell.buildingName.text = building.street_address
        cell.type.text = building.accessibility_type
        cell.typeDesc.text = building.accessibility_type_description
        
        let accRating = "\((building.accessibility_rating?.description)!)/3"
        cell.rating.text = accRating
       
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSearching{
            let building = allFilteredBuidings[indexPath.item]
            let message = building.accessibility_type_description
            let title = building.accessibility_type
            NotificationProvider.sharedInstance.alertUser(title: title!, message: message!, viewController: self)
        }
        else{
            let building = allBuildingData[indexPath.item]
            let message = building.accessibility_type_description
            let title = building.accessibility_type
            NotificationProvider.sharedInstance.alertUser(title: title!, message: message!, viewController: self)
        }
    
    }
    
    
    private func loadData(){
        APIClient.sharedInstance.getBuildingInfoNew { (responce) in
            self.allBuildingData = responce
            self.allFilteredBuidings = responce
            self.allBuildingData.sort{($0.accessibility_rating)! > ($1.accessibility_rating)!}
            
            self.tableView.reloadData()
            self.hud?.dismiss()
        }
    }

    

}



extension NewBuildingAccessibiltyTableViewController: UISearchBarDelegate{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearching = true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearching = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        allFilteredBuidings.removeAll(keepingCapacity: false)
        let predicateString = searchBar.text!
        allFilteredBuidings = allBuildingData.filter({$0.street_address?.range(of: predicateString) != nil})
        isSearching = (allFilteredBuidings.count == 0) ? false: true
        tableView?.reloadData()
    }
}



