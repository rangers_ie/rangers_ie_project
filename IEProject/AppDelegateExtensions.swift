//
//  AppDelegateExtensions.swift
//  IEProject
//
//  Created by Minon Weerasinghe on 3/10/18.
//  Copyright © 2018 RangersIE. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftPTV
extension AppDelegate: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLCircularRegion{
            print("Entered region \(region.identifier)")
        }
    }
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if region is CLCircularRegion{
            
            GoogleClient.sharedInstance.getPlaceFromId(palceId: region.identifier) { (place) in
                self.locationManager.stopMonitoring(for: region)
                print("App delegate adding \((place.result?.name)!) to Firebase")
                
                
                Utilities.handleRegionLeftEvent(region: region, viewController: (self.window?.rootViewController)!)
                
            }
            
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //guard let locationNow = locations.last else {print("Unable to get current location");return}
        
        guard let location = manager.location?.coordinate else {print("Unable to get current location");return}
        let locationNow = CLLocation(latitude: location.latitude, longitude: location.longitude)
        
        print("Location updated - Method from AppDelegate")
        currentLocation = locationNow
        runAllMethods()
        locationManager.stopUpdatingLocation()
    }
    
    func runAllMethods(){
        loadTrendingFromZomato()
        getLocationInformation()
        loadAllMetroStops()
        loadAllTramStop()
        loadPublicToiletData()
        loadAllCafesAroundGivenLocation()
        loadAllRestaurantsAroundGivenLocation()
    }
    
    
    
}

extension AppDelegate{
    
     func setupLocationManager(){
        //Request Autoriziation from user to use the current location
        self.locationManager.requestAlwaysAuthorization()
        
        //Request Authorization from user to use location in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            
            
        }
    }
    
}

//For loading data
extension AppDelegate{
    func loadTrendingFromZomato(){
        
      
        
        //Collection ID: 1 is for top trending for the week
        ZomatoAPI.getTopTrending(location: currentLocation, collectionId: 1, establishmentType: nil) { (responce) in
            self.allTrendingLocations = responce
            print("loadTrendingFromZomato DONE")
        }
       
    }
    
    //Reverse geo coding
    func getLocationInformation(){

        GoogleClient.sharedInstance.reverseGeocode(location: currentLocation) { (responce) in

            guard let addressComponents = responce.results?.first?.address_components else {return}
            self.currentAddressComponents = addressComponents
            self.currentLocationInformation = responce
            print("getLocationInformation DONE")
        }
    }
    
    //Loading PTV RouteTypes
    func getRouteType(){
        
        PTVProvider.ptvAPI.retrieveRouteTypes(parameters: nil) { (responce) in
            guard let allRoutes = responce?.routeTypes else {print("NO ROUTE TYPES"); return}
            print("getRouteType DONE")
            DispatchQueue.main.async {
                self.allRouteTypes = allRoutes
            
            }
        }
    }
    
    
    func loadAllMetroStops(){
        
    
        let ptvLocation = PTVLocation(lats: currentLocation.coordinate.latitude , longs: currentLocation.coordinate.longitude)
        
        let params = ["route_types":"0","max_distance":"10000"]
        
        PTVProvider.ptvAPI.retrieveStopsNearLocation(location: ptvLocation, parameters: params) { (allStops) in
            
            guard let allStops = allStops?.stops else {print("Stops not retrieved");return}
            self.allTrainStops = allStops
            print("loadAllMetroStops DONE")

        }
    }
    
    func loadAllTramStop(){
     
        let ptvLocation = PTVLocation(lats: currentLocation.coordinate.latitude , longs: currentLocation.coordinate.longitude)
        let params = ["route_types":"1","max_distance":"10000"]
        PTVProvider.ptvAPI.retrieveStopsNearLocation(location: ptvLocation, parameters: params) { (allStops) in
            
            guard let allStops = allStops?.stops else {print("Stops not retrieved");return}
            self.allTramStops = allStops
            print("loadAllTramStop DONE")
            
        }
        
        
    
    }
    
    
    func loadPublicToiletData(){
        APIClient.sharedInstance.getPublicToiletsData { (responce) in
            self.allPublicToilets = responce
            //self.loadToiletDataWithDistance()
            print("loadPublicToiletData DONE")
        }
    }
    
   
    
    func loadAllCafesAroundGivenLocation(){
        
        let type = "cafe"
        GoogleClient.sharedInstance.getPlacesAroundGivenLocation(coordinates: currentLocation, locationType: type) { (response) in
            guard let allCafes = response.results else {print("CAFES NOT LOADED");return}
            self.allCafeForGivenLocation = allCafes
            print("loadAllCafesAroundGivenLocation DONE")
          
            
        }
    }
    
    func loadAllRestaurantsAroundGivenLocation(){
        let type = "restaurant"
        GoogleClient.sharedInstance.getPlacesAroundGivenLocation(coordinates: currentLocation, locationType: type) { (response) in
            guard let allRests = response.results else {print("RESTS NOT LOADED");return}
            self.allRestsForGivenLocation = allRests
            print("loadAllRestaurantsAroundGivenLocation DONE")
          
        }
    }
    
   


}
